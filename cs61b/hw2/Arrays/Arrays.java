/* NOTE: The file ArrayUtil.java contains some functions that may be useful
 * in testing your answers. */

/** HW #2 */

import com.sun.tools.javac.code.Attribute;

import java.util.ArrayList;
import java.util.Iterator;

/** Array utilities.
 *  @author Xiyuan Wang
 */
class Arrays {
    /* C. */
    /** Returns a new array consisting of the elements of A followed by the
     *  the elements of B. */
    static int[] catenate(int[] A, int[] B) {
        /* *Replace this body with the solution. */
        int [] result = new int [A.length + B.length];
        for (int i = 0; i < result.length; i++ ) {
            if (i < A.length) {
                result[i] = A[i];
            } else {
                result[i] = B[i - A.length];
            }
        }
        return result;
    }

    /** Returns the array formed by removing LEN items from A,
     *  beginning with item #START. */
    static int[] remove(int[] A, int start, int len) {
        /* *Replace this body with the solution. */
        int l = 0;
        if (start > A.length){
            l = A.length;
        } else if (start + len > A.length) {
            if (start == 0){
                l = 0;
            } else {
                l = start - 1;
            }
        } else {
            l = A.length - len;
        }
        int [] arr = new int [l];
        int i = 0;
        for (int j = 0; j < A.length; j++ ) {
            if (j < start - 1) {
                arr[j] = A[j];
            } else if (j >= start - 1 && j < start + len - 1) {
               continue;
            } else if (start - 1 + i < l) {
                arr[start - 1 + i] = A[j];
                i ++;
            }
        }

        return arr;
    }

    /* E. */
    /** Returns the array of arrays formed by breaking up A into
     *  maximal ascending lists, without reordering.
     *  For example, if A is {1, 3, 7, 5, 4, 6, 9, 10}, then
     *  returns the three-element array
     *  {{1, 3, 7}, {5}, {4, 6, 9, 10}}. */
    static int[][] naturalRuns(int[] A) {
        /* *Replace this body with the solution. */
        int [] [] arr = new int [1][];
        int [] temp;
        int [] temp1;
        for(int i = 0; i < A.length; i++ ) {
            if (A[i] >= A[i + 1]) {
                System.arraycopy(A, 0, arr[0], );
            }
        }
    }
}

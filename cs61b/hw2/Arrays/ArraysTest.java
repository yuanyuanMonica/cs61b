import org.junit.Test;

import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.Assert.*;

/** Test for catenate remove and natural run
 *  @author Xiyuan Wang
 */

public class ArraysTest {
    /** Test for catenate
     */
    @Test
    public void catenateTest(){
        int [] A = new int[2];
        int [] B = new int [3];
        for(int i = 0; i < A.length; i++ ) {
            A[i] = i;
        }
        for (int i = 0; i < B.length; i++ ) {
            B[i] = i + 2;
        }
        IntList C = IntList.list(0, 1, 2, 3, 4);
        assertEquals(true, Utils.equals(C, Arrays.catenate(A, B)));
    }

    /** Test for remove
     */
    @Test
    public void removeTest(){
        int [] A = new int [7];
        for(int i = 0; i < A.length; i++ ) {
            A[i] = i;
        }
        IntList B = IntList.list(0, 1, 2, 3, 4, 5, 6);
        IntList C = IntList.list();
        IntList D = IntList.list(0, 1, 2, 3);
        IntList E = IntList.list(0, 1, 2, 3, 6);

        assertEquals(true, Utils.equals(B, Arrays.remove(A, 8, 1)));
        assertEquals(true, Utils.equals(C, Arrays.remove(A, 0, 8)));
        assertEquals(true, Utils.equals(D, Arrays.remove(A, 5, 5)));
        assertEquals(true, Utils.equals(E, Arrays.remove(A, 5, 2)));
    }

    /** Test for naturalRuns
     */
    @Test
    public void naturalRunsTest() {

    }

    public static void main(String[] args) {
        System.exit(ucb.junit.textui.runClasses(ArraysTest.class));
    }
}

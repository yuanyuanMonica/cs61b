/* NOTE: The file Utils.java contains some functions that may be useful
 * in testing your answers. */


/** HW #2, Problem #1. */

/** List problem.
 *  @author Xiyuan Wang
 */
class Lists {
    /** Return the list of lists formed by breaking up L into "natural runs":
     *  that is, maximal strictly ascending sublists, in the same order as
     *  the original.  For example, if L is (1, 3, 7, 5, 4, 6, 9, 10, 10, 11),
     *  then result is the four-item list
     *            ((1, 3, 7), (5), (4, 6, 9, 10), (10, 11)).
     *  Destructive: creates no new IntList items, and may modify the
     *  original list pointed to by L. */
    static IntList2 naturalRuns(IntList L) {
        /* *Replace this body with the solution. */
        IntList curr = L;
        while (curr != null && curr.tail != null){
            if (curr.head >= curr.tail.head){
                IntList temp = curr.tail;
                curr.tail = null;
                return new IntList2(L, naturalRuns(temp));
            }
            curr = curr.tail;
        }
        return new IntList2(L, null);
    }

}

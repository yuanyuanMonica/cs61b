import org.junit.Test;
import static org.junit.Assert.*;

/**This Test is to test naturalrun function in Lists.java
 *  @author Xiyuan Wang
 */

public class ListsTest {

    @Test
    public void naturalrunTest(){
        /** Test for empty list
         */
        IntList l = IntList.list();
        assertEquals(IntList2.list(l), Lists.naturalRuns(l));
        /**Test for one element list
         */
        IntList l1 = IntList.list(1);
        assertEquals(IntList2.list(l1), Lists.naturalRuns(l1));
        /**Test for list
         */
        IntList l2 = IntList.list(1, 2, 3, 2, 1);
        assertEquals(IntList2.list(IntList.list(1, 2, 3), IntList.list(2), IntList.list(1)), Lists.naturalRuns(l2));

        /**Test for another list
         */
        IntList l3 = IntList.list(1, 3, 7, 5, 4, 6, 9, 10, 10, 11);
        assertEquals(IntList2.list(IntList.list(1, 3, 7), IntList.list(5),
                IntList.list(4, 6, 9, 10), IntList.list(10, 11)), Lists.naturalRuns(l3));

    }


    // It might initially seem daunting to try to set up
    // Intlist2 expected.
    //
    // There is an easy way to get the IntList2 that you want in just
    // few lines of code! Make note of the IntList2.list method that
    // takes as input a 2D array.

    public static void main(String[] args) {
        System.exit(ucb.junit.textui.runClasses(ListsTest.class));
    }
}

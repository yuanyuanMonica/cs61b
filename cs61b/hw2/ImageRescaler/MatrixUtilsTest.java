import org.junit.Test;
import static org.junit.Assert.*;

/** Test for accumulate and accumulateVerical
 *  @author Xiyuan Wang
 */

public class MatrixUtilsTest {

    public static double[][] createSample_input(){
        double[][] sample_input ={
                {1000000,  1000000,   1000000,   1000000},
                {1000000,     75990,     30003,   1000000},
                {1000000,     30002,    103046,   1000000},
                {1000000,     29515,     38273,   1000000},
                {1000000,     73403,     35399,  1000000},
                {1000000,   1000000,   1000000,   1000000}};
        return sample_input;
    }

    public static  double[][] createSample_input1() {
        double [][] sample_input1 = {
                {1000000, 1000000, 1000000, 1000000, 1000000, 1000000},
                {1000000, 75990, 30002, 29515, 73403, 1000000},
                {1000000, 30003, 103046, 38273, 35399, 1000000},
                {1000000, 1000000, 1000000, 1000000, 1000000, 1000000}
        };
        return sample_input1;
    }

    public static double[][] createSample_Output() {
        double[][] sample_output = {
                {1000000,   1000000,   1000000,   1000000},
                {2000000,   1075990,   1030003,  2000000},
                {2075990,   1060005,   1133049,   2030003},
                {2060005,   1089520,   1098278,   2133049},
                {2089520,   1162923,   1124919,   2098278},
                {2162923,   2124919,   2124919,   2124919}
        };

        return sample_output;
    }

    /** Test for accumulateVertical function
     */
    @Test
    public void accumulateVerticalTest() {
        assertArrayEquals(createSample_Output(), MatrixUtils.accumulateVertical(createSample_input()));
    }
    /** Test for accumulate function
     */
    @Test
    public void  accumulateTest() {
        assertArrayEquals(createSample_Output(), MatrixUtils.accumulate(createSample_input(), MatrixUtils.Orientation.VERTICAL));
        assertArrayEquals(createSample_Output(), MatrixUtils.accumulate(createSample_input1(), MatrixUtils.Orientation.HORIZONTAL));
    }

    public static void main(String[] args) {
        System.exit(ucb.junit.textui.runClasses(MatrixUtilsTest.class));
    }
}

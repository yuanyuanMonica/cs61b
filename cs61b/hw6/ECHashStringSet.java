// REPLACE THIS STUB WITH THE CORRECT SOLUTION.
// The current contents of this file are merely to allow things to compile 
// out of the box. It bears scant relation to a proper solution (for one thing,
// a hash table should not be a SortedStringSet.)

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/** A set of String values.
 *  @author Xiyuan Wang
 */
class ECHashStringSet implements StringSet {

    ECHashStringSet() {
        _hashTable = new StringList [ (int) (1 / minLoad)];
        _size = 0;
    }

    private StringList getBucket(int b) {
        return _hashTable[b];
    }

    public void put(String s) {
        if (load() > maxLoad) {
            resize();
        }
        if (!contains(s)) {
            int bucketNum = bucketNum(s);
            StringList myBucket = getBucket(bucketNum);
            if (myBucket == null) {
                StringList l = new StringList();
                l.add(s);
                _hashTable[bucketNum] = l;;
            } else {
                myBucket.add(s);
            }
            _size++;
        }
    }

    public boolean contains(String s){
        int bucketNum = bucketNum(s);
        StringList myBucket = getBucket(bucketNum);
        if (myBucket != null) {
            for (int i = 0; i < myBucket.size(); i++ ) {
                if (myBucket.get(i).equals(s)) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<String> asList() {
        StringList l = new StringList();
        Iterator [] iter_list = new Iterator[_hashTable.length];
        for (int i = 0; i < _hashTable.length; i++) {
            if (getBucket(i) != null) {
                iter_list[i] = getBucket(i).iterator();
            }
        }
        while (l.size() < size()) {
            for (int i = 0; i < _hashTable.length; i++) {
                if (iter_list[i] != null && iter_list[i].hasNext()) {
                    String curr = (String)iter_list[i].next();
                    if (l.size() == 0) {
                        l.add(curr);
                    } else {
                        int add_or_not = l.size();
                        for (int j = 0; j < l.size(); j++) {
                            if (l.get(j).compareTo(curr) >= 0) {
                                l.add(j, curr);
                                break;
                            }
                        }
                        if (l.size() == add_or_not) {
                            l.add(curr);
                        }
                    }
                }
            }
        }
        return l;
    }

    public int size() {
        return _size;
    }

    private int hashCode(String s) {
        return s.hashCode() & 0x7fffffff;
    }

    private int bucketNum(String s) {
        return hashCode(s) % _hashTable.length;
    }

    private double load() {
        return ((double)_size / (double)_hashTable.length);
    }

    private void resize() {
        StringList [] oldTable = _hashTable;
        int oldLength = _hashTable.length;
        _hashTable = new StringList[2 * _hashTable.length];
        _size = 0;
        for (int i = 0; i < oldLength; i++ ) {
            StringList myBucket = oldTable[i];
            if (myBucket != null) {
                for (int j = 0; j < myBucket.size(); j++ ) {
                    this.put(myBucket.get(j));
                }
            }
        }
    }

    //HasTable created for Strings
    private StringList [] _hashTable;

    //Current number of String stored
    private int _size;

    //maximum load factor, minmum load factor
    private static final double minLoad = 0.2;
    private static final double maxLoad = 5;

    //Structures of ArrayList of Strings
    private class StringList extends ArrayList<String> {
    }
}
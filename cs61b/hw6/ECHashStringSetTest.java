import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

public class ECHashStringSetTest {
    @Test
    public void BSTStringSetTest() {
        ECHashStringSet s1 = new ECHashStringSet();
        s1.put("banana");
        s1.put("apple");
        s1.put("different");
        s1.put("cinnamon");


        assertEquals(true, s1.contains("apple"));
        assertEquals(false, s1.contains("same"));

        List<String> exp = new ArrayList<String>();
        exp.add("apple");
        exp.add("banana");
        exp.add("cinnamon");
        exp.add("different");
        List<String> actual = s1.asList();
        for (int i = 0; i < exp.size(); i++ ) {
            assertEquals(exp.get(i), actual.get(i));
        }
    }

    @Test
    public void moreTest() {
        TreeSet<String> exp1 = new TreeSet<>();
        ECHashStringSet s2 = new ECHashStringSet();
        for (int i = 0; i < 30; i++ ) {
            String rs = StringUtils.randomString(7);
            System.out.println(rs);
            exp1.add(rs);
            if (i == 25) {
                System.out.println("to be resized");
            }
            s2.put(rs);
        }
        List<String> l = s2.asList();
        for (int i = 0; i <exp1.size(); i++) {
            assertEquals(exp1.pollFirst(), l.get(i));
        }
    }

}

import java.util.Iterator;
import utils.Filter;

/** A kind of Filter that lets through every other VALUE element of
 *  its input sequence, starting with the first.
 *  @author I
 */
class AlternatingFilter<Value> extends Filter<Value> {

    /** A filter of values from INPUT that lets through every other
     *  value. */
    AlternatingFilter(Iterator<Value> input) {
        super(input); //FIXME? //No thanks
        // FIXME
        call_time = 0;
        //Fixed
    }

    @Override
    protected boolean keep() {
        // FIXME
        if (call_time == 0){
            call_time = 1;
            return true;
        }
        call_time = 0;
        return false;
        //Fixed
    }

    // FIXME
    private int call_time;
    //Fixed


}

package qirkat;

import org.junit.Test;
import static org.junit.Assert.*;

/** Tests of the Board class.
 *  @author Xiyuan Wang
 */
public class BoardTest {

    private static final String INIT_BOARD =
        "  b b b b b\n  b b b b b\n  b b - w w\n  w w w w w\n  w w w w w";

    private static final String[] GAME1 =
    { "c2-c3", "c4-c2",
      "c1-c3", "a3-c1",
      "c3-a3", "c5-c4",
      "a3-c5-c3",
    };

    private static final String [] GAME2 =
    {"c2-c3",  "c4-c2", "c1-c3", "a3-c1", "c3-a3",
     "b4-b3", "a3-c3", "d4-b2", "a2-c2", "c1-c3",
     "d3-b3", "e5-d4", "e3-e5-c3" , "d5-e5", "c3-c4",
     "c5-c3-a3", "d2-e3", "b5-b4", "e3-e4", "e5-e3",
     "e2-e4", "a3-b3", "e4-e5", "b4-a3", "b1-c1",
     "a5-b4", "e1-e2", "b3-c3", "e2-e3", "b4-b3",
     "d1-e1", "a4-b4", "e3-d4", "c3-d3", "d4-d2",
     "b3-c3", "d2-e2", "b4-b3", "a1-b1", "a3-a2",
     "c1-d1",  "b3-a3", "b1-c1", "a2-a1", "e2-e3",
     "c3-b2", "e3-d4", "a3-a2", "c1-a3", "a2-a4",
     "d4-e4", "a4-b4", "d1-d2", "b4-b3", "d2-e3",
     "b3-b2", "e1-e2", "b2-c1", "e3-d3"};

    private static final String GAME1_BOARD =
        "  b b - b b\n  b - - b b\n  - - w w w\n  w - - w w\n  w w b w w";

    private static void makeMoves(Board b, String[] moves) {
        for (String s : moves) {
            Move curr = Move.parseMove(s);
            b.makeMove(curr);
        }

    }

    @Test
    public void testInit1() {
        Board b0 = new Board();
        assertEquals(INIT_BOARD, b0.toString());
    }

    @Test
    public void testSetPieces() {
        Board b0 = new Board();
        b0.setPieces("wwbww w--ww --www b--bb bb-bb",
                PieceColor.WHITE);
        assertEquals(GAME1_BOARD, b0.toString());
        assertEquals(b0.whoseMove(), PieceColor.WHITE);
    }

    @Test
    public void testInternalCopy() {
        Board b0 = new Board();
        b0.setPieces("wwbww w--ww --www b--bb bb-bb",
                PieceColor.WHITE);
        Board b1 = new Board(b0);
        assertEquals(b0, b1);
    }

    @Test
    public void testGet() {
        Board b0 = new Board();
        b0.setPieces("wwbww w--ww --www b--bb bb-bb",
                PieceColor.WHITE);
        assertEquals(PieceColor.BLACK, b0.get(2));
        assertEquals(PieceColor.EMPTY, b0.get(17));
    }

    @Test
    public void testMakeMoves() {
        Board b0 = new Board();
        Board b1 = new Board();
        makeMoves(b0, GAME2);
        b1.setPieces("b-b-- ----w ---w- ----w ----w", PieceColor.BLACK);
        assertEquals(b1, b0);
    }

    @Test
    public void testMoves1() {
        Board b0 = new Board();
        makeMoves(b0, GAME1);
        assertEquals(GAME1_BOARD, b0.toString());
    }


    @Test
    public void testUndo() {
        Board b0 = new Board();
        Board b1 = new Board(b0);
        makeMoves(b0, GAME1);
        Board b2 = new Board(b0);
        for (int i = 0; i < GAME1.length; i += 1) {
            b0.undo();
        }
        assertEquals("failed to return to start", b1, b0);
        makeMoves(b0, GAME1);
        assertEquals("second pass failed to reach same position", b2, b0);
    }

}

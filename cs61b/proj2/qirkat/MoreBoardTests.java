package qirkat;

import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MoreBoardTests {
    private final char[][] boardRepr = new char[][]{
        {'b', 'b', 'b', 'b', 'b'},
        {'b', 'b', 'b', 'b', 'b'},
        {'b', 'b', '-', 'w', 'w'},
        {'w', 'w', 'w', 'w', 'w'},
        {'w', 'w', 'w', 'w', 'w'}
    };

    private final String [] undoTest = new String[]
    {"d2-c3", "d4-d2", "d1-d3", "b4-d2-d4", "b2-b4"};

    private final String [] moveTest = new String[]
    {"d3-c3", "b3-d3", "e3-c3", "d4-e3", "c3-d4",
     "d5-d3", "d2-d4", "e5-c3", "b2-d4", "c5-d5",
     "d4-c5", "c4-c3", "c2-c4", "b4-d4", "c5-e5-c3",
     "b5-c5", "e2-d2", "c5-b4", "a2-b2", "b4-b3",
     "b2-b4", "a4-c4-c2-e2", "c1-c2", "a5-b4", "d1-c1", "b4-b3"};

    private final PieceColor currMove = PieceColor.WHITE;

    /**
     * @return the String representation of the initial state. This will
     * be a string in which we concatenate the values from the bottom of
     * board upwards, so we can pass it into setPieces. Read the comments
     * in Board#setPieces for more information.
     *
     * For our current boardRepr, the String returned by
     * getInitialRepresentation is
     * "  w w w w w\n  w w w w w\n  b b - w w\n  b b b b b\n  b b b b b"
     *
     * We use a StringBuilder to avoid recreating Strings (because Strings
     * are immutable).
     */
    private String getInitialRepresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append("  ");
        for (int i = boardRepr.length - 1; i >= 0; i--) {
            for (int j = 0; j < boardRepr[0].length; j++) {
                sb.append(boardRepr[i][j] + " ");
            }
            sb.deleteCharAt(sb.length() - 1);
            if (i != 0) {
                sb.append("\n  ");
            }
        }
        return sb.toString();
    }

    /** Create a new board with the initial state. */
    private Board getBoard() {
        Board b = new Board();
        b.setPieces(getInitialRepresentation(), currMove);
        return b;
    }

    /** reset board b to initial state.*/
    private void resetToInitialState(Board b) {
        b.setPieces(getInitialRepresentation(), currMove);
    }

    @Test
    public void getsetTest() {
        Board b = getBoard();
        assertEquals(b.get(0), PieceColor.WHITE);
        assertEquals(b.get(10), PieceColor.BLACK);
        assertEquals(b.get(12), PieceColor.EMPTY);
        assertEquals(b.get('a', '1'), PieceColor.WHITE);
        assertEquals(b.get('c', '3'), PieceColor.EMPTY);
    }

    @Test
    public void jumpPossibleTest() {
        Board b = getBoard();
        b.setPieces("w---b -w-b- --b-- -b-w- b---w", currMove);
        for (int i = 0; i < 25; i++) {
            assertEquals(false, b.jumpPossible(i));
        }
        assertEquals(currMove, b.whoseMove());
    }

    @Test
    public void setPiecesTest() {
        Board b = getBoard();
        b.setPieces("bbbbb bbbbb bbbbb bbbbb bbbbb", currMove);
        for (int i = 0; i < 25; i++) {
            assertEquals(PieceColor.BLACK, b.get(i));
        }

    }

    @Test
    public void legalMoveTest() {
        Board b = getBoard();
        b.setPieces("----w --bb- -w--- -bb-- -----", currMove);
        assertEquals(false, b.legalMove(Move.parseMove("c2-a4")));
        assertEquals(false, b.legalMove(Move.parseMove("b2-c1")));
        assertEquals(false, b.legalMove(null));
        assertEquals(true, b.legalMove(Move.parseMove("b3-b5")));
        assertEquals(false, b.legalMove(Move.parseMove("e1-c3")));
        assertEquals(true, b.legalMove(Move.parseMove("e1-c3-a5")));
    }

    @Test
    public void moreLegalMoveTest() {
        Board b = getBoard();
        b.setPieces("----w --bb- -w--- -bb-- -----", currMove);
        System.out.println(b.toString());
        assertEquals(false, b.legalMove(Move.parseMove("b3-d5")));
        assertEquals(false, b.legalMove(Move.parseMove("b3-c1")));
        assertEquals(false, b.legalMove(Move.parseMove("b3-d5")));
        assertEquals(false, b.legalMove(Move.parseMove("e1-d3")));
        assertEquals(false, b.legalMove(Move.parseMove("b3-d1-d3")));
    }


    @Test
    public void testGetJumps() {
        Board b0 = new Board();
        b0.setPieces("bw-ww bwwww ---ww -bb-b bbbbb", PieceColor.BLACK);
        ArrayList<Move> exp = new ArrayList<>();
        exp.add(Move.parseMove("a1-c3-c1-a1"));
        exp.add(Move.parseMove("a1-c1-a3"));
        exp.add(Move.parseMove("a1-c1-c3-a1"));
        ArrayList<Move> moves = b0.getMoves();
        assertEquals(exp, moves);
    }

    @Test
    public void testGetNonCapMoves() {
        Board b0 = new Board();
        ArrayList<Move> exp = new ArrayList<>();
        exp.add(Move.parseMove("b2-c3"));
        exp.add(Move.parseMove("c2-c3"));
        exp.add(Move.parseMove("d2-c3"));
        exp.add(Move.parseMove("d3-c3"));
        assertEquals(exp , b0.getMoves());
    }

    @Test
    public void testGetMove() {
        Board b0 = new Board();
        b0.setPieces("b---b b---- ----- ----w -w--w", PieceColor.BLACK);
        ArrayList<Move> moves = b0.getMoves();
        for (Move m: moves) {
            assertEquals(Move.parseMove("a2-b2"), m);
        }
        b0.setPieces("b---b b---- ----- ----w -w--w", PieceColor.WHITE);
        moves = b0.getMoves();
        assertEquals(1, moves.size());
        assertEquals(true, moves.contains(Move.parseMove("e4-d4")));
        assertEquals(false, moves.contains(Move.parseMove("e4-d5")));
        b0.makeMove(Move.parseMove("e4-d4"));
        assertEquals(false, b0.legalMove(Move.parseMove("d4-e4")));
    }

    @Test
    public void testGameOver() {
        Board b0 = new Board();
        b0.setPieces("----- ----- --b-- ----- -----", PieceColor.WHITE);
        assertEquals(true, b0.gameOver());
        b0.setPieces("----- ----- --b-- ----- -----", PieceColor.BLACK);
        assertEquals(false, b0.gameOver());
    }

    @Test
    public void testBackwardsMove() {
        Board b0 = new Board();
        b0.setPieces("----- --b-- ----- -w--- -----", PieceColor.WHITE);
        b0.makeMove(Move.parseMove("b4-c4"));
        b0.makeMove(Move.parseMove("c2-b2"));
        assertEquals(false, b0.legalMove(Move.parseMove("c4-b4")));
        assertEquals(false, b0.legalMove(Move.parseMove("b2-c2")));
    }

    @Test
    public void testGetMoves1() {
        Board b = new Board();
        b.setPieces("wwwww wwww- b-w-b bbbb- bbbb-", currMove);
        ArrayList<Move> moves = b.getMoves();
        List<Move> exp = new ArrayList<>();
        exp.add(Move.parseMove("c3-e5"));
        for (int i = 0; i < exp.size(); i++) {
            assertEquals(true, exp.contains(moves.get(i)));
            assertEquals(true, moves.contains(exp.get(i)));
        }
        exp.clear();

        b.setPieces("wwwww www-w bbwww bbbbb bbbbb", PieceColor.BLACK);
        moves = b.getMoves();
        exp.add(Move.parseMove("d4-d2"));
        exp.add(Move.parseMove("b4-d2"));
        for (int i = 0; i < exp.size(); i++) {
            assertEquals(true, exp.contains(moves.get(i)));
            assertEquals(true, moves.contains(exp.get(i)));
        }
        exp.clear();

        b.setPieces("wwwww www-w bbwww bbbbb bbbbb", PieceColor.BLACK);
        moves = b.getMoves();
        exp.add(Move.parseMove("d4-d2"));
        exp.add(Move.parseMove("b4-d2"));
        for (int i = 0; i < exp.size(); i++) {
            assertEquals(true, exp.contains(moves.get(i)));
            assertEquals(true, moves.contains(exp.get(i)));
        }
        exp.clear();
    }

    @Test
    public void testGetMoves2() {
        Board b = new Board();
        List<Move> exp = new ArrayList<>();
        b.setPieces("--www b-www --bww -bb-b bbbbb", PieceColor.WHITE);
        exp.add(Move.parseMove("d3-b3"));
        ArrayList<Move> moves = b.getMoves();
        for (int i = 0; i < exp.size(); i++) {
            assertEquals(true, exp.contains(moves.get(i)));
            assertEquals(true, moves.contains(exp.get(i)));
        }
        exp.clear();

        b.setPieces("----- ---bb -bw-- b--b- -----", PieceColor.WHITE);
        exp.add(Move.parseMove("c3-a3-a5"));
        exp.add(Move.parseMove("c3-e5"));
        exp.add(Move.parseMove("c3-e1-e3-c5"));
        moves = b.getMoves();
        for (int i = 0; i < exp.size(); i++) {
            assertEquals(true, exp.contains(moves.get(i)));
            assertEquals(true, moves.contains(exp.get(i)));
        }
        exp.clear();

        b.setPieces("-bww- b-bww -b-wb -b--b bb-bb", PieceColor.WHITE);
        exp.add(Move.parseMove("d2-b2"));
        exp.add(Move.parseMove("c1-a1-a3-c5"));
        exp.add(Move.parseMove("c1-a1-a3-c3-c1"));
        exp.add(Move.parseMove("c1-c3-a3-c5"));
        exp.add(Move.parseMove("c1-c3-a3-a1-c1"));
        moves = b.getMoves();
        for (int i = 0; i < exp.size(); i++) {
            assertEquals(true, exp.contains(moves.get(i)));
            assertEquals(true, moves.contains(exp.get(i)));
        }
        exp.clear();
    }

    @Test
    public void testUndo() {
        Board b = new Board();
        for (int i = 0; i < undoTest.length; i++) {
            Move m = Move.parseMove(undoTest[i]);
            b.makeMove(m);
        }
        System.out.println(b.toString());
        for (int i = 0; i < undoTest.length; i++) {
            b.undo();
        }
        assertEquals(new Board(), b);
    }

    @Test
    public void testgetNonCapMoves() {
        Board b = new Board();
        for (int i = 0; i < moveTest.length; i++) {
            b.makeMove(Move.parseMove(moveTest[i]));
        }
        for (int i = 0; i < 1; i++) {
            b.undo();
        }
    }

}

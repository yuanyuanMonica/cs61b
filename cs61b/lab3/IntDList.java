
public class IntDList {

    protected DNode _front, _back;

    public IntDList() {
        _front = _back = null;
    }

    public IntDList(Integer... values) {
        _front = _back = null;
        for (int val : values) {
            insertBack(val);
        }
    }

    /** Returns the first item in the IntDList. */
    public int getFront() {
        return _front._val;
    }

    /** Returns the last item in the IntDList. */
    public int getBack() {
        return _back._val;
    }

    /** Return value #I in this list, where item 0 is the first, 1 is the
     *  second, ...., -1 is the last, -2 the second to last.... */
    public int get(int i) {
        // Your code here
        if(i >= 0){
            IntDList.DNode result = _front;
            for (int j = 0; j < i; j ++){
                result = result._next;
            }
            return result._val;
        }
        else{
            IntDList.DNode result = _back;
            for (int j = 1; j < -i; j ++){
                result = result._prev;
            }
            return result._val;
        }
    }

    /** The length of this list. */
    public int size() {
        // Your code here
        if(_front == null){
            return 0;
        }
        else if(_front == _back){
            return 1;
        }
        else{
            DNode curr = _front;
            int num = 1;
            while (curr != _back){
                curr = curr._next;
                num ++;
            }
            return num;
        }
    }

    /** Adds D to the front of the IntDList. */
    public void insertFront(int d) {
        // Your code here
        DNode new_front = new DNode(d);
        if (_front == null){
            _back = new_front;
            _front = new_front;
        }
        else{
            _front._prev = new_front;
            new_front._next = _front;
            _front = new_front;
        }
    }

    /** Adds D to the back of the IntDList. */
    public void insertBack(int d) {
        // Your code here
        DNode new_back = new DNode(d);
        if (_back == null){
            _back = new_back;
            _front = new_back;
        }
        else{
            _back._next = new_back;
            new_back._prev = _back;
            _back = new_back;
        }
    }

    /** Removes the last item in the IntDList and returns it.
     * This is an extra challenge problem. */
    public int deleteBack() {
        return 0;   // Your code here

    }

    /** Returns a string representation of the IntDList in the form
     *  [] (empty list) or [1, 2], etc. 
     * This is an extra challenge problem. */
    public String toString() {
        return null;   // Your code here
    }

    /* DNode is a "static nested class", because we're only using it inside
     * IntDList, so there's no need to put it outside (and "pollute the
     * namespace" with it. This is also referred to as encapsulation.
     * Look it up for more information! */
    protected static class DNode {
        protected DNode _prev;
        protected DNode _next;
        protected int _val;

        private DNode(int val) {
            this(null, val, null);
        }

        private DNode(DNode prev, int val, DNode next) {
            _prev = prev;
            _val = val;
            _next = next;
        }
    }

}

package db61b;

import org.junit.Test;
import ucb.junit.textui;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

/** The suite of all JUnit tests for the qirkat package.
 *  @author P. N. Hilfinger
 */
public class UnitTest {

    @Test
    public void databaseTest() {
        Table firstTable = new Table(new String[]{"a"});
        Database db = new Database();
        db.put("_firstTable", firstTable);
        assertEquals(db.get("_firstTable"), firstTable);

        Table sameName = new Table(new String []{"a", "b"});
        db.put("_firstTable", sameName);
        assertEquals(db.get("_firstTable"), sameName);

    }

    @Test
    public void tableTest1Add1() {
        Table t1 = new Table(new String[]{"First_Column", "Second_Column"});
        assertEquals(t1.columns(), 2);
        assertEquals(t1.size(), 0);
        t1.add(new String[]{"value11", "value12"});
        t1.add(new String[]{"value21", "value22"});
        assertEquals(t1.size(), 2);
        assertEquals(t1.columns(), 2);
        try {
            t1.add(new String[]{
                "WrongLength1",
                "WrongLength2",
                "WrongLength3"});
        } catch (DBException e) {
            System.out.println(e);
        }
        t1.add(new String[]{"value51", "value52"});
        assertEquals(t1.add(new String[]{"value51", "value32"}), true);
        assertEquals(t1.size(), 4);
        assertEquals(t1.add(new String[]{"value51", "value52"}), false);
        assertEquals(t1.size(), 4);

        t1.add(new String[]{"value31", "value32"});
        t1.add(new String[]{"value41", "value42"});
        t1.print();
        assertEquals(t1.size(), 6);
        assertEquals(t1.getTitle(1), "Second_Column");
        assertEquals(t1.findColumn("First_Column"), 0);
        assertEquals(t1.findColumn("aaaaaa"), -1);

    }

    @Test
    public void tableTestAdd2() {
        Table t1 = new Table(new String[]{"First_Column", "Second_Column"});
        Table t3 = new Table(new String[]{"_1st"});
        Table t4 = new Table(new String[]{"Col_1", "Col_2"});
        assertEquals(t3.add(new String[]{"value11"}), true);
        assertEquals(t3.add(new String[]{"value21"}), true);
        assertEquals(t3.add(new String[]{"value31"}), true);
        assertEquals(t3.add(new String[]{"value41"}), true);
        assertEquals(t4.add(new String[]{"value11", "value12"}), true);
        assertEquals(t4.add(new String[]{"value31", "value32"}), true);
        assertEquals(t4.add(new String[]{"value41", "value42"}), true);
        t3.print();
        t4.print();
        List<Column> c1 = new ArrayList<Column>();
        List<Column> c2 = new ArrayList<Column>();
        List<Column> badList = new ArrayList<Column>();
        c1.add(new Column("_1st", t3, t3, t4));
        c1.add(new Column("Col_2", t3, t3, t4));
        c2.add(new Column("Col_1", t3, t4));
        c2.add(new Column("Col_2", t3, t4));
        badList.add(new Column("_1st", t3, t4));
        assertEquals(t1.add(c1, 0, 9999999, 0), true);
        assertEquals(t1.add(c2, 9999999, 1), true);
        try {
            t1.add(badList, 999999, 999999);
        } catch (DBException e) {
            System.out.println(e);
        }
        Table exp = new Table(new String[]{"First_Column", "Second_Column"});
        exp.add(new String[]{"value11", "value12"});
        exp.add(new String[]{"value31", "value32"});
        assertEquals(exp.columns(), t1.columns());
        assertEquals(exp.size(), t1.size());
        t1.print();
        for (int c = 0; c < exp.columns(); c++) {
            for (int r = 0; r < exp.size(); r++) {
                assertEquals(exp.get(r, c), t1.get(r, c));
            }
        }
    }

    @Test
    public void readwriteTableTest() {
        Table t1 = new Table(new String[]{"First", "Second", "Third"});
        t1.add(new String[]{"Name1", "Name2", "Name3"});
        t1.add(new String[]{"Age1", "Age2", "Age3"});
        t1.add(new String[]{"Gender1", "Gender2", "Gender3"});
        t1.add(new String[]{"Degree1", "Degree2", "Degree3"});
        t1.add(new String[]{"Location1", "Location2", "Location3"});
        System.out.println("");
        Table t2 = Table.readTable("../testing/Test");
        for (int i = 0; i < t1.columns(); i++) {
            for (int j = 0; j < t1.size(); j++) {
                assertEquals(t2.get(j, i), t1.get(j, i));
            }
        }
        t1.writeTable("T");
        Table t3 = Table.readTable("T");
        t3.print();
    }

    /** Run the JUnit tests in this package. Add xxxTest.class entries to
     *  the arguments of runClasses to run other JUnit tests. */
    public static void main(String[] ignored) {
        textui.runClasses(UnitTest.class);
    }

}

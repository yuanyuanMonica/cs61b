import static org.junit.Assert.*;
import org.junit.Test;

public class CompoundInterestTest {

    @Test
    public void testNumYears() {
        /** Sample assert statement for comparing integers.

        assertEquals(0, 0); */
        assertEquals(0, CompoundInterest.numYears(2015));
        assertEquals(2, CompoundInterest.numYears(2017));
    }

    @Test
    public void testFutureValue() {
        double tolerance = 0.01;
        assertEquals(0, CompoundInterest.futureValue(0, 12, 2017), tolerance);
        assertEquals(1000, CompoundInterest.futureValue(1000, 0, 2020), tolerance);
        assertEquals(902.5, CompoundInterest.futureValue(1000, -5, 2017), tolerance);
        assertEquals(1000, CompoundInterest.futureValue(1000, 12, 2015), tolerance);
        assertEquals(12.544, CompoundInterest.futureValue(10, 12, 2017), tolerance);
    }

    @Test
    public void testFutureValueReal() {
        double tolerance = 0.01;
        assertEquals(0, CompoundInterest.futureValueReal(0, 12, 2017, 3), tolerance);
        assertEquals(1000, CompoundInterest.futureValueReal(1000, 0, 2020, 0), tolerance);
        assertEquals(884.54, CompoundInterest.futureValueReal(1000, -5, 2017, 1), tolerance);
        assertEquals(920.64, CompoundInterest.futureValueReal(1000, -5, 2017, -1), tolerance);
        assertEquals(1000, CompoundInterest.futureValueReal(1000, 12, 2015, 3), tolerance);
        assertEquals(11.80, CompoundInterest.futureValueReal(10, 12, 2017, 3), tolerance);
    }


    @Test
    public void testTotalSavings() {
        double tolerance = 0.01;
        assertEquals(0, CompoundInterest.totalSavings(0, 2020, 3), tolerance);
        assertEquals(1000, CompoundInterest.totalSavings(1000, 2015, 3), tolerance);
        assertEquals(6000, CompoundInterest.totalSavings(1000, 2020, 0), tolerance);
        assertEquals(2910.9, CompoundInterest.totalSavings(1000, 2017, -3), tolerance);
        assertEquals(16550, CompoundInterest.totalSavings(5000, 2017, 10), tolerance);
    }

    @Test
    public void testTotalSavingsReal() {
        double tolerance = 0.01;
        assertEquals(0, CompoundInterest.totalSavingsReal(0, 2020, 3, 3), tolerance);
        assertEquals(1000, CompoundInterest.totalSavingsReal(1000, 2015, 10, 10), tolerance);
        assertEquals(3310, CompoundInterest.totalSavingsReal(1000, 2017, 10, 0), tolerance);
        assertEquals(2970.1, CompoundInterest.totalSavingsReal(1000, 2017, 10, 10), tolerance);
        assertEquals(3674.1, CompoundInterest.totalSavingsReal(1000, 2017, 10, -10), tolerance);
    }


    /* Run the unit tests in this file. */
    public static void main(String... args) {
        System.exit(ucb.junit.textui.runClasses(CompoundInterestTest.class));
    }
}

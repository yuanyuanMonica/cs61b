import static org.junit.Assert.*;
import org.junit.Test;

import javax.swing.text.html.InlineView;

public class IntListTest {

    /** Sample test that verifies correctness of the IntList.list static
     *  method. The main point of this is to convince you that
     *  assertEquals knows how to handle IntLists just fine.
     */

    @Test
    public void testList() {
        IntList one = new IntList(1, null);
        IntList twoOne = new IntList(2, one);
        IntList threeTwoOne = new IntList(3, twoOne);

        IntList x = IntList.list(3, 2, 1);
        assertEquals(threeTwoOne, x);
    }

    /** Do not use the new keyword in your tests. You can create
     *  lists using the handy IntList.list method.
     *
     *  Make sure to include test cases involving lists of various sizes
     *  on both sides of the operation. That includes the empty list, which
     *  can be instantiated, for example, with
     *  IntList empty = IntList.list().
     *
     *  Keep in mind that dcatenate(A, B) is NOT required to leave A untouched.
     *  Anything can happen to A.
     */

    @Test
    public void testDcatenate() {
        //Test for Both A and B is empty
        IntList l1 = IntList.list();
        IntList l2 = IntList.list();
        assertEquals(IntList.list(), IntList.dcatenate(l1, l2));

        //Test for A is empty, B is not
        l1 = IntList.list();
        l2 = IntList.list(1);
        assertEquals(IntList.list(1), IntList.dcatenate(l1, l2));
        l2 = IntList.list(1, 2, 3);
        assertEquals(IntList.list(1, 2, 3), IntList.dcatenate(l1, l2));

        //Test for B is empty, A is not
        l2 = IntList.list();
        l1 = IntList.list(2);
        assertEquals(IntList.list(2), IntList.dcatenate(l1, l2));
        l1 = IntList.list(2, 4, 8);
        assertEquals(IntList.list(2, 4, 8), IntList.dcatenate(l1, l2));

        //Test for neither is empty;
        l1 = IntList.list(7,8,9);
        l2 = IntList.list(10, 11, 12);
        assertEquals(IntList.list(7, 8, 9, 10, 11, 12), IntList.dcatenate(l1, l2));


    }

    /** Tests that subtail works properly. Again, don't use new.
     *
     *  Make sure to test that subtail does not modify the list.
     */

    @Test
    public void testSubtail() {
        //Test for empty list
        IntList l1 = IntList.list();
        assertEquals(IntList.list(), IntList.subTail(l1, 0));
        assertEquals(IntList.list(), l1);

        //Test for start larger than length of list
        IntList l2 = IntList.list(1);
        assertEquals(IntList.list(), IntList.subTail(l2, 1));
        assertEquals(IntList.list(1), l2);

        //Test for start = 0
        IntList l3 = IntList.list(1, 2, 3);
        assertEquals(IntList.list(1, 2, 3), IntList.subTail(l3, 0));
        assertEquals(IntList.list(1, 2, 3), l3);

        //Test for start from the last element
        assertEquals(IntList.list(3), IntList.subTail(l3, 2));
        assertEquals(IntList.list(1, 2, 3), l3);
    }

    /** Tests that sublist works properly. Again, don't use new.
     *
     *  Make sure to test that sublist does not modify the list.
     */

    @Test
    public void testSublist() {
        //Test for len = 0
        IntList l1 = IntList.list(1, 2, 3);
        assertEquals(IntList.list(), IntList.sublist(l1, 0, 0));
        assertEquals(IntList.list(1, 2, 3), l1);

        //Test for len = length of the list
        assertEquals(IntList.list(1, 2, 3), IntList.sublist(l1, 0, 3));
        assertEquals(IntList.list(1, 2, 3), l1);

        //Test for start from middle
        IntList l2 = IntList.list(1, 2, 3, 4, 5);
        assertEquals(IntList.list(2, 3, 4), IntList.sublist(l2, 1, 3));
        assertEquals(IntList.list(1, 2, 3, 4, 5), l2);
        assertEquals(IntList.list(3), IntList.sublist(l2, 2, 1));
        assertEquals(IntList.list(1, 2, 3, 4, 5), l2);



    }

    /** Tests that dSublist works properly. Again, don't use new.
     *
     *  As with testDcatenate, it is not safe to assume that list passed
     *  to dSublist is the same after any call to dSublist
     */

    @Test
    public void testDsublist() {
        //Test for len = 0
        IntList l1 = IntList.list(1, 2, 3, 4, 5);
        assertEquals(IntList.list(), IntList.dsublist(l1, 3, 0));

        //Test for len = 1
        IntList l2 = IntList.list(1, 2, 3, 4, 5);
        assertEquals(IntList.list(4), IntList.dsublist(l2, 3, 1));

        //Test for start = 0
        l1 = IntList.list(1, 2, 3, 4, 5);
        assertEquals(IntList.list(1, 2, 3, 4), IntList.dsublist(l1, 0, 4));

        //Test for start in the middle
        l1 = IntList.list(1, 2, 3, 4, 5);
        assertEquals(IntList.list(2, 3, 4), IntList.dsublist(l1, 1, 3));


    }


    /* Run the unit tests in this file. */
    public static void main(String... args) {
        System.exit(ucb.junit.textui.runClasses(IntListTest.class));
    }
}

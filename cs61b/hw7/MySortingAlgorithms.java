import com.sun.tools.javac.code.Attribute;

import java.util.*;

/**
 * Class containing all the sorting algorithms from 61B to date.
 *
 * You may add any number instance variables and instance methods
 * to your Sorting Algorithm classes.
 *
 * You may also override the empty no-argument constructor, but please
 * only use the no-argument constructor for each of the Sorting
 * Algorithms, as that is what will be used for testing.
 *
 * Feel free to use any resources out there to write each sort,
 * including existing implementations on the web or from DSIJ.
 *
 * All implementations except Distribution Sort adopted from Algorithms,
 * a textbook by Kevin Wayne and Bob Sedgewick. Their code does not
 * obey our style conventions.
 */
public class MySortingAlgorithms {

    /**
     * Java's Sorting Algorithm. Java uses Quicksort for ints.
     */
    public static class JavaSort implements SortingAlgorithm {
        @Override
        public void sort(int[] array, int k) {
            Arrays.sort(array, 0, k);
        }

        @Override
        public String toString() {
            return "Built-In Sort (uses quicksort for ints)";
        }
    }

    /** Insertion sorts the provided data. */
    public static class InsertionSort implements SortingAlgorithm {
        @Override
        public void sort(int[] array, int k) {
            // FIXME
            int sorted_index = 0;
            for (int i = 1; i < k; i++) {
                int curr = array[i];
                for (int j = sorted_index; j >= 0; j--) {
                    if (curr < array[j]) {
                        int temp = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = temp;
                    }else if (curr > array[j]) {
                        break;
                    }
                }
//                if (curr < array[0]) {
//                    int temp = array[i];
//                    array[i] = array[0];
//                    array[0] = temp;
//                }
                sorted_index ++;
            }
        }

        @Override
        public String toString() {
            return "Insertion Sort";
        }
    }

    /**
     * Selection Sort for small K should be more efficient
     * than for larger K. You do not need to use a heap,
     * though if you want an extra challenge, feel free to
     * implement a heap based selection sort (i.e. heapsort).
     */
    public static class SelectionSort implements SortingAlgorithm {
        @Override
        public void sort(int[] array, int k) {
            // FIXME
            for (int i = 0; i < k; i++) {
                //iterate through array length to find the index of the smallest number
                //index of the minimum number
                int min = i;
                for (int j = i; j < k; j++) {
                    //if founded, assign it to min
                    if (array[j] < array[min]) {
                        min = j;
                    }
                }
                //swap the minimum number to the place
                int temp = array[min];
                array[min] = array[i];
                array[i] = temp;
            }
        }

        @Override
        public String toString() {
            return "Selection Sort";
        }
    }

    /** Your mergesort implementation. An iterative merge
      * method is easier to write than a recursive merge method.
      * Note: I'm only talking about the merge operation here,
      * not the entire algorithm, which is easier to do recursively.
      */
    public static class MergeSort implements SortingAlgorithm {
        @Override
        public void sort(int[] array, int k) {
            // FIXME
            if (k != 1) {
                sort(array, k / 2);
                int[] firstHalf = Arrays.copyOfRange(array, 0, k / 2);
                int[] secondHalf = Arrays.copyOfRange(array, k / 2, k);
                sort(secondHalf, secondHalf.length);
                merge(firstHalf, secondHalf, array);
            }
        }

        // may want to add additional methods
        //assume arr1 and arr2 has the same length merged have double their length
        // and merge them together
        private void merge(int[] arr1, int[] arr2, int[] merged) {
            int iter1 = 0;
            int iter2 = 0;
            int len = arr1.length + arr2.length;
            int i = 0;
            while (i < len) {
                if (iter1 >= arr1.length
                    && iter2 >= arr2.length) {
                    break;
                } else if (iter1 < arr1.length
                        && iter2 >= arr2.length) {
                    merged[i] = arr1[iter1];
                    iter1++;
                } else if (iter1 >= arr1.length
                        && iter2 < arr2.length) {
                    merged[i] = arr2[iter2];
                    iter2++;
                } else {
                    if (arr1[iter1] > arr2[iter2]) {
                        merged[i] = arr2[iter2];
                        iter2++;
                    } else {
                        merged[i] = arr1[iter1];
                        iter1++;
                    }
                }
                i++;
            }
        }

        @Override
        public String toString() {
            return "Merge Sort";
        }
    }

    /**
     * Your Distribution Sort implementation.
     * You should create a count array that is the
     * same size as the value of the max digit in the array.
     */
    public static class DistributionSort implements SortingAlgorithm {
        @Override
        public void sort(int[] array, int k) {
            // FIXME: to be implemented
        }

        // may want to add additional methods

        @Override
        public String toString() {
            return "Distribution Sort";
        }
    }

    /** Your Heapsort implementation.
     */
    public static class HeapSort implements SortingAlgorithm {
        @Override
        public void sort(int[] array, int k) {
            // FIXME
        }

        @Override
        public String toString() {
            return "Heap Sort";
        }
    }

    /** Your Quicksort implementation.
     */
    public static class QuickSort implements SortingAlgorithm {
        @Override
        public void sort(int[] array, int k) {
            // FIXME
            sort(array, 0, k - 1 );
        }

        private void sort(int[] array, int start, int end) {
            int pivot = start;
            int i = pivot + 1;
            int j = end;
            while (i <= end && j >= start && i != j) {
                if (array[i] <= array[pivot]) {
                    i++;
                } else if (array[j] >= array[pivot]) {
                    j--;
                } else {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                    i++;
                    j--;
                }

            }

            int new_pivot;
            if (array[i] >= array[pivot]) {
                new_pivot = i - 1;
            } else {
                new_pivot = i;
            }
            int old_pivot = array[pivot];
            array[pivot] = array[new_pivot];
            array[new_pivot] = old_pivot;
            pivot = new_pivot;


            if (pivot - start > 1) {
                sort(array, start, pivot);
            }
            if (end - pivot > 1) {
                sort(array, pivot + 1, end);
            }
        }

        @Override
        public String toString() {
            return "Quicksort";
        }
    }

    /* For radix sorts, treat the integers as strings of x-bit numbers.  For
     * example, if you take x to be 2, then the least significant digit of
     * 25 (= 11001 in binary) would be 1 (01), the next least would be 2 (10)
     * and the third least would be 1.  The rest would be 0.  You can even take
     * x to be 1 and sort one bit at a time.  It might be interesting to see
     * how the times compare for various values of x. */

    /**
     * LSD Sort implementation.
     */
    public static class LSDSort implements SortingAlgorithm {
        @Override
        public void sort(int[] a, int k) {
            // FIXME
            Queue<Integer> [] buckets = new Queue [10];
            for (int i = 0; i < 10; i++) {
                buckets[i] = new LinkedList<Integer>();
            }
            int num_digits = 1;
            boolean sorted = false;
            while (!sorted) {
                sorted = sort(a, num_digits, buckets, k);
                num_digits += 1;
            }
        }

        private boolean sort(int[] a, int num_digits, Queue<Integer>[] buckets, int upper_bound) {
            boolean sorted = false;
            for (int i = 0; i < upper_bound; i++) {
                int curr = a[i];
                int temp = curr;
                int divi = 10;
                for (int j = 1; j < num_digits; j++) {
                    temp /= 10;
                }
                if (temp / divi != 0) {
                    int d = temp % divi;
                    buckets[d].offer(curr);
                } else {
                    buckets[temp].offer(curr);
                }
            }
            if (buckets[0].size() == upper_bound) {
                sorted = true;
            }
            int i = 0;
            for (Queue<Integer> b: buckets) {
                while (b.peek() != null) {
                    a[i] = b.poll();
                    i++;
                }
            }
            return sorted;
        }

        @Override
        public String toString() {
            return "LSD Sort";
        }
    }

    /**
     * MSD Sort implementation.
     */
    public static class MSDSort implements SortingAlgorithm {
        @Override
        public void sort(int[] a, int k) {
            // FIXME
        }

        @Override
        public String toString() {
            return "MSD Sort";
        }
    }

    /** Exchange A[I] and A[J]. */
    private static void swap(int[] a, int i, int j) {
        int swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

}

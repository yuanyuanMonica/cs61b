1. Quicksort, BSTs, Mergesort

a. List all comparisons performed by Quicksort when sorting [5, 3, 2, 1, 7, 8, 4, 6] using the stable partitioning procedure described in class. The answers for the first partitioning operation are provided for you. The order for which pair does not matter (either 5-3 or 3-5 is fine). You may find running the provided Quicksort class to be useful. 

partition from index 0 to index 7 5-3, 5-2, 5-1, 5-7, 5-8, 5-4, 5-6,
partition from index 0 to index 3 3-2, 3-1, 3-4,
partition from index 0 to index 1 1-2,
partition from index 5 to index 7 7-8, 7-6.


b. List all comparisons performed when inserting [5, 3, 2, 1, 7, 8, 4, 6] into a BST. Give your comparisons in the order they occur. The first few have been provided for you:

insert 5 //nothing
insert 3 5-3,
insert 2 5-2, 3-2,
insert 1 5-1, 3-1, 2-1,
insert 7 5-7,
insert 8 5-8, 7-8,
insert 4 5-4, 3-4,
insert 6 5-6, 7-6.


c. What do you observe about the set of comparisons performed by Quicksort vs. BST insertion?

They are the same set and also the pivots are the roots of the (sub)tree in BST.

d. True or false: Suppose we have an array x which, when inserted into a BST from x[0..], results in a bushy BST. For any such array x, quicksorting x using the leftmost item as a pivot and the partitioning algorithm from part a will always be fast (i.e. N log N time instead of N^2).

True

e. Give an example of a comparison performed by Mergesort that is not given in your answers to 1a or 1b.

4-6
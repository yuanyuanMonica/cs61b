import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;

/** HW #8, Problem 3.
 *  @author Xiyuan Wang
  */
public class Intervals {
    /** Assuming that INTERVALS contains two-element arrays of integers,
     *  <x,y> with x <= y, representing intervals of ints, this returns the
     *  total length covered by the union of the intervals. */
    public static int coveredLength(List<int[]> intervals) {
        // REPLACE WITH APPROPRIATE STATEMENTS.
        Collections.sort(intervals, new arrayComparator());
        int start = intervals.get(0)[0];
        int end = intervals.get(0)[1];
        int length = end - start;
        for (int[] i: intervals) {
            if (i[0] <= end) {
                if (i[1] > end) {
                    length += i[1] - end;
                    end = i[1];
                }
            } else {
                start = i[0];
                end = i[1];
                length += end - start;
            }
        }
        return length;
    }

    private static class arrayComparator implements Comparator<int[]> {
        public int compare(int[] o1, int[] o2) {
            if (o1[0] > o2[0]) {
                return 1;
            } else if (o1[0] == o2[0]) {
                return 0;
            } else {
                return -1;
            }
        }
        public boolean equals(Object obj) {
            return obj.equals(this);
        }
    }

    /** Test intervals. */
    static final int[][] INTERVALS = {
        {19, 30},  {8, 15}, {3, 10}, {6, 12}, {4, 5},
    };
    /** Covered length of INTERVALS. */
    static final int CORRECT = 23;

    /** Performs a basic functionality test on the coveredLength method. */
    @Test
    public void basicTest() {
        assertEquals(CORRECT, coveredLength(Arrays.asList(INTERVALS)));
    }

    /** Runs provided JUnit test. ARGS is ignored. */
    public static void main(String[] args) {
        System.exit(ucb.junit.textui.runClasses(Intervals.class));
    }

}

package gitlet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/** Structures to keep track of  Commits.
 * @author Xiyuan Wang
 * */
public class Branch implements Serializable {
    /** Name of the branch. */
    private String name;

    /** ID of latest committed commit on this branch. */
    private String latest;

    /** A Hashmap of pointers to all commits on this Branch,
     * Key: filename like "hw4",
     * Value: ID. */
    private List<String> commits = new ArrayList<>();

    /** Contructor of A Branch with N as name, L as latestCommit Id,
     * B as a list of commits to be added into the branch. */
    Branch(String n, String l, List<String> b) {
        name = n;
        latest = l;
        commits = b;
    }

    /** Contructor of A Branch with N as name, L as latestCommit Id,
     *  to be added into the branch. */
    Branch(String n, String l) {
        name = n;
        latest = l;
    }

    /** Return the name of Branch. */
    public String name() {
        return name;
    }

    /** Return the Latest Commit ID on this Branch. */
    public String latestCommitID() {
        return latest;
    }

    /** Add a Commit with Id as C. */
    public void addCommits(String c) {
        latest = c;
        commits.add(c);
    }

    /** Remove Commits till latest Commit Id same as C. */
    public void removeCommits(String c) {
        while (!latest.startsWith(c)) {
            int last = commits.size() - 1;
            commits.remove(last);
            latest = commits.get(commits.size() - 1);
        }
    }

    /** Return the list of Commits added to the Branch. */
    public List<String> getCommits() {
        return commits;
    }

    /** Return the Split point between this Branch and other. */
    public String splitPoint() {
        return commits.get(0);
    }

    /** Return A string representation of the Branch. */
    public String toString() {
        String ret = latest;
        return name + " " + ret;
    }

}

package gitlet;

import org.junit.Test;
public class GitletTest {
    /* Black Command */
    public Command blank = new Command("");

    /* Black CommandInterpreter */
    public CommandInterpreter cmd = new CommandInterpreter(new String[]{""});

    /* Blank Gitlet */
    public Gitlet gitlet = new Gitlet(cmd);

    @Test
    public void initTest() {
        String[] args = new String[]{"init"};
        cmd = new CommandInterpreter(args);
        gitlet = new Gitlet(cmd);
        gitlet.process();
    }
}

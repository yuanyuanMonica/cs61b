package gitlet;

import java.util.ArrayList;
import java.util.List;

/** Command that the program received.
 *  @author Xiyuan Wang
 */
public class Command {
    /** Type of Command to be carried. */
    private String _type;

    /** Operands passed in. */
    private List<String> _operands;

    /** Constructor for makeing a Command with Command type T.*/
    Command(String t) {
        _type = t;
        _operands = new ArrayList<String>();
    }

    /** Return the type of the Command. */
    public String type() {
        return _type;
    }

    /** Return the operands of the Command,
    return null if no operands.
     */
    public List<String> operands() {
        return _operands;
    }

    /** Set operands for this Command with operand OP. */
    public void setOperands(String op) {
        _operands.add(op);
    }
}

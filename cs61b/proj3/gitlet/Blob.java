package gitlet;

import java.io.File;
import java.io.Serializable;

/** Structure to store the contents of a file.
 * @author Xiyuan Wang
 */
public class Blob implements Serializable {
    /** Context of file. */
    private byte[] fileContext;

    /** File of user input. */
    private File file;

    /** SHA-1 ID of the Blob. */
    private String id;

    /** Contruction of a Blob corresponding to File F. */
    Blob(File f) {
        file = f;
        fileContext = Utils.readContents(f);
        id = Utils.sha1(file.getName(), fileContext);
    }

    /** Return the SHA-1 ID of this blob. */
    public String id() {
        return id;
    }

    /** Return the Filename of the File of user input. */
    public String filename() {
        return file.getName();
    }

    /** Return the file context stored in this Blob. */
    public byte[] context() {
        return fileContext;
    }

    /** Return the string representation of the Blob. */
    public String toString() {
        return "id: " + id + "\n"
                + "filename: " + filename();
    }

}

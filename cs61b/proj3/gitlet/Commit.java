package gitlet;

import java.io.File;
import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

/** Structure to keep tracked of different version of Blobs.
 * @author Xiyuan Wang
 */
public class Commit implements Serializable {
    /** Time of this commit. */
    private Date time;

    /** Log message. */
    private String logMessage;

    /** ID of Parent Commit. */
    private String parentID;

    /** ID of Second Parent Commit. */
    private String secondParent;

    /** ID of this Commit. */
    private String id = null;

    /** Blobs of this commit. */
    private HashMap<String, String> blobs = new HashMap<>();

    /** Construction of initial Commit with T as time. */
    Commit(long t) {
        logMessage = "initial commit";
        time = new Date(t);
        if (id == null) {
            setID(id());
        }
    }

    /** Construction of Commit with MSG as message, PRNT as parent ID,
     * B as blobs tracked by this Commit, T as the time.
     */
    Commit(String msg, String prnt, List<Blob> b, long t) {
        logMessage = msg;
        parentID = prnt;
        if (b != null) {
            Commit latestCommit = getCurrentBranchLatest();
            blobs = new HashMap<>(getValidBlobs(latestCommit.getBlobs()));
            for (Blob bl : b) {
                addBlob(bl);
            }
        }
        time = new Date(t);
    }

    /** Construction of a Merged Commit with with MSG as message,
     *  PRNT as parent ID, B as blobs tracked by this Commit, T as the time,
     *  SECONDPRNT as second parent ID.
     */
    Commit(String msg, String prnt, List<Blob> b, long t, String secondPrnt) {
        logMessage = msg;
        parentID = prnt;
        if (b != null) {
            Commit latestCommit = getCurrentBranchLatest();
            blobs = new HashMap<>(getValidBlobs(latestCommit.getBlobs()));
            for (Blob bl : b) {
                addBlob(bl);
            }
        }
        time = new Date(t);
        secondParent = secondPrnt;
    }

    /** Return the tracked blobs. */
    public HashMap<String, String> getBlobs() {
        return blobs;
    }

    /** Return the Log message. */
    public String getLogMsg() {
        return logMessage;
    }

    /** Add Blob B into this commit. */
    public void addBlob(Blob b) {
        if (blobs.containsKey(b.filename())) {
            blobs.replace(b.filename(), b.id());
        } else {
            blobs.put(b.filename(), b.id());
        }
    }

    /** Return the SHA-1 ID of the Commits. */
    public String id() {
        if (id == null) {
            setID(Utils.sha1(Utils.serialize(this)));
        }
        return id;
    }

    /** Set up the SHA1-ID with I as ID. */
    private void setID(String I) {
        id = I;
    }

    /** Return first Parent ID. */
    public String parent() {
        return parentID;
    }

    /** Return the latest Commit on Current Branch. */
    public Commit getCurrentBranchLatest() {
        String cb = Utils.readContentsAsString(
                new File("./.gitlet/.metadata", "currBranch"));
        Branch currB = Utils.readObject(
                new File("./.gitlet/.metadata/.branches", cb), Branch.class);
        return Utils.readObject(
                new File("./.gitlet/.commits",
                        currB.latestCommitID()), Commit.class);
    }

    /** Return valid Blobs from Blobs BL which are not from remove. */
    public HashMap<String, String> getValidBlobs(HashMap<String, String> bl) {
        for (String s: new File("./.gitlet/.metadata/.remove").list()) {
            if (bl.containsKey(s)) {
                bl.remove(s);
            }
        }
        return bl;
    }

    /** Return a string representation of the Commit. */
    public String toString() {
        SimpleDateFormat s = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy Z");
        if (secondParent == null) {
            return "===\n"
                    + "commit " + id + "\n"
                    + "Date: " + s.format(time) + "\n"
                    + logMessage;
        } else {
            return "===\n"
                    + "commit " + id + "\n"
                    + "Merge: " + parentID.substring(0, 7) + " "
                    + secondParent.substring(0, 7) + "\n"
                    + "Date: " + s.format(time) + "\n"
                    + logMessage;
        }
    }
}

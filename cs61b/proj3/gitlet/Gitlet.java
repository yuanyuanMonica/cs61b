package gitlet;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/** Structure to process Command.
 * @author Xiyuan Wang */
public class Gitlet {
    /** Working Directory in testing. */
    private File workingDirectory = new File(".", ".gitlet");

    /** .COMMITS Folder in Working Directory. */
    private File commits = new File(workingDirectory, ".commits");

    /** .BLOBS Folder in Working Directory. */
    private File blobs = new File(workingDirectory, ".blobs");

    /** .METADATA Folder in Working Directory. */
    private File meta = new File(workingDirectory, ".metadata");

    /** .STAGING Folder in .METADATA Folder. */
    private File stag = new File(meta, ".staging");

    /** .REMOVE Folder in .STAGING Folder. */
    private File remove = new File(meta, ".remove");

    /** .BRANCHES Folder in .METADATA Folder. */
    private File branch = new File(meta, ".branches");

    /** HEAD File in .METADATA Folder. */
    private File head = new File(meta, "head");

    /** CURRBRANCH File in .METADATA Folder. */
    private File currBranch = new File(meta, "currBranch");

    /** Shared Initial Commit. */
    private static Commit initC = new Commit(0);

    /** Command Interpreter using. */
    private CommandInterpreter cmd;

    /** Command that need to be done. */
    private Command command;

    /** Construction of Gitlet with C as CommandInterpreter. */
    Gitlet(CommandInterpreter c) {
        cmd = c;
    }

    /** Process a Command given by CommandInterpreter. */
    public void process() {
        command = cmd.parseCommand();
        if (!workingDirectory.exists() && !command.type().equals("init")) {
            throw new GitletException("Not in an initialized "
                    + "Gitlet directory.");
        }
        switch (command.type()) {
        case "init":
            init();
            break;
        case "add":
            add();
            break;
        case "commit":
            commit();
            break;
        case "rm":
            rm();
            break;
        case "checkout":
            checkout();
            break;
        case "branch":
            branch();
            break;
        case "rm-branch":
            rmBranch();
            break;
        case "status":
            status();
            break;
        case "log":
            log();
            break;
        case "global-log":
            globalLog();
            break;
        case "find":
            find();
            break;
        case "reset":
            reset();
            break;
        case "merge":
            merge();
            break;
        default:
            throw new GitletException("No command with that name exists.");
        }
    }

    /** Init Command. */
    public void init() {
        if (!workingDirectory.mkdir()) {
            throw new GitletException(" A Gitlet version-control system "
                    + "already exists in the current directory.");
        }

        commits.mkdir();
        blobs.mkdir();
        meta.mkdir();
        stag.mkdir();
        remove.mkdir();
        branch.mkdir();

        File f = new File(commits, initC.id());
        Utils.writeObject(f, initC);

        List<String> b = new ArrayList<>();
        b.add(initC.id());
        Branch master = new Branch("master", initC.id(), b);
        File m = new File(branch, master.name());
        Utils.writeObject(m, master);

        Utils.writeContents(head, initC.id());
        Utils.writeContents(currBranch, master.name());
    }

    /** Add Command. */
    public void add() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: add [filename]");
        }
        try {
            add(command.operands().get(0));
        } catch (GitletException e) {
            throw new GitletException("File does not exist.");
        }
    }

    /** add Helper with FILENAME. */
    public void add(String filename) {
        File f = new File(".", filename);
        if (!f.exists()) {
            if (new File(remove, command.operands().get(0)).exists()) {
                File f2 = new File(remove, command.operands().get(0));
                f2.delete();
            } else {
                throw new GitletException("File does not exist.");
            }
        } else {
            Blob b = new Blob(f);

            Commit ct = getCurrentCommit();
            HashMap<String, String> bl = ct.getBlobs();

            if (bl != null && bl.containsValue(b.id())) {
                File f1 = new File(stag, b.id());
                File f2 = new File(remove, b.filename());
                if (f1.exists()) {
                    Utils.restrictedDelete(f1);
                }
                if (f2.exists()) {
                    f2.delete();
                }
            } else {
                Utils.writeObject(new File(stag, b.filename()), b);
            }
        }
    }

    /** Commit Command. */
    public void commit() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: commit [message]");
        }

        String message = command.operands().get(0);

        commit(message, null);
    }

    /** commit Helper with MESSAGE and SECONDPARENTID. */
    public void commit(String message, String secondParentID) {
        if (stag.list().length == 0 && remove.list().length == 0) {
            throw new GitletException("No changes added to the commit.");
        }

        if (message.equals("")) {
            throw new GitletException("Please enter a commit message.");
        }
        Commit prevcmt = getCurrentCommit();
        List<Blob> bl = new ArrayList<>();
        String[] stagedFiles = stag.list();

        for (String s: stagedFiles) {
            File stagedFile = new File(stag, s);
            Blob tobe = Utils.readObject(stagedFile, Blob.class);
            bl.add(tobe);
            stagedFile.delete();
            Utils.writeObject(new File(blobs, tobe.id()), tobe);
        }

        Commit c;
        if (secondParentID == null) {
            c = new Commit(message, prevcmt.id(),
                    bl, System.currentTimeMillis());
        } else {
            c = new Commit(message, prevcmt.id(),
                    bl, System.currentTimeMillis(), secondParentID);
        }


        updateCurrentCommit(c.id());
        updateCurrentBranchAddCommit(c.id());

        Utils.writeObject(new File(commits, c.id()), c);

        for (String s: remove.list()) {
            File f3 = new File(remove, s);
            f3.delete();
        }
    }

    /** rm Command. */
    public void rm() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: rm [filename]");
        }

        String fn = command.operands().get(0);
        rm(fn);
    }

    /** rm Helper Function with FILENAME. */
    public void rm(String filename) {
        File f = new File(stag, filename);
        Commit currC = getCurrentCommit();
        if (!f.exists() && !currC.getBlobs().containsKey(filename)) {
            throw new GitletException("No reason to remove the file.");
        }

        if (f.exists()) {
            f.delete();
        }

        if (currC.getBlobs().containsKey(filename)) {
            File actual = new File(".", filename);
            if (actual.exists()) {
                Utils.writeObject(new File(remove, filename),
                        new Blob(new File(".", filename)));
                Utils.restrictedDelete(new File(".", filename));
            } else {
                String theBlob = currC.getBlobs().get(filename);
                Blob thatBlob = Utils.readObject(
                        new File(blobs, theBlob), Blob.class);
                Utils.writeObject(new File(remove, filename), thatBlob);
            }
        }
    }

    /** log Command. */
    public void log() {
        Commit c = getCurrentCommit();
        while (c.parent() != null) {
            System.out.println(c.toString() + "\n");
            c = getParentCommit(c);
        }
        System.out.println(initC.toString() + "\n");
    }

    /** branch Command. */
    public void branch() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: branch [branchname].");
        }
        String bn = command.operands().get(0);

        if (new File(branch, bn).exists()) {
            throw new GitletException("A branch with "
                    + "that name already exists.");
        }

        Commit ct = getCurrentCommit();
        Branch br = new Branch(bn, ct.id());
        br.addCommits(ct.id());

        Utils.writeObject(new File(branch, br.name()), br);
    }

    /** checkout Command. */
    public void checkout() {
        if (command.operands().size() == 2) {
            if (!command.operands().get(0).equals("--")) {
                throw new GitletException("Incorrect operands.");
            }
            String fn = command.operands().get(1);
            checkoutFile(fn);
        } else if (command.operands().size() == 3) {
            String commitID = command.operands().get(0);
            String filename = command.operands().get(2);
            if (!command.operands().get(1).equals("--")) {
                throw new GitletException("Incorrect operands.");
            }
            checkoutCommit(commitID, filename);
        } else if (command.operands().size() == 1) {
            String bn = command.operands().get(0);
            checkoutBranch(bn);
        } else {
            throw new GitletException("Please follow:\n checkout -- filename\n"
                    + " checkout commitID --filename\n checkout branchname");
        }

    }

    /** status Command. */
    public void status() {
        Branch currentBranch = getCurrentBranch();

        System.out.println("=== Branches ===");
        for (String s: Utils.plainFilenamesIn(branch)) {
            if (currentBranch.name().equals(s)) {
                System.out.println("*" + currentBranch.name());
            } else {
                System.out.println(s);
            }
        }
        System.out.println("");

        System.out.println("=== Staged Files ===");
        for (String s: Utils.plainFilenamesIn(stag)) {
            System.out.println(s);
        }
        System.out.println("");

        System.out.println("=== Removed Files ===");
        for (String s: Utils.plainFilenamesIn(remove)) {
            System.out.println(s);
        }
        System.out.println("");

        System.out.println("=== Modifications Not Staged For Commit ===");
        System.out.println("");

        System.out.println("=== Untracked Files ===");
        System.out.println("");

    }

    /** global-log Command. */
    public void globalLog() {
        for (String s: commits.list()) {
            Commit ct = Utils.readObject(new File(commits, s), Commit.class);
            System.out.println(ct.toString() + "\n");
        }
    }

    /** find Command. */
    public void find() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: find [commit msg]");
        }
        String msg = command.operands().get(0);
        String[] commitsID = commits.list();
        boolean founded = false;
        for (String s: commitsID) {
            Commit ct = Utils.readObject(new File(commits, s), Commit.class);
            if (ct.getLogMsg().equals(msg)) {
                founded = true;
                System.out.println(ct.id());
            }
        }
        if (!founded) {
            throw new GitletException("Found no commit with that message.");
        }
    }

    /** reset Command. */
    public void reset() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: reset [commit id]");
        }
        String resetcommitID = command.operands().get(0);
        if (!new File(commits, resetcommitID).exists()) {
            throw new GitletException("No commit with that id exists.");
        }
        checkoutBHelp(resetcommitID);

        for (String s: stag.list()) {
            rm(s);
        }

        updateCurrentBranchAddCommit(resetcommitID);
        updateCurrentCommit(resetcommitID);
    }

    /** rm-branch Command. */
    public void rmBranch() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: rm-branch [branchname]");
        }
        String branchname = command.operands().get(0);

        if (!new File(branch, branchname).exists()) {
            throw new GitletException("A branch with that "
                    + "name does not exist.");
        }

        Branch currBr = getCurrentBranch();
        if (currBr.name().equals(branchname)) {
            throw new GitletException("Cannot remove the current branch.");
        }

        File br = new File(branch, branchname);
        br.delete();
    }

    /** Merge command. */
    public void merge() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: merge [branchname]");
        }
        String brn = command.operands().get(0);
        if (!new File(branch, brn).exists()) {
            throw new GitletException("A branch with "
                    + "that name does not exist.");
        }
        Branch merge = Utils.readObject(new File(branch, brn), Branch.class);
        Branch currentBranch = getCurrentBranch();
        Commit sp = getSplitPoint(merge, currentBranch);
        if (stag.list().length != 0 || remove.list().length != 0) {
            throw new GitletException("You have uncommitted changes.");
        }

        if (currentBranch.name().equals(brn)) {
            throw new GitletException("Cannot merge a "
                    + "branch with itself.");
        }

        Commit ct = getCurrentCommit();
        Commit mergeCommit = Utils.readObject(new File(commits,
                merge.latestCommitID()), Commit.class);
        for (String s: mergeCommit.getBlobs().keySet()) {
            File working = new File(".", s);
            if (!ct.getBlobs().keySet().contains(s)
                    && working.exists()) {
                throw new GitletException
                ("There is an untracked file in the way;"
                        + " delete it or add it first.");
            }
        }

        if (currentBranch.getCommits().contains(merge.latestCommitID())) {
            throw new GitletException("Given branch is "
                    + "an ancestor of the current branch.");
        } else if (merge.getCommits().contains
                (currentBranch.latestCommitID())) {
            List<String> mergedCommits = merge.getCommits();
            int i = mergedCommits.indexOf(currentBranch.latestCommitID());
            for (int j = i + 1; j < mergedCommits.size(); j++) {
                currentBranch.addCommits(mergedCommits.get(j));
            }
            updateCurrentCommit(currentBranch.latestCommitID());
            System.out.println("Current branch fast-forwarded.");
        } else {
            mergeHelp(merge, currentBranch, sp);

            String msg = "Merged " + merge.name()
                    + " into " + currentBranch.name() + ".";
            commit(msg, merge.latestCommitID());
        }
    }



    /******** HELPER FUNCTIONS ********/

    /** Assuming the Current Branch File exists and Return Current Branch. */
    private Branch getCurrentBranch() {
        String cbn = Utils.readContentsAsString(currBranch);
        Branch br = Utils.readObject(new File(branch, cbn), Branch.class);
        return br;
    }

    /** Assuming the Current Commit File exists and Return Current Commit. */
    private Commit getCurrentCommit() {
        String headCommit = Utils.readContentsAsString(head);
        File ccf = new File(commits, headCommit);
        Commit ct = Utils.readObject(ccf, Commit.class);
        return ct;
    }

    /** Update the head to be Commit with CTID as ID
     * and write it back to HEAD File. */
    private void updateCurrentCommit(String ctid) {
        Utils.writeContents(head, ctid);
    }

    /** Update the currBranch to add Commit with CTID
     * and write it back to .Branches folder. */
    private void updateCurrentBranchAddCommit(String ctid) {
        String currB = Utils.readContentsAsString(currBranch);
        Branch cb = Utils.readObject(new File(branch, currB), Branch.class);
        cb.addCommits(ctid);
        Utils.writeObject(new File(branch, currB), cb);
        Utils.writeContents(currBranch, cb.name());
    }

    /** Return the parent Commit of passed-in Commit CT. */
    private Commit getParentCommit(Commit ct) {
        String p = ct.parent();
        return Utils.readObject(new File(commits, p), Commit.class);
    }

    /** Checkout filename as FN helper function. */
    private void checkoutFile(String fn) {
        Commit ct = getCurrentCommit();
        if (!ct.getBlobs().containsKey(fn)) {
            throw new GitletException("File does not exist in that commit.");
        }

        String blobid = ct.getBlobs().get(fn);
        Blob b = Utils.readObject(new File(blobs, blobid), Blob.class);
        Utils.writeContents(new File(".", fn), b.context());
    }

    /** Checkout COMMITID -- FILENAME helper function. */
    private void checkoutCommit(String commitID, String filename) {
        List<String> theFile = new ArrayList<String>();
        int foundedCommit = 0;

        for (String s: commits.list()) {
            if (foundedCommit > 1) {
                throw new GitletException("The specified Commit "
                        + "is not unique.");
            }
            if (s.startsWith(commitID)) {
                foundedCommit++;
                Commit ct = Utils.readObject
                        (new File(commits, s), Commit.class);
                if (ct.getBlobs().containsKey(filename)) {
                    theFile.add(ct.getBlobs().get(filename));
                }
            }
        }

        if (theFile.size() == 0 && foundedCommit != 0) {
            throw new GitletException("File does not exist in that commit.");
        } else if (foundedCommit == 0) {
            throw new GitletException("No commit with that id exists.");
        }

        Blob b = Utils.readObject(new File(blobs, theFile.get(0)), Blob.class);
        Utils.writeContents(new File(".", filename), b.context());

    }

    /** Checkout branchname as BN helper function. */
    private void checkoutBranch(String bn) {
        if (!new File(branch, bn).exists()) {
            throw new GitletException("No such branch exists.");
        } else if (getCurrentBranch().name().equals(bn)) {
            throw new GitletException
            ("No need to checkout the current branch.");
        } else {
            Branch newbranch = Utils.readObject
                    (new File(branch, bn), Branch.class);
            checkoutBHelp(newbranch.latestCommitID());

            Utils.writeContents(currBranch, newbranch.name());
            Utils.writeContents(head, newbranch.latestCommitID());
        }
    }

    /** Checkout Branch Helper with NEWCOMMITID as ID of its tip of commit.*/
    private void checkoutBHelp(String newcommitID) {
        Commit currCt = getCurrentCommit();
        HashMap<String, String> blbs = currCt.getBlobs();
        Commit newcommit = Utils.readObject
                (new File(commits, newcommitID), Commit.class);
        HashMap<String, String> newfiles = newcommit.getBlobs();

        for (String s: blbs.keySet()) {
            if (!newfiles.keySet().contains(s)) {
                Utils.restrictedDelete(new File(".", s));
            }
        }
        for (String s: newfiles.keySet()) {
            File f1 = new File(".", s);
            if (f1.exists()) {
                if (!blbs.keySet().contains(s)) {
                    String sha1 = Utils.sha1
                            (f1.getName(), Utils.readContents(f1));
                    String sha2 = newfiles.get(s);
                    if (!sha1.equals(sha2)) {
                        throw new GitletException
                        ("There is an untracked file in the way;"
                                        + " delete it or add it first.");
                    }
                } else {
                    Blob b1 = Utils.readObject
                            (new File(this.blobs, newfiles.get(s)), Blob.class);
                    Utils.writeContents(f1, b1.context());
                }
            } else {
                Blob b1 = Utils.readObject
                        (new File(this.blobs, newfiles.get(s)), Blob.class);
                Utils.writeContents(f1, b1.context());
            }
        }
    }

    /** Merge Helper with merged branch as MERGE,
     * current branch as CURRENTBRANCH, split point as SP. */
    private void mergeHelp(Branch merge, Branch currentBranch, Commit sp) {
        Commit mergedCommit = Utils.readObject(
                new File(commits, merge.latestCommitID()), Commit.class);
        Commit currCommit = getCurrentCommit();
        HashMap<String, String> mergedFiles = mergedCommit.getBlobs();
        HashMap<String, String> currFiles = currCommit.getBlobs();
        HashMap<String, String> spFiles = sp.getBlobs();
        for (String s: mergedFiles.keySet()) {
            if (!spFiles.containsKey(s)) {
                if (!currFiles.containsKey(s)) {
                    checkoutCommit(mergedCommit.id(), s);
                    add(s);
                } else if (currFiles.containsKey(s)
                        && !currFiles.get(s).equals(mergedFiles.get(s))) {
                    resolveConflicts(s, currFiles, mergedFiles);
                }
            }
        }
        for (String s: spFiles.keySet()) {
            if (mergedFiles.containsKey(s)
                    && !mergedFiles.get(s).equals(spFiles.get(s))) {
                if (currFiles.containsKey(s)) {
                    if (currFiles.get(s).equals(spFiles.get(s))) {
                        checkoutCommit(mergedCommit.id(), s);
                        add(s);
                    } else if (!currFiles.get(s).equals(spFiles.get(s))
                            && !currFiles.get(s).equals(mergedFiles.get(s))) {
                        resolveConflicts(s, currFiles, mergedFiles);
                    }
                } else {
                    resolveConflicts(s, currFiles, mergedFiles);
                }
            } else if (!mergedFiles.containsKey(s)) {
                if (currFiles.containsKey(s)
                        && currFiles.get(s).equals(spFiles.get(s))) {
                    rm(s);
                } else if (currFiles.containsKey(s)
                        && !currFiles.get(s).equals(spFiles.get(s))) {
                    resolveConflicts(s, currFiles, mergedFiles);
                }
            }
        }
    }

    /** Return the most recent common ancestor,
     * for both Branches, MERGE and CURRENTBRANCH. */
    private Commit getSplitPoint(Branch merge, Branch currentBranch) {
        String msp = merge.splitPoint();
        String cbsp = currentBranch.splitPoint();
        String sp;
        if (msp.equals(cbsp)) {
            sp = msp;
        } else if (msp.equals(initC.id()) && !cbsp.equals(initC.id())) {
            sp = cbsp;
        } else if (!msp.equals(initC.id()) && cbsp.equals(initC.id())) {
            sp = msp;
        } else {
            System.out.println("lucky me");
            sp = null;
        }
        return Utils.readObject(new File(commits, sp), Commit.class);
    }

    /** Resolve Conflict Merge with S as filename, CURRFILES as current files
     * MERGEDFILES as merged files . */
    private void resolveConflicts(String s, HashMap<String, String> currFiles,
                                  HashMap<String, String> mergedFiles) {
        System.out.println("Encountered a merge conflict.");
        boolean currContained = currFiles.keySet().contains(s);
        boolean mergedContained = mergedFiles.keySet().contains(s);
        if (currContained && !mergedContained) {
            Blob currContent = Utils.readObject
                    (new File(blobs, currFiles.get(s)), Blob.class);
            Utils.writeContents(new File(".", s), "<<<<<<< HEAD\n",
                    currContent.context(), "=======\n", "", ">>>>>>>\n");
        } else if (!currContained && mergedContained) {
            Blob mergedContent = Utils.readObject
                    (new File(blobs, mergedFiles.get(s)), Blob.class);
            Utils.writeContents(new File(".", s), "<<<<<<< HEAD\n",
                    "", "=======\n", mergedContent.context(), ">>>>>>>\n");
        } else if (currContained && mergedContained) {
            Blob currContent = Utils.readObject
                    (new File(blobs, currFiles.get(s)), Blob.class);
            Blob mergedContent = Utils.readObject
                    (new File(blobs, mergedFiles.get(s)), Blob.class);
            Utils.writeContents(new File(".", s), "<<<<<<< HEAD\n",
                    currContent.context(), "=======\n",
                    mergedContent.context(), ">>>>>>>\n");
        }
        add(s);
    }
}

package gitlet;

/** Simulator to interpret the Command passed in.
 * @author Xiyuan Wang
 */
public class CommandInterpreter {
    /** Arguments passed in. */
    private String[] args;

    /** Construction of a CommandInterpreter with A as args passed in. */
    CommandInterpreter(String[] a) {
        this.args = a;
    }

    /** Parse the passed in arguments into Command,
     * Return a Command. */
    public Command parseCommand() {
        Command cmd = new Command(args[0]);
        if (args.length > 1) {
            for (int i = 1; i < args.length; i++) {
                cmd.setOperands(args[i]);
            }
        }
        return cmd;
    }
}

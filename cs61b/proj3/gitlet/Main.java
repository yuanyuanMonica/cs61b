package gitlet;

/** Driver class for Gitlet, the tiny stupid version-control system.
 *  @author Xiyuan Wang
 */
public class Main {

    /** Usage: java gitlet.Main ARGS, where ARGS contains
     *  <COMMAND> <OPERAND> .... */
    public static void main(String... args) {
        if (args.length == 0) {
            System.out.println("Please enter a command.");
            return;
        }
        CommandInterpreter cmd = new CommandInterpreter(args);
        Gitlet gitlet = new Gitlet(cmd);
        try {
            gitlet.process();
        } catch (GitletException e) {
            System.out.println(e.getMessage());
        }
        return;
    }

}

import java.util.Observable;
/**
 *  @author Josh Hug
 */

public class MazeDepthFirstPaths extends MazeExplorer {
    /* Inherits public fields:
    public int[] distTo;
    public int[] edgeTo;
    public boolean[] marked;
    */
    private int s;
    private int t;
    private boolean targetFound = false;
    private Maze maze;


    public MazeDepthFirstPaths(Maze m, int sourceX, int sourceY, int targetX, int targetY) {
        super(m);
        maze = m;
        s = maze.xyTo1D(sourceX, sourceY);
        t = maze.xyTo1D(targetX, targetY);
        distTo[s] = 0;
        edgeTo[s] = s;
    }

    private void dfs(int v) {
        //marked the current vertex as visited
        marked[v] = true;
        //update the drawing
        announce();

        //check if we find the target vertex
        if (v == t) {
            targetFound = true;
        }
        //if so, then stop
        if (targetFound) {
            return;
        }
        //for every adjacent vertex for current vertex v
        //visit those that we didn't visit before
        for (int w : maze.adj(v)) {
            if (!marked[w]) {
                //if not visited before
                //update edge to that vertex to
                edgeTo[w] = v;
                //draw out the path
                announce();
                //update distance
                distTo[w] = distTo[v] + 1;
                dfs(w);
                if (targetFound) {
                    return;
                }
            }
        }
    }

    @Override
    public void solve() {
        dfs(s);
    }
}


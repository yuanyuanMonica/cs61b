import java.util.Observable;
import java.util.Stack;

/**
 *  @author Josh Hug
 */

public class MazeCycles extends MazeExplorer {
    /* Inherits public fields:
    public int[] distTo;
    public int[] edgeTo;
    public boolean[] marked;
    */
    private boolean cycleFound;
    private int s;
    private Stack<Integer> stack;
    private int[] parents;

    public MazeCycles(Maze m) {
        super(m);
        cycleFound = false;
        s = maze.xyTo1D(1, 1);
        stack = new Stack<>();
        distTo[s] = 0;
        edgeTo[s] = s;
        parents = new int[maze.V()];
    }

    @Override
    public void solve() {
        // TODO: Your code here!
        cycle(s);
    }

    // Helper methods go here
    private boolean cycle(int v) {
        stack.push(v);
        marked[v] = true;
//        announce();
        for (int w : maze.adj(v)) {
            if (!marked[w]) {
                parents[w] = v;
                edgeTo[w] = v;
                distTo[w] = distTo[v] + 1;
//                announce();
                cycle(w);
            } else if (parents[v] != w) {
                cycleFound = true;
            }
            if (cycleFound) {
                return true;
            }
        }
        return false;
    }
}


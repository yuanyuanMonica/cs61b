package plumbum_beta;

public class PlumbumBetaAnswers {
	public static final String answerToQuestion1A = "java.lang.NullPointerException";
	public static final String answerToQuestion1B = "SparseIntVector.java:22";
	public static final String answerToQuestion1C = "top";

	public static final String answerToQuestion2 = "No initialization of List";

	public static final String answerToQuestion3A = "SparseIntVector.java:54";
	public static final String answerToQuestion3B = "IndexOutOfRange";
	public static final String answerToQuestion3C = "SparseIntVectorTest.java:13";
}

public class threesum {
		public static void main(String [] args){
			int [] a = {-6, 2, 4};
			System.out.println("Should print true for {-6, 2, 4}" + threeSum(a));		
			int [] b = {-6, 2, 5};
			System.out.println("Should print false for {-6, 2, 5}" + threeSum(b));
			int [] c = {-6, 3, 10, 200};
			System.out.println("Should print true for {-6, 3, 10, 200}" + threeSum(c));	
			int [] d = {8, 2, -1, 15};
			System.out.println("Should print true for {8, 2, -1, 15}" + threeSum(d));		
			int [] e = {8, 2, -1, -1, 15};
			System.out.println("Should print true for {8, 2, -1, -1, 15}" + threeSum(e));		
			int [] f = {5, 1, 0, 3, 6};
			System.out.println("Should print true for {5, 1, 0, 3, 6}" + threeSum(f));
		}

		static boolean threeSum(int [] a){
			for (int i = 0; i < a.length; i ++){
				for (int j = 0; j < a.length; j++){
					for (int k = 0; k < a.length; k++){
						if (a[i] + a[j] + a[k] == 0){
							return true;
						}
					}
				}
			}
			return false;
		}
	

}
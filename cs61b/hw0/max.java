public class max{
public static void main (String [] args){
  int[] a = {1, 2, 3, 4};
  int[] b = {789432, 3, -1034134, 100000000};
  System.out.println("Should print 4");
  System.out.println(max_for(a));
  System.out.println("Should print 4");
  System.out.println(max_while(a));
  System.out.println("Should print 100000000");
  System.out.println(max_for(b));
  System.out.println("Should print 100000000");
  System.out.println(max_while(b));
}
static int max_for(int[] a){
  int max = a[0];
  for (int i = 0; i < a.length; i++){
    if (max < a[i]){
      max = a[i];
    }
  };
  return max;
}

static int max_while(int[] a){
  int max = a[0];
  int i = 0;
  while (i < a.length){
    if (max < a[i]){
      max = a[i];
    }
    i++;
  }
  return max;
}
}

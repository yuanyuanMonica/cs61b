import org.junit.Test;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import static org.junit.Assert.*;

public class UnionFindTest {
    @Test
    public void Test() {
        UnionFind uf = new UnionFind(8);
        System.out.println(uf.union(2, 3));
        System.out.println(uf.union(1, 6));
        System.out.println(uf.union(5, 7));
        System.out.println(uf.union(8, 4));
        System.out.println(uf.union(7, 2));
        System.out.println(uf.find(3));
        System.out.println(uf.union(6, 4));
        System.out.println(uf.union(6, 3));
        System.out.println(uf.find(7));
        System.out.println(uf.find(8));
        for (int i  = 1; i <= 8; i++) {
            for (int j = 1; j <= 8; j++) {
                assertEquals(true, uf.samePartition(i, j));
            }
        }
    }

//    @Test
//    public void modsTest() {
//        UnionFind uf = new UnionFind(100);
//
//        for (int i = 1; i <= 100; i++) {
//            switch (uf.find(i % 10)) {
//                case 0:
//                    uf.union(i, 0);
//                    break;
//                case 1:
//                    uf.union(i, 1);
//                    break;
//                case 2:
//                    uf.union(i, 2);
//                    break;
//                case 3:
//                    uf.union(i, 3);
//                    break;
//                case 4:
//                    uf.union(i, 4);
//                    break;
//                case 5:
//                    uf.union(i, 5);
//                    break;
//                case 6:
//                    uf.union(i, 6);
//                    break;
//                case 7:
//                    uf.union(i, 7);
//                    break;
//                case 8:
//                    uf.union(i, 8);
//                    break;
//                case 9:
//                    uf.union(i, 9);
//                    break;
//            }
//        }
//
//        Hashtable<Integer, Integer> t = new Hashtable<>();
//        for (int i = 1; i <= 100; i++) {
//            int n = uf.find(i);
//            t.put(n, i);
//        }
//
//        System.out.println("done");
//    }
}

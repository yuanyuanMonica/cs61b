import java.util.Arrays;

/** A partition of a set of contiguous integers that allows (a) finding whether
 *  two integers are in the same partition set and (b) replacing two partitions
 *  with their union.  At any given time, for a structure partitioning
 *  the integers 1-N, each partition is represented by a unique member of that
 *  partition, called its representative.
 *  @author Xiyuan Wang
 */
public class UnionFind {

    /** A union-find structure consisting of the sets { 1 }, { 2 }, ... { N }.
     */
    public UnionFind(int N) {
        // FIXME
        num = N;
        n = new int[num];
        //Assign every Set and ParentIndex
        for (int i = 0; i < num; i++) {
            n[i] = i + 1;
        }
    }

    /** Return the representative of the partition currently containing V.
     *  Assumes V is contained in one of the partitions.  */
    public int find(int v) {
       return find_help(v);
       // FIXME
    }

    /** Return true iff U and V are in the same partition. */
    public boolean samePartition(int u, int v) {
        return find(u) == find(v);
    }

    /** Union U and V into a single partition, returning its representative. */
    public int union(int u, int v) {
        int p_u = find_help(u);
        int p_v = find_help(v);
        if (p_u > p_v) {
            n[p_u - 1] = p_v;
            return p_v;
        } else {
            n[p_v - 1] = p_u;
            return p_u;
        }
        // FIXME
    }

    // FIXME

    /** Return the Parent Value of v and reconnect it to its parent */
    private int find_help(int v) {
        int parent = n[v - 1];
        if (parent != v) {
            return find_help(parent);
        } else {
            return v;
        }
    }

    /** Array to store all individual set of Integers */
    private int[] n;

    /** Total number of sets */
    private int num;
}

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

/** Minimal spanning tree utility.
 *  @author Xiyuan Wang
 */
public class MST {

    /** Given an undirected, weighted, connected graph whose vertices are
     *  numbered 1 to V, and an array E of edges, returns a list of edges
     *  in E that form a minimal spanning tree of the input graph.
     *  Each edge in E is a three-element int array of the form (u, v, w),
     *  where 0 < u < v <= V are vertex numbers, and 0 <= w is the weight
     *  of the edge. The result is an array containing edges from E.
     *  Neither E nor the arrays in it may be modified.  There may be
     *  multiple edges between vertices.  The objects in the returned array
     *  are a subset of those in E (they do not include copies of the
     *  original edges, just the original edges themselves.) */
    public static int[][] mst(int V, int[][] E) {
        int[][] e = new int[E.length][];
        boolean[] isResult = new boolean[E.length];
        int result_length = 0;
        System.arraycopy(E, 0, e, 0, E.length);

        Arrays.sort(e, EDGE_WEIGHT_COMPARATOR);
        UnionFind uf = new UnionFind(V);
        for (int i = 0; i < e.length; i++) {
            int[] edge = e[i];
            if (!uf.samePartition(edge[0], edge[1])) {
                isResult[i] = true;
                result_length ++;
                uf.union(edge[0], edge[1]);
            }
        }

        int[][] result = new int[result_length][];
        int num = 0;
        for (int j = 0; j < e.length; j++) {
            if (isResult[j]) {
                result[num] = e[j];
                num++;
            }
        }
        return result;  // FIXME
    }

    /** An ordering of edges by weight. */
    private static final Comparator<int[]> EDGE_WEIGHT_COMPARATOR =
        new Comparator<int[]>() {
            @Override
            public int compare(int[] e0, int[] e1) {
                return e0[2] - e1[2];
            }
        };

}

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of a BST based String Set.
 * @author Xiyuan Wang
 */
public class BSTStringSet implements StringSet {
    /** Creates a new empty set. */
    public BSTStringSet() {
        root = null;
    }

    @Override
    public void put(String s) {
        // FIXME
        root = put(root, s);
    }

    private Node put(Node t, String s) {
        if (t == null) {
            return new Node(s);
        }
        if (t.s.compareTo(s) > 0) {
            t.left = put(t.left, s);
        } else if (t.s.compareTo(s) < 0) {
            t.right =  put(t.right, s);
        }
        return t;
    }

    @Override
    public boolean contains(String s) {
        return contains(root, s);// FIXME
    }

    private boolean contains(Node t, String s) {
        if (t == null) {
            return false;
        } else if (t.s.equals(s)) {
            return true;
        } else if (t.s.compareTo(s) > 0) {
            return contains(t.left, s);
        } else {
            return contains(t.right, s);
        }
    }

    @Override
    public List<String> asList() {
        return asList(root); // FIXME
    }

    private List<String> asList(Node t) {
        if (t == null) {
            return new ArrayList<String>();
        } else {
            List<String> l = asList(t.left);
            l.add(t.s);
            List<String> r = asList(t.right);
            l.addAll(r);
            return l;
        }
    }

    /** Represents a single Node of the tree. */
    private static class Node {
        /** String stored in this Node. */
        private String s;
        /** Left child of this Node. */
        private Node left;
        /** Right child of this Node. */
        private Node right;

        /** Creates a Node containing SP. */
        public Node(String sp) {
            s = sp;
        }
    }

    /** Root node of the tree. */
    private Node root;
}

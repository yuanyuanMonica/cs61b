import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/** Tests of HW5
 *  @author P. N. Hilfinger
 */
public class HW5Test {
    @Test
    public void BSTStringSetTest() {
        BSTStringSet s1 = new BSTStringSet();
        s1.put("banana");
        s1.put("apple");
        s1.put("different");
        s1.put("cinnamon");


        assertEquals(true, s1.contains("apple"));
        assertEquals(false, s1.contains("same"));

        List<String> exp = new ArrayList<String>();
        exp.add("apple");
        exp.add("banana");
        exp.add("cinnamon");
        exp.add("different");
        for (int i = 0; i < exp.size(); i++ ) {
            assertEquals(exp.get(i), s1.asList().get(i));
        }
    }

    public static void main(String[] args) {
        System.exit(ucb.junit.textui.runClasses(LastBitTest.class,
                                                NybblesTest.class,
                                                CompactLinkedListTest.class));
    }

}


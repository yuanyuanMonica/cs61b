import org.junit.Test;;
import static org.junit.Assert.*;

/**String translation Tests
 * @author Xiyuan Wang
 */
public class TranslateTest {
    /**Test for empty string and regular string
     */
    @Test
    public void translateTest() {
        String s1 = "";
        String from = "a b c d e f";
        String to = "A B C D E F";

        Translate t = new Translate();
        assertEquals("",t.translate(s1, from, to));

        String s2 = "aaaaaabbbbbcccccdddddeeeeefffffabcdefabcdefabcdef";
        assertEquals("AAAAAABBBBBCCCCCDDDDDEEEEEFFFFFABCDEFABCDEFABCDEF",
                t.translate(s2, from, to));

    }


}

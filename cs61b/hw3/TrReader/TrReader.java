import java.io.Reader;
import java.io.IOException;

/** Translating Reader: a stream that is a translation of an
 *  existing reader.
 *  @author Xiyuan Wang
 */
public class TrReader extends Reader {
    /** A new TrReader that produces the stream of characters produced
     *  by STR, converting all characters that occur in FROM to the
     *  corresponding characters in TO.  That is, change occurrences of
     *  FROM.charAt(0) to TO.charAt(0), etc., leaving other characters
     *  unchanged.  FROM and TO must have the same length. */
    public TrReader(Reader str, String from, String to) {
        this.str = str;
        this.from = from;
        this.to = to;
    }

    /** This function is to read one character out of the source
     * STR is the reader that we got from constructor,
     * FROM is the library that we need to change in the original source
     * TO is the library that we need to change into
     * What we need to return is the ascii code of the character
     * which is an integer.
     */

    @Override
    public void close() throws IOException {
        str.close();
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int num = str.read(cbuf, off, len);
        for (int i = off; i < off + len; i++) {
            int index = -1;
            for (int j = 0; j < from.length(); j++) {
                if (cbuf[i] == from.charAt(j)) {
                    index = j;
                    break;
                }
            }
            if (index != -1) {
                cbuf[i] = to.charAt(index);
            }
        }
        return num;
    }

    /**STR is the reader that we passed in.
     */
    private Reader str;
    /**FROM is the library that we need to change in the original library.
     */
    private String from;
    /**TO is the library reference that we need to change the word to.
     */
    private String to;

}



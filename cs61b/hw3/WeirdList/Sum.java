/* Sum class implement a sum-all-element version of apply */
public class Sum implements IntUnaryFunction {
    /* S is the result of the sum of all elements */
    protected int s;

    /* Constructor will set S to zero to begin */
    public Sum() {
        this.s = 0;
    }

    /* Apply Func takes in an integer X(each element)
    Return the sum of all the element */
    public int apply(int x) {
        s += x;
        return s;
    }
}

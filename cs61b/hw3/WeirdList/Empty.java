/* Empty Class will give a template of a empty weirdlist */
public class Empty extends WeirdList {

    /* This is the default constructor for WeirdList */
    public Empty(int head, WeirdList tail) {
        super(head, tail);
    }

    /* This will return the length of a empty WeirdList */
    @Override
    public int length() {
        return 0;
    }

    /* This will return nothing since a empty WeirdList
    has nothing to print out */
    @Override
    public String toString() {
        return "";
    }

    /* This will return an EMPTY version of WeirdList
     since we have nothing to be mapped */
    @Override
    public WeirdList map(IntUnaryFunction func) {
        return WeirdList.EMPTY;
    }
}

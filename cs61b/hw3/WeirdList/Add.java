/*Add class implements a addition version of apply function*/
public class Add implements IntUnaryFunction {
    /* N is the number that need to be add on for every element*/
    protected int n;

    /* Constructor pass in one parameter N into the class*/
    public Add(int n) {
        this.n = n;
    }

    /* Apply function will add n for any Pass in Parameter X
    will RETURN the new X*/
    public int apply(int x) {
        return x + n;
    }
}

test12-add-status: ERROR (incorrect output)
    Error on line 17 of test12-add-status.in
-------------------- test12-add-status.in --------------------
1. # Status with two adds
2. I setup1.inc
3. > status
4. === Branches ===
5. *master
6. 
7. === Staged Files ===
8. f.txt
9. g.txt
10. 
11. === Removed Files ===
12. 
13. === Modifications Not Staged For Commit ===
14. 
15. === Untracked Files ===
16. 
17. <<<
--------------------------------------------------------------
-------------------- setup1.inc --------------------
1. # Initialize and add two files.
2. I prelude1.inc
3. + f.txt wug.txt
4. + g.txt notwug.txt
5. > add g.txt
6. <<<
7. > add f.txt
8. <<<
----------------------------------------------------
-------------------- prelude1.inc --------------------
1. # Standard commands and definitions
2. > init
3. <<<
4. D DATE "Date: \w\w\w \w\w\w \d+ \d\d:\d\d:\d\d \d\d\d\d [-+]\d\d\d\d"
5. # A status log header RE.  Captures the commit id in its sole group.
6. D COMMIT_HEAD "commit ([a-f0-9]+)[ \t]*\n(?:Merge:\s+[0-9a-f]{7}\s+[0-9a-f]{7}[ ]*\n)?${DATE}"
7. # A full log entry.  Captures the entry. Assume logs messages don't contain
8. # "==="
9. D COMMIT_LOG "(===[ ]*\ncommit [a-f0-9]+[ ]*\n(?:Merge:\s+[0-9a-f]{7}\s+[0-9a-f]{7}[ ]*\n)?${DATE}[ ]*\n(?:.|\n)*?(?=\Z|\n===))"
10. # An arbitrary line of text (works even with ?s)
11. D ARBLINE "[^\n]*(?=\n|\Z)"
12. # Zero or more arbitrary full lines of text.
13. D ARBLINES "(?:(?:.|\n)*(?:\n|\Z)|\A|\Z)"
------------------------------------------------------
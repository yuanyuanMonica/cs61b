package gitlet;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Gitlet {
    /** Current Directory */
    private File repo = new File("..");

    /** Working Directory in testing */
    private File workingDirectory = new File(".", ".gitlet");

    /** .COMMITS Folder in Working Directory */
    private File commits = new File(workingDirectory, ".commits");

    /** .BLOBS Folder in Working Directory */
    private File blobs = new File(workingDirectory, ".blobs");

    /** .METADATA Folder in Working Directory */
    private File meta = new File(workingDirectory, ".metadata");

    /** .STAGING Folder in .METADATA Folder */
    private File stag = new File(meta, ".staging");

    /** .REMOVE Folder in .STAGING Folder */
    private File remove = new File(meta, ".remove");

    /** .BRANCHES Folder in .METADATA Folder */
    private File branch = new File(meta, ".branches");

    /** HEAD File in .METADATA Folder */
    private File head = new File(meta, "head");

    /** CURRBRANCH File in .METADATA Folder */
    private File currBranch = new File(meta, "currBranch");

    /** Shared Initial Commit */
    private static Commit initC = new Commit(0);

    /** Command Interpreter using */
    private CommandInterpreter cmd;

    /** Command that need to be done */
    private Command command;

    Gitlet(CommandInterpreter c) {
        cmd = c;
    }

    public void process() {
        //new Class of Command would be better //fixed
        command = cmd.parseCommand();
        switch (command.type()) {
            case "init":
                init();
                break;
            case "add":
                add();
                break;
            case "commit":
                commit();
                break;
            case "rm":
                rm();
                break;
            case "checkout":
                checkout();
                break;
            case "branch":
                branch();
                break;
            case "rm-branch":
                rmBranch();
                break;
            case "status":
                status();
                break;
            case "log":
                log();
                break;
            case "global-log":
                globalLog();
                break;
            case "find":
                find();
                break;
            case "reset":
                reset();
                break;
            case "merge":
                merge();
                break;
            default:
                throw new GitletException("this Command haven't been implemented");
        }
    }

    /** Init Command */
    public void init() {
        if(!workingDirectory.mkdir()){
            throw new GitletException(" A Gitlet version-control system already exists in the current directory.");
        }
        //Make Folders
//        System.out.println("Execute Init command");
//        System.out.println("make folders");
        commits.mkdir();
        blobs.mkdir();
        meta.mkdir();
        stag.mkdir();
        remove.mkdir();
        branch.mkdir();

        //Add initial Commits object
//        System.out.println(initC.id());
        File f = new File(commits, initC.id());
        Utils.writeObject(f, initC);
//        System.out.println("write initial Commits object: "+initC.id());

        //Add master Branch object into .branches
//        System.out.println("add initial commit to master branch");
        List<String> b = new ArrayList<>();
        b.add(initC.id());
        Branch master = new Branch("master", initC.id(), b);
        File m = new File(branch, master.name());
        Utils.writeObject(m, master);
//        System.out.println(master.toString());

        //set head to be inital Commit
//        System.out.println("set head to be inital Commit");
        Utils.writeContents(head, initC.id());

        //Set CurrBranch to be master
//        System.out.println("Set CurrBranch to be master");
        Utils.writeObject(currBranch, master.name());
    }
    /** Add Command */
    public void add() {
//        System.out.println("Execute add Command");
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: add [filename]");
        }
        try {
            //Check if file exits
//            System.out.println("file exists??");
            File f = new File(".", command.operands().get(0));
            if (!f.exists()) {
                throw new GitletException("File does not exist.");
            } else {
                //make a Blob object with content
                Blob b = new Blob(f);
//            System.out.println("new Blob object with content"+b.id());

                //Check current commit
//            System.out.println("staged and committed in current Commit??");
                Commit ct = getCurrentCommit();
                HashMap<String, String> bl = ct.getBlobs();
                if (bl != null && bl.containsValue(b.id())) {
                    //delete staged same file
//                System.out.println("same file with same context that's staged??");
                    File f1 = new File(stag, b.id());
                    if (f1.exists()) {
                        Utils.restrictedDelete(f1);
                    }
//                System.out.println(Utils.restrictedDelete(f1));
//            } else if (new File(remove, b.filename()).exists()) {
////            System.out.println("has been rm before??");
//                File f2 = new File(remove, b.id());
//                f2.delete();
////            System.out.println(f2.delete());
                } else {
//                System.out.println("add the blobs into .STAG");
                    Utils.writeObject(new File(stag, b.filename()), b);
                }
            }

            //if its put in .REMOVE then remove it from .REMOVE
            //TODO
//            System.out.println("has been rm before??");
//            File f2 = new File(remove, b.id());
//            f2.delete();
//            System.out.println(f2.delete());
        } catch (Exception e) {
            if (new File(remove, command.operands().get(0)).exists()) {
                File f2 = new File(remove, command.operands().get(0));
                f2.delete();
            } else {
                throw new GitletException("File does not exist.");
            }
        }
    }

    /** Commit Command */
    public void commit() {
        //Check ARGs is in the right format
//        System.out.println("Execute Commit");
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: commit [message]");
        }

        //Check .STAG and .REMOVE to see if we have updates
//        System.out.println("check .stag and .remove");
        if (stag.list().length == 0 && remove.list().length == 0) {
            throw new GitletException("No changes added to the commit.");
        }

//        if ()

        String message = command.operands().get(0);
        if (message.equals("")) {
            throw new GitletException("Please enter a commit message.");
        }
        Commit prev_cmt = getCurrentCommit();
        //deserialize all blobs file in .STAGED folder and store them in a List
        //For each blob store them in .BLOBS folder
//        System.out.println("get all blobs from .STAG folder");
        List<Blob> bl = new ArrayList<>();
        String[] staged_Files = stag.list();

        for (String s: staged_Files) {
            File staged_File = new File(stag, s);
            Blob tobe = Utils.readObject(staged_File, Blob.class);

//            System.out.println(tobe.toString());
//            System.out.println("add this blob to the list");
            bl.add(tobe);
            staged_File.delete();

//            System.out.println("store this blob to .BLOBS");
            Utils.writeObject(new File(blobs, tobe.id()), tobe);
        }
        //make a new Commit object
//        System.out.println("make a new commit object");
        Commit c = new Commit(message, prev_cmt.id(), bl, System.currentTimeMillis());
//        System.out.println(c.toString());
        //update Head and store it
//        System.out.println("Update the current commit to be this commit");
        updateCurrentCommit(c.id());
        updateCurrentBranch(c.id());
        //store the commit object to .COMMIT folder
//        System.out.println("Write new Commit to .COMMIT "+c.id());
//        System.out.println("new Commit"+ c.toString());
        Utils.writeObject(new File(commits, c.id()), c);

        //clear remove folder
        for (String s: remove.list()) {
            File f3 = new File (remove, s);
            f3.delete();
        }
    }

    /** rm Command */
    public void rm() {
//        System.out.println("Execute rm");
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: rm [filename]");
        }
        //get filename and make the file to a blob
        String fn = command.operands().get(0);
        File f = new File(stag, fn);
        Commit currC = getCurrentCommit();
        if (!f.exists() && !currC.getBlobs().containsKey(fn) ) {
            throw new GitletException("No reason to remove the file.");
        }

        //check if we have already staged the file
        if (f.exists()) {
//            System.out.println("the file is already staged");
//            System.out.println("unstage??");
//            System.out.println(f.delete());
//            Utils.writeObject(new File(remove, fn), new Blob(f)); //
            f.delete();
        }
        //Check currCommit to see if its tracked
        if (currC.getBlobs().containsKey(fn)){
//            System.out.println("the file has been committed in currentCommit");
//            System.out.println(Utils.restrictedDelete(new File(".", fn)));
            File actual = new File(".", fn);
            if (actual.exists()) {
                Utils.writeObject(new File(remove, fn), new Blob(new File(".", fn)));
                Utils.restrictedDelete(new File(".", fn));
            } else {
                String theBlob = currC.getBlobs().get(fn);
                Blob thatBlob = Utils.readObject(new File(blobs, theBlob), Blob.class);
                Utils.writeObject(new File(remove, fn), thatBlob);
            }
        }
    }

    //TODo: Merge case log
    public void log() {
        Commit c = getCurrentCommit();
        while (c.parent() != null) {
            System.out.println(c.toString()+"\n");
            c = getParentCommit(c);
//            System.out.println(c.getBlobs().keySet().toArray());
        }
        System.out.println(initC.toString()+"\n");
//        System.out.println("end");
    }

    public void branch() {
//        System.out.println("Execute branch");
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: branch [branchname].");
        }
        String bn = command.operands().get(0);

        //check if this branch already exists
        if (new File(branch, bn).exists()) {
            throw new GitletException("A branch with that name already exists.");
        }

        //create a new Branch object with the passed in branchname.
        Commit ct = getCurrentCommit();
        Branch br = new Branch(bn, ct.id());
        br.addCommits(ct.id());
//        System.out.println("new Branch created: "+br.toString());

        //write this new Branch to .BRANCHES folder
        Utils.writeObject(new File(branch, br.name()), br);
    }

    public void checkout() {
        //Checkout -- filename
        if (command.operands().size() == 2) {
//            System.out.println("Execute checkout -- filename");
            String fn = command.operands().get(1);
            checkoutFile(fn);
        } else if (command.operands().size() == 3) {
//            System.out.println("Execute checkout commitID -- filename");
            String commitID = command.operands().get(0);
            String filename = command.operands().get(2);
            if (!command.operands().get(1).equals("--")) {
                throw new GitletException("Incorrect operands.");
            }
            checkoutCommit(commitID, filename);
        } else if (command.operands().size() == 1) {
//            System.out.println(command.operands().get(0));
//            System.out.println("Execute checkout branchname");
            String bn = command.operands().get(0);
            checkoutBranch(bn);
        } else {
            throw new GitletException("Please follow:\n checkout -- filename\n" +
                    " checkout commitID --filename\n checkout branchname");
        }

    }

    //TODO:
    public void status() {
        //get Current Branch
        Branch currBranch = getCurrentBranch();

        //Print out branches
        System.out.println("=== Branches ===");
        for (String s: branch.list()) {
            if (currBranch.name().equals(s)) {
                System.out.println("*" + currBranch.name());
            } else {
                System.out.println(s);
            }
        }
        System.out.println("");

        //print out staged files
        System.out.println("=== Staged Files ===");
        for (String s: stag.list()) {
            System.out.println(s);
        }
        System.out.println("");

        //print out remove files
        System.out.println("=== Removed Files ===");
        for (String s: remove.list()) {
            System.out.println(s);
        }
        System.out.println("");

        //TODO: modifications not staged for commit
        System.out.println("=== Modifications Not Staged For Commit ===");
        System.out.println("");
        //TODO: untracked files
        System.out.println("=== Untracked Files ===");
        System.out.println("");
//        System.out.println("end of status");
    }

    public void globalLog() {
        for (String s: commits.list()) {
            Commit ct = Utils.readObject(new File(commits, s), Commit.class);
            System.out.println(ct.toString() + "\n");
        }
    }

    /** find Command */
    public void find() {
//        System.out.println("Execute find");
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: find [commit msg]");
        }
        String msg = command.operands().get(0);
        String[] commits_id = commits.list();
        boolean founded = false;
        for (String s: commits_id) {
            Commit ct = Utils.readObject(new File(commits, s), Commit.class);
            if (ct.getLogMsg().equals(msg)) {
                founded = true;
                System.out.println(ct.id());
            }
        }
        if (!founded) {
            throw new GitletException("Found no commit with that message.");
        }
    }

    //TODO: Reset
    /** reset Command */
    public void reset() {

    }

    /** rm-branch Command */
    public void rmBranch() {
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: rm-branch [branchname]");
        }
        String branchname = command.operands().get(0);

        //check if the branch exists
        if (!new File(branch, branchname).exists()) {
            throw new GitletException("A branch with that name does not exist.");
        }

        //check if it is the Current Branch
        Branch currBr = getCurrentBranch();
        if (currBr.name().equals(branchname)) {
            throw new GitletException("Cannot remove the current branch.");
        }

        //delete the branch object from .BRANCHES folder
        System.out.println("delete the branch from .BRANCHES");
        File br = new File(branch, branchname);
        System.out.println(br.delete());
//        br.delete();
    }

    /* Merge command */
    public void merge(){
        if (command.operands().size() != 1) {
            throw new GitletException("Please follow: merge [branchname]");
        }
        //get branch name
        String brn = command.operands().get(0);

        //get checkedout branch and current branch and split point
        Branch checked = Utils.readObject(new File(branch, brn), Branch.class);
        Branch currBranch = getCurrentBranch();

        //get the Split point
        Commit sp = getSplitPoint(checked, currBranch);

        //check if there is staged or removed files need to be committed
        if (stag.list().length != 0 || remove.list().length != 0) {
            throw new GitletException("You have uncommitted changes.");
        }
        //check if the branch exists
        if (!new File(branch, brn).exists()) {
            throw new GitletException("A branch with that name does not exist.");
        }
        //check if checked out branch is the current branch
        if (currBranch.name().equals(brn)) {
            throw new GitletException("Cannot merge a branch with itself.");
        }
        //check if there is an untracked file
        if (!checkUntracked(new File(".."))) {
            throw new GitletException("There is an untracked file in the way; delete it or add it first.");
        }

        //Case 1:
        if (currBranch.getCommits().contains(checked.latestCommitID())) {
            //do nothing
            throw new GitletException("Given branch is an ancestor of the current branch.");
        } else if (checked.getCommits().contains(currBranch.latestCommitID())) {
            List<String> checkedCommits = checked.getCommits();
            int i = checkedCommits.indexOf(currBranch.latestCommitID());
            for (int j = i + 1; j < checkedCommits.size(); j++) {
                currBranch.addCommits(checkedCommits.get(j));
            }
            updateCurrentCommit(currBranch.latestCommitID());
        } else {
            merge_help(checked, currBranch, sp);
        }
    }



    /******** HELPER FUNCTIONS ********/

    /* Assuming the Current Branch File exists and get Current Branch */
    private Branch getCurrentBranch() {
        String cbn = Utils.readContentsAsString(currBranch);
        Branch br = Utils.readObject(new File(branch, cbn), Branch.class);
        return br;
    }

    /* Assuming the Current Commit File exists and get Current Commit */
    private Commit getCurrentCommit() {
        String headCommit = Utils.readContentsAsString(head);
        File ccf = new File(commits, headCommit);
        Commit ct = Utils.readObject(ccf, Commit.class);
        return ct;
    }

    /* Update the head to be Commit CT and write it back to HEAD File,
    Update the currBranch to set this commit as latest and write it back to currBranch */
    private void updateCurrentCommit(String ctid) {
        Utils.writeContents(head, ctid);
    }

    /* Update the currBranch to be Branch bc and Return the new Tree Object */
    private void updateCurrentBranch(String ctid) {
        String currB = Utils.readContentsAsString(currBranch);
        Branch cb = Utils.readObject(new File(currBranch, currB), Branch.class);
        cb.addCommits(ctid);
        Utils.writeObject(new File(currBranch, currB), cb);
        Utils.writeObject(currBranch, currB);
    }

    /* Get the parent Commit of passed-in Commit and Return it */
    private Commit getParentCommit(Commit ct) {
        String p = ct.parent();
//        System.out.println("the parent has this id" + p);
        return Utils.readObject(new File(commits, p), Commit.class);
    }

    /** Checkout filename helper function */
    private void checkoutFile(String fn) {
        //get filename
//        String fn = command.operands().get(1);

        //get the current Commit and check if the file is in there
        Commit ct = getCurrentCommit();
        if (!ct.getBlobs().containsKey(fn)) {
            throw new GitletException("File does not exist in that commit.");
        }

        //overwrite the file in working directory
        String blobid = ct.getBlobs().get(fn);
        Blob b = Utils.readObject(new File(blobs, blobid), Blob.class);
        Utils.writeContents(new File(".", fn), b.context());
    }

    /** Checkout commitID  -- filename helper function */
    private void checkoutCommit(String commitID, String filename) {
        //get commitID and filename
//        String commitID = command.operands().get(0);
//        String filename = command.operands().get(2);

        List<String> theFile = new ArrayList<String>();
        int foundedCommit = 0;


        //check if the specified commit exists
        //TODO Abbreviated Commit id
        for (String s: commits.list()) {
            //if exists more than once
            if (foundedCommit > 1) {
                throw new GitletException("The specified Commit is not unique.");
            }
            if (s.startsWith(commitID)) {
                foundedCommit ++;
                Commit ct = Utils.readObject(new File(commits, s), Commit.class);
                //check if it has that file
                if (ct.getBlobs().containsKey(filename)) {
                    theFile.add(ct.getBlobs().get(filename));
                }
            }
        }

        //check if there is no such a file in that commit
        if (theFile.size() == 0 && foundedCommit != 0) {
            throw new GitletException("File does not exist in that commit.");
        } else if (foundedCommit == 0) {
            throw new GitletException("No commit with that id exists.");
        }

        //overwrite the file we found
        Blob b = Utils.readObject(new File(blobs, theFile.get(0)), Blob.class);
        Utils.writeContents(new File(".", filename), b.context());

    }

    /** Checkout branchname helper function */
    private void checkoutBranch(String bn) {
        //get the branch name arguments
//        String bn = command.operands().get(0);
        //check if branchname exists
        if (!new File(branch, bn).exists()) {
            throw new GitletException("No such branch exists.");
        }
        //check if current branch is the to-be-checked-out branch
        else if (getCurrentBranch().name().equals(bn)) {
            throw new GitletException("No need to checkout the current branch.");
        }
        //check if this checkout will change any untracked file in current branch
        else {
            Commit currCt = getCurrentCommit();
            HashMap<String, String> blobs = currCt.getBlobs();

            //get new Files from latest commit of currBranch
            Branch new_branch = Utils.readObject(new File(branch, bn), Branch.class);
            Commit new_commit = Utils.readObject(new File(commits, new_branch.latestCommitID()), Commit.class);
            HashMap<String, String> new_files = new_commit.getBlobs();

            //TODO:check untracked files by current branch
            //delete all tracked files by current branch
            for (String s: blobs.keySet()) {
                Utils.restrictedDelete(new File(".", s));
            }

            //Get all files on given branch and overwrite the working directory
            for (String s: new_files.keySet()) {
                File f = new File(".", s);
                Blob b = Utils.readObject(new File(this.blobs, new_files.get(s)), Blob.class);
                Utils.writeContents(f, b.context());
            }

            //Update current Branch
            Utils.writeContents(currBranch, new_branch.name());
            Utils.writeContents(head, new_branch.latestCommitID());

            //TODO: What is untracked file to be exactly?
            }
        }

    /* check if there is files in the path specified by the File that is untracked by the current Commit */
    private boolean checkUntracked (File f) {

        Commit ct = getCurrentCommit();
        HashMap<String, String> bls = ct.getBlobs();

        for (String s: Utils.plainFilenamesIn(f)) {
            File new_f = new File(f, s);
            String sha = Utils.sha1(new_f.getName(), Utils.readContents(new_f));
            if (!bls.containsValue(sha)) {
                return false;
            }
        }

        return true;
    }

    /** Merge Helper */
    private void merge_help(Branch checked, Branch currBranch, Commit sp) {
        Commit latestChecked = Utils.readObject(new File(commits, checked.latestCommitID()), Commit.class);
        Commit currCommit = getCurrentCommit();

        List<String> sha1InSP = (List<String>)sp.getBlobs().values();
        List<String> sha1InLC = (List<String>)latestChecked.getBlobs().values();
        List<String> sha1InCC = (List<String>)currCommit.getBlobs().values();
        for (String s1: sp.getBlobs().keySet()) {

        }

    }

    /** Get the most recent comman ancestor for both Branches and return that Commit */
    private Commit getSplitPoint(Branch checked, Branch currBranch) {
        String sp;
        if (checked.splitPoint().equals(initC.id())) {
            sp = currBranch.splitPoint();
        } else {
            sp = checked.splitPoint();
        }
        return Utils.readObject(new File(commits, sp), Commit.class);
    }
}

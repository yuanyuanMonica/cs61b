package qirkat;

import java.util.ArrayList;

import static qirkat.PieceColor.*;

/** A Player that computes its own moves.
 *  @author Xiyuan Wang
 */
class AI extends Player {

    /** Maximum minimax search depth before going to static evaluation. */
    private static final int MAX_DEPTH = 8;
    /** A position magnitude indicating a win (for white if positive, black
     *  if negative). */
    private static final int WINNING_VALUE = Integer.MAX_VALUE - 1;
    /** A magnitude greater than a normal value. */
    private static final int INFTY = Integer.MAX_VALUE;

    /** A new AI for GAME that will play MYCOLOR. */
    AI(Game game, PieceColor myColor) {
        super(game, myColor);
    }

    @Override
    Move myMove() {
        Main.startTiming();
        Move move = findMove();
        Main.endTiming();
        System.out.println(myColor() + " :" + move.toString());
        return move;
    }

    /** Return a move for me from the current position, assuming there
     *  is a move. */
    private Move findMove() {
        Board b = new Board(board());
        if (myColor() == WHITE) {
            findMove(b, MAX_DEPTH, true, 1, -INFTY, INFTY);
        } else {
            findMove(b, MAX_DEPTH, true, -1, -INFTY, INFTY);
        }
        return _lastFoundMove;
    }

    /** The move found by the last call to one of the ...FindMove methods
     *  below. */
    private Move _lastFoundMove;

    /** Find a move from position BOARD and return its value, recording
     *  the move found in _lastFoundMove iff SAVEMOVE. The move
     *  should have maximal value or have value > BETA if SENSE==1,
     *  and minimal value or value < ALPHA if SENSE==-1. Searches up to
     *  DEPTH levels.  Searching at level 0 simply returns a static estimate
     *  of the board value and does not set _lastMoveFound. */
    private int findMove(Board board, int depth, boolean saveMove, int sense,
                         int alpha, int beta) {
        Move best;
        best = null;

        // FIXME
        if (depth == 0) {
            return staticScore(board);
        }
        if (board.gameOver()) {
            return staticScore(board);
        }
        int bestScore;
        if (sense == 1) {
            bestScore = 0 - INFTY;
            ArrayList<Move> movesMax = board.getMoves();
            for (Move m: movesMax) {
                board.makeMove(m);
                int response = findMove(board, depth - 1,
                        false, -1, alpha, beta);
                board.undo();
                if (response >= bestScore) {
                    bestScore = response;
                    best = m;
                    beta = Math.min(beta, response);
                }
                if (beta <= alpha) {
                    break;
                }
            }
        } else {
            bestScore = INFTY;
            ArrayList<Move> movesMin = board.getMoves();
            for (Move m: movesMin) {
                board.makeMove(m);
                int response = findMove(board, depth - 1,
                        false, 1, alpha, beta);
                board.undo();
                if (response <= bestScore) {
                    bestScore = response;
                    best = m;
                    alpha = Math.max(alpha, response);
                }
                if (beta <= alpha) {
                    break;
                }
            }
        }

        if (saveMove) {
            _lastFoundMove = best;
        }

        return bestScore; // FIXME
    }

    /** Return a heuristic value for BOARD. */
    private int staticScore(Board board) {
        if (board.whoseMove().equals(PieceColor.WHITE)) {
            if (board.gameOver()) {
                return 0 - INFTY;
            }
        } else if (board.whoseMove().equals(PieceColor.BLACK)) {
            if (board.gameOver()) {
                return INFTY;
            }
        }
        int whitePiece = 1;
        int blackPiece = -1;
        int score = 0;
        for (int i = 0; i < 25; i++) {
            if (board.get(i).equals(PieceColor.WHITE)) {
                score += whitePiece;
            } else if (board.get(i).equals(PieceColor.BLACK)) {
                score += blackPiece;
            }
        }
        return score; // FIXME
    }
}

package qirkat;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;
import java.util.Formatter;

import static qirkat.PieceColor.*;
import static qirkat.Move.*;

/** A Qirkat board.   The squares are labeled by column (a char value between
 *  'a' and 'e') and row (a char value between '1' and '5'.
 *
 *  For some purposes, it is useful to refer to squares using a single
 *  integer, which we call its "linearized index".  This is simply the
 *  number of the square in row-major order (with row 0 being the bottom row)
 *  counting from 0).
 *
 *  Moves on this board are denoted by Moves.
 *  @author Xiyuan Wang
 */
class Board extends Observable {

    /** A new, cleared board at the start of the game. */
    Board() {
        // FIXME?
        clear();
        //No
    }

    /** A copy of B. */
    Board(Board b) {
        internalCopy(b);
    }

    /** Return a constant view of me (allows any access method, but no
     *  method that modifies it). */
    Board constantView() {
        return this.new ConstantBoard();
    }

    /** Clear me to my starting state, with pieces in their initial
     *  positions. */
    void clear() {
        _whoseMove = WHITE;
        _gameOver = false;

        // FIXME
        numPieces = TWENTYFIVE;
        _board = new PieceColor [numPieces];
        for (int i = 0; i < _board.length; i++) {
            if (i == 13 || i == 14 || i <= 9) {
                set(i, PIECE_VALUES[1]);
            } else if (i == 12) {
                set(i, PIECE_VALUES[0]);
            } else {
                set(i, PIECE_VALUES[2]);
            }
        }
        //Fixed


        setChanged();
        notifyObservers();
    }

    /** Copy B into me. */
    void copy(Board b) {
        internalCopy(b);
    }

    /** Copy B into me. */
    private void internalCopy(Board b) {
        // FIXME
        numPieces = b.numPieces;
        _nonCapMoves = new ArrayList<Move> (b._nonCapMoves);
        _previousMoves = (Stack<Move>) b._previousMoves.clone();
        _gameOver = b._gameOver;
        _whoseMove = b._whoseMove;
        if (_board == null) {
            numPieces = b.numPieces;
            _board = new PieceColor[numPieces];
        }
        for (int i = 0; i < numPieces; i++) {
            PieceColor p = b.get(i);
            set(i, p);
        }
    }

    /** Set my contents as defined by STR.  STR consists of 25 characters,
     *  each of which is b, w, or -, optionally interspersed with whitespace.
     *  These give the contents of the Board in row-major order, starting
     *  with the bottom row (row 1) and left column (column a). All squares
     *  are initialized to allow horizontal movement in either direction.
     *  NEXTMOVE indicates whose move it is.
     */
    void setPieces(String str, PieceColor nextMove) {
        if (nextMove == EMPTY || nextMove == null) {
            throw new IllegalArgumentException("bad player color");
        }
        str = str.replaceAll("\\s", "");
        if (!str.matches("[bw-]{25}")) {
            throw new IllegalArgumentException("bad board description");
        }
        // FIXME
        _whoseMove = nextMove;

        for (int k = 0; k < str.length(); k += 1) {
            switch (str.charAt(k)) {
            case '-':
                set(k, EMPTY);
                break;
            case 'b': case 'B':
                set(k, BLACK);
                break;
            case 'w': case 'W':
                set(k, WHITE);
                break;
            default:
                break;
            }
        }

        // FIXME
        _nonCapMoves.clear();
        _previousMoves.clear();

        setChanged();
        notifyObservers();
    }

    /** Return true iff the game is over: i.e., if the current player has
     *  no moves. */
    boolean gameOver() {
        if (isMove()) {
            _gameOver = false;
        } else {
            _gameOver = true;
        }
        return _gameOver;
    }

    /** Return the current contents of square C R, where 'a' <= C <= 'e',
     *  and '1' <= R <= '5'.  */
    PieceColor get(char c, char r) {
        assert validSquare(c, r);
        return get(index(c, r));
        // FIXME?

    }

    /** Return the current contents of the square at linearized index K. */
    PieceColor get(int k) {
        assert validSquare(k);
        return _board[k];
        // FIXME

    }

    /** Set get(C, R) to V, where 'a' <= C <= 'e', and
     *  '1' <= R <= '5'. */
    private void set(char c, char r, PieceColor v) {
        assert validSquare(c, r);
        set(index(c, r), v);
        // FIXME?

    }

    /** Set get(K) to V, where K is the linearized index of a square. */
    private void set(int k, PieceColor v) {
        assert validSquare(k);
        // FIXME
        _board[k] = v;


    }

    /** Return true iff MOV is legal on the current board. */
    boolean legalMove(Move mov) {
        if (mov == null) {
            return false;
        }
        int from = mov.fromIndex();
        int to = mov.toIndex();
        if (!get(from).equals(whoseMove())) {
            return false;
        } else if (!get(to).equals(PieceColor.EMPTY)) {
            return false;
        }


        if (!mov.isJump()) {
            List<Integer> pos = new ArrayList<Integer>();
            if (jumpPossible()) {
                return false;
            } else if (!adjacentRow(pos, from).contains(to)
                    && !adjacentCol(pos, from).contains(to)
                    && !adjacentDiag(pos, from).contains(to)) {
                return false;
            } else if ((whoseMove() == PieceColor.BLACK
                    && mov.toIndex() > mov.fromIndex()
                    && !mov.isRightMove())
                    || (whoseMove() == PieceColor.WHITE
                    && mov.toIndex() < mov.fromIndex()
                    && !mov.isLeftMove())){
                return false;
            } else if (_nonCapMoves.contains(mov)) {
                return false;
            } else if (mov.isRightMove() || mov.isLeftMove()) {
                if (whoseMove() == PieceColor.BLACK
                        && row(mov.fromIndex()) == '1') {
                    return false;
                } else if (whoseMove() == PieceColor.WHITE
                        && row(mov.fromIndex()) == '5') {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            if (checkJump(mov, false)) {
                return true;
            } else {
                return false;
            }
        }
        // FIXME
    }

    /** Return a list of all legal moves from the current position. */
    ArrayList<Move> getMoves() {
        ArrayList<Move> result = new ArrayList<>();
        getMoves(result);
        return result;
    }

    /** Add all legal moves from the current position to MOVES. */
    void getMoves(ArrayList<Move> moves) {
        if (gameOver()) {
            return;
        }
        if (jumpPossible()) {
            for (int k = 0; k <= MAX_INDEX; k += 1) {
                getJumps(moves, k);
            }
        } else {
            for (int k = 0; k <= MAX_INDEX; k += 1) {
                getMoves(moves, k);
            }
        }
    }

    /** Add all legal non-capturing moves from the position
     *  with linearized index K to MOVES. */
    private void getMoves(ArrayList<Move> moves, int k) {
        // FIXME
        if (get(k) == whoseMove()) {
            char row0 = row(k);
            char col0 = col(k);
            List<Integer> pos = new ArrayList<Integer>();
            adjacentRow(pos, k);
            adjacentCol(pos, k);
            adjacentDiag(pos, k);
            for (int toIndex : pos) {
                if (get(toIndex) == PieceColor.EMPTY) {
                    Move m = move(col0, row0, col(toIndex), row(toIndex));
                    if (legalMove(m)) {
                        moves.add(m);
                    }
                }
            }
        }
    }

    /** Add all legal captures from the position
     * with linearized index K to MOVES. */
    private void getJumps(ArrayList<Move> moves, int k) {
        // FIXME
        List<Integer> pos = new ArrayList<Integer>();
        adjacentRow(pos, k);
        adjacentCol(pos, k);
        adjacentDiag(pos, k);
        if (get(k) == whoseMove()) {
            for (int jumpedI : pos) {
//                System.out.println("jumped Index is " + jumpedI);
                PieceColor jumpedP = get(jumpedI);
                if (jumpedP == whoseMove().opposite()) {
//                    System.out.println("jumped Piece has Color" + jumpedP + "same as " + whoseMove().opposite());
                    int diff_col = col(jumpedI) - col(k);
                    int diff_row = row(jumpedI) - row(k);
                    char to_col = (char) (col(jumpedI) + diff_col);
                    char to_row = (char) (row(jumpedI) + diff_row);
                    if (validSquare(to_col, to_row) && get(index(to_col, to_row)).equals(PieceColor.EMPTY)) {
                        int to = index(to_col, to_row);
//                        System.out.println("To Place is " + to);
                        set(to, whoseMove());
                        set(jumpedI, PieceColor.EMPTY);
                        set(k, PieceColor.EMPTY);
                        if (jumpPossible(to)) {
//                            System.out.println("Still have jumps");
                            getJumps(moves, to);
                            set(to, PieceColor.EMPTY);
                            set(jumpedI, whoseMove().opposite());
                            set(k, whoseMove());
                            Move m = move(col(k), row(k));
//                            System.out.println("curr Move is " + m.toString());
                            for (int i = 0; i < moves.size(); i++) {
                                Move tail_m = moves.get(i);
                                int from = tail_m.fromIndex();
                                if (from == to) {
                                    Move add = move(m, tail_m);
                                    if (checkJump(add, false)) {
                                        moves.remove(i);
                                        moves.add(i, add);
                                    }
                                }
                            }
                        } else {
//                            System.out.println("No more jumps");
                            set(to, PieceColor.EMPTY);
                            set(jumpedI, whoseMove().opposite());
                            set(k, whoseMove());
                            Move m = move(col(k), row(k), col(to), row(to));
                            if (checkJump(m, false)) {
                                moves.add(0, m);
                            }
                        }
                    }
                }
            }
        }
    }

    /** Get all adjacent row from the position
     * with linearized index K RETURN to POS. */
    private List<Integer> adjacentCol(List<Integer> pos, int k) {
        if (col(k) == 'a') {
            pos.add(k + 1);
        } else if (col(k) == 'e') {
            pos.add(k - 1);
        } else {
            pos.add(k + 1);
            pos.add(k - 1);
        }
        return pos;
    }

    /** Get all adjacent col from the position
     * with linearized index K RETURN to POS. */
    private List<Integer> adjacentRow(List<Integer> pos, int k) {
        if (row(k) == '5') {
            pos.add(k - 5);
        } else if (row(k) == '1') {
            pos.add(k + 5);
        } else {
            pos.add(k - 5);
            pos.add(k + 5);
        }
        return pos;
    }

    /** Get all adjacent diagonal nodes from the position
     * with linearized index K RETURN the result in POS.*/
    private List<Integer> adjacentDiag(List<Integer> pos, int k) {
        if (k % 2 == 0) {
            if (row(k) == '5') {
                if (col(k) == 'a') {
                    pos.add(k - 4);
                } else if (col(k) == 'e') {
                    pos.add(k - 6);
                } else {
                    pos.add(k - 4);
                    pos.add(k - 6);
                }
            } else if (row(k) == '1') {
                if (col(k) == 'a') {
                    pos.add(k + 6);
                } else if (col(k) == 'e') {
                    pos.add(k + 4);
                } else {
                    pos.add(k + 4);
                    pos.add(k + 6);
                }
            } else {
                if (col(k) == 'a') {
                    pos.add(k + 6);
                    pos.add(k - 4);
                } else if (col(k) == 'e') {
                    pos.add(k + 4);
                    pos.add(k - 6);
                } else {
                    pos.add(k + 6);
                    pos.add(k - 4);
                    pos.add(k + 4);
                    pos.add(k - 6);
                }
            }
        }
        return pos;
    }

    /** Return true iff MOV is a valid jump sequence on the current board.
     *  MOV must be a jump or null.  If ALLOWPARTIAL, allow jumps that
     *  could be continued and are valid as far as they go.  */
    boolean checkJump(Move mov, boolean allowPartial) {
        if (mov == null) {
//            System.out.println("you must move");
            return false;
        }
        int diffRows = mov.row1() - mov.row0();
        int diffCols = mov.col1() - mov.col0();
        if ((diffCols != 2 && diffCols != -2 && diffCols != 0)
            || (diffRows != 2 && diffRows != -2 && diffRows != 0)) {
//            System.out.println("you can't jumped over "+diffCols+" columns");
//            System.out.println("you can't jumped over "+diffRows+" rows");
            return false;
        }
        int jumpedIndex = mov.jumpedIndex();
//        System.out.println("Jumped index is "+jumpedIndex);
        List<Integer> pos = new ArrayList<Integer>();
        pos = adjacentRow(pos, mov.toIndex());
        pos = adjacentCol(pos, mov.toIndex());
        pos = adjacentDiag(pos, mov.toIndex());
        if (get(jumpedIndex) != whoseMove().opposite()) {
//            System.out.println("you can't jump over your piece or empty");
            return false;
        } else if (get(mov.toIndex()) != PieceColor.EMPTY) {
//            System.out.println("you can't go that place since its not empty");
            return false;
        } else if (!pos.contains(jumpedIndex)) {
//            System.out.println("there is no path connected them");
            return false;
        } else if (mov.jumpTail() != null) {
//            System.out.println("tail of the move");
            PieceColor there = get(mov.toIndex());
            set(mov.toIndex(), whoseMove());
            set(mov.jumpedIndex(), PieceColor.EMPTY);
            set(mov.fromIndex(), PieceColor.EMPTY);
            boolean result = checkJump(mov.jumpTail(), allowPartial);
            set(mov.toIndex(), there);
            set(mov.jumpedIndex(), whoseMove().opposite());
            set(mov.fromIndex(), whoseMove());
            return result;
        }
        if (allowPartial) {
            return true;
        } else {
            PieceColor there = get(mov.toIndex());
            set(mov.toIndex(), whoseMove());
            if (!jumpPossible(mov.toIndex())) {
                set(mov.toIndex(), there);
                return true;
            } else {
                set(mov.toIndex(), there);
                return false;
            }
        }
        //fixed
        // FIXME
    }

    /** Return true iff a jump is possible for a piece at position C R. */
    boolean jumpPossible(char c, char r) {
        return jumpPossible(index(c, r));
    }

    /** Return true iff a jump is possible for a piece at position with
     *  linearized index K. */
    boolean jumpPossible(int k) {
        if (get(k) == whoseMove()) {
            List<Integer> pos = new ArrayList<Integer>();
            adjacentRow(pos, k);
            adjacentCol(pos, k);
            adjacentDiag(pos, k);
            for (int jumpedIndex : pos) {
                PieceColor curr = get(jumpedIndex);
                if (curr != PieceColor.EMPTY
                        && curr == whoseMove().opposite()) {
                    int diff_col = col(jumpedIndex) - col(k);
                    int diff_row = row(jumpedIndex) - row(k);
                    char toIndex_col = (char) (col(jumpedIndex) + diff_col);
                    char toIndex_row = (char) (row(jumpedIndex) + diff_row);
                    if (validSquare(toIndex_col, toIndex_row)) {
                        int toIndex = index(toIndex_col, toIndex_row);
                        List<Integer> _pos = new ArrayList<>();
                        if (adjacentRow(_pos, jumpedIndex).contains(toIndex)
                                || adjacentCol(_pos, jumpedIndex).contains(toIndex)
                                || adjacentDiag(_pos, jumpedIndex).contains(toIndex)) {
                            if (get(toIndex) == PieceColor.EMPTY) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        //fixed
        return false;

        // FIXME
    }

    /** Return true iff a jump is possible from the current board. */
    boolean jumpPossible() {
        for (int k = 0; k <= MAX_INDEX; k += 1) {
            if (jumpPossible(k)) {
                return true;
            }
        }
        return false;
    }

    /** Return the color of the player who has the next move.  The
     *  value is arbitrary if gameOver(). */
    PieceColor whoseMove() {
        return _whoseMove;
    }

//    /** assume the passed in MOVE is legal and set it to the board */
//    void temporarySet(Move move) {
//        if (legalMove(move)) {
//            if (move.isJump()) {
//                set(move.fromIndex(), PieceColor.EMPTY);
//                set(move.jumpedIndex(), PieceColor.EMPTY);
//                set(move.toIndex(), whoseMove());
//            } else {
//                set(move.toIndex(), whoseMove());
//                set(move.fromIndex(), PieceColor.EMPTY);
//            }
//            _whoseMove = _whoseMove.opposite();
//        } else {
//            System.out.println("getting a invalid move from get moves");
//        }
//    }
//
//    void temporaryReverse(Move move) {
//        if (!legalMove(move)) {
//            System.out.println("can't be undo illegal moves");
//            return;
//        }
//        if (move.isJump()) {
//            set(move.toIndex(), EMPTY);
//            set(move.jumpedIndex(), whoseMove().opposite());
//            set(move.fromIndex(), whoseMove());
//        } else {
//            set(move.toIndex(), PieceColor.EMPTY);
//            set(move.fromIndex(), whoseMove());
//        }
//        _whoseMove = _whoseMove.opposite();
//    }

    /** Assume MOVE is legal JUMP check the Piece of SINGLE MOVE
     * is deleted in this jump or not */
    void checkNonCapMoves(Move move) {
        if (move.jumpTail() != null) {
            checkNonCapMoves(move.jumpTail());
        }
        for (int i = 0; i < _nonCapMoves.size(); i++) {
            Move m = _nonCapMoves.get(i);
            if (m.fromIndex() == move.fromIndex()) {
                _nonCapMoves.remove(m);
            } else if (m.fromIndex() == move.jumpedIndex()) {
                _nonCapMoves.remove(m);
            }
        }
    }




    /** Perform the move C0R0-C1R1, or pass if C0 is '-'.  For moves
     *  other than pass, assumes that legalMove(C0, R0, C1, R1). */
    void makeMove(char c0, char r0, char c1, char r1) {
        makeMove(Move.move(c0, r0, c1, r1, null));
    }

    /** Make the multi-jump C0 R0-C1 R1..., where NEXT is C1R1....
     *  Assumes the result is legal. */
    void makeMove(char c0, char r0, char c1, char r1, Move next) {
        makeMove(Move.move(c0, r0, c1, r1, next));
    }

    /** Make the Move MOV on this Board, assuming it is legal. */
    void makeMove(Move mov) {
        // FIXME
        if (legalMove(mov)) {
            if (mov.isJump()) {
                makeJump(mov);
                checkNonCapMoves(mov);
            } else {
                makeNonCapMove(mov);
            }
            _previousMoves.push(mov);
            if (mov.isLeftMove() || mov.isRightMove()) {
                if (_nonCapMoves.size() != 0) {
                    for (int i = 0; i < _nonCapMoves.size(); i++) {
                        Move curr = _nonCapMoves.get(i);
                        if (curr.fromIndex() == mov.fromIndex()) {
                            _nonCapMoves.remove(curr);
                        }
                    }
                    _nonCapMoves.add(reverseMove(mov));
                } else {
                    _nonCapMoves.add(reverseMove(mov));
                }
            }
            _whoseMove = _whoseMove.opposite();
            _gameOver = gameOver();
        } else {
            throw new GameException("");
        }
        //fixed
        setChanged();
        notifyObservers();
    }

    /** assume MOVE is a legal JUMP and make this jump on the board */
    private void makeJump(Move move) {
        set(move.fromIndex(), PieceColor.EMPTY);
        set(move.jumpedIndex(), PieceColor.EMPTY);
        set(move.toIndex(), whoseMove());
        if (move.jumpTail() != null) {
            makeJump(move.jumpTail());
        }
    }
    /** assume MOVE is a legal MOVE and make this move on the board */
    private void makeNonCapMove(Move move) {
        set(move.fromIndex(), PieceColor.EMPTY);
        set(move.toIndex(), whoseMove());
    }

    /** Undo the last move, if any. */
    void undo() {
        if (_previousMoves.size() != 0) {
            Move mov = _previousMoves.pop();
            if (mov.isJump()) {
                undoJump(mov, whoseMove());
            } else {
                undoMove(mov, whoseMove());
                if (_nonCapMoves.contains(reverseMove(mov))) {
                    _nonCapMoves.remove(reverseMove(mov));
                }
            }
            _gameOver = gameOver();
            _whoseMove = _whoseMove.opposite();
        }
    }

    /** assume MOVE is a legal JUMP has been acted out on board
     * that need to be undo */
    private void undoJump(Move move, PieceColor whosemove) {
        if (move.jumpTail() != null) {
            undoJump(move.jumpTail(), whosemove);
        }
        set(move.toIndex(), PieceColor.EMPTY);
        set(move.jumpedIndex(), whosemove);
        set(move.fromIndex(), whosemove.opposite());
    }

    /** assume MOVE is a legal MOVE has been acted out on board
     * that need to be undo */
    private void undoMove(Move move, PieceColor whosemove) {
        set(move.toIndex(), PieceColor.EMPTY);
        set(move.fromIndex(), whosemove.opposite());
    }

    /** assume MOVE is a legal NON CAP MOVE on board */
    Move reverseMove(Move move) {
        return move(move.col1(), move.row1(), move.col0(), move.row0());
    }

    @Override
    public String toString() {
        return toString(false);
    }

    /** Return a text depiction of the board.  If LEGEND, supply row and
     *  column numbers around the edges. */
    String toString(boolean legend) {
        Formatter out = new Formatter();
        // FIXME
        if (legend) {

        //Fixed without legend
        } else {
            int i = numPieces - 5;
            while (i >= 5) {
                out.format("  %s %s %s %s %s\n", get(i + 0).shortName(),
                        get(i + 1).shortName(), get(i + 2).shortName(),
                        get(i + 3).shortName(), get(i + 4).shortName());
                i -= 5;
            }
            out.format("  %s %s %s %s %s", get(0).shortName(),
                    get(1).shortName(), get(2).shortName(),
                    get(3).shortName(), get(4).shortName());
        }
        return out.toString();
    }

    @Override
    public boolean equals(Object o) {
        Board b = (Board) o;
        for (int i = 0; i < numPieces; i++) {
            if (!get(i).equals(b.get(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /** Return true iff there is a move for the current player. */
    private boolean isMove() {
        if (jumpPossible()) {
            return true;
        } else {
            for (int i = 0; i < MAX_INDEX; i++) {
                ArrayList<Move> non_cap = new ArrayList<>();
//                System.out.println("SEEE THIS IS "+whoseMove()+" MOVE");
                getMoves(non_cap, i);
                if (non_cap.size() != 0) {
                    return true;
                }
            }
            return false;
        }
    }


    /** Player that is on move. */
    private PieceColor _whoseMove;

    /** Set true when game ends. */
    private boolean _gameOver;

    /** Number of pieces on board. */
    private int numPieces;

    /** Current board. */
    private PieceColor [] _board;

    /** Convenience value giving values of pieces at each ordinal position. */
    static final PieceColor[] PIECE_VALUES = PieceColor.values();

    /** An arrayList for storing every moves. */
    private Stack<Move> _previousMoves = new Stack<Move>();

    /** An ArrayList for storing non-Capture moves */
    private List<Move> _nonCapMoves = new ArrayList<>();

    /** Total number of pieces on board. */
    static final int TWENTYFIVE = 25;

    /** One cannot create arrays of ArrayList<Move>, so we introduce
     *  a specialized private list type for this purpose. */
    private static class MoveList extends ArrayList<Move> {
    }

    /** A read-only view of a Board. */
    private class ConstantBoard extends Board implements Observer {
        /** A constant view of this Board. */
        ConstantBoard() {
            super(Board.this);
            Board.this.addObserver(this);
        }

        @Override
        void copy(Board b) {
            assert false;
        }

        @Override
        void clear() {
            assert false;
        }

        @Override
        void makeMove(Move move) {
            assert false;
        }

        /** Undo the last move. */
        @Override
        void undo() {
            assert false;
        }

        @Override
        public void update(Observable obs, Object arg) {
            super.copy((Board) obs);
            setChanged();
            notifyObservers(arg);
        }
    }
}

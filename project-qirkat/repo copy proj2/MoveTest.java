/* Author: Paul N. Hilfinger.  (C) 2008. */

package qirkat;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import static qirkat.Move.*;

/** Test Move creation.
 *  @author Xiyuan Wang
 */
public class MoveTest {

    @Test
    public void testMove1() {
        Move m = move('a', '3', 'b', '2');
        assertNotNull(m);
        assertFalse("move should not be jump", m.isJump());
    }

    @Test
    public void testJump1() {
        Move m = move('a', '3', 'a', '5');
        assertNotNull(m);
        assertTrue("move should be jump", m.isJump());
    }

    @Test
    public void testString() {
        assertEquals("a3-b2", move('a', '3', 'b', '2').toString());
        assertEquals("a3-a5", move('a', '3', 'a', '5').toString());
        assertEquals("a3-a5-c3", move('a', '3', 'a', '5',
                                      move('a', '5', 'c', '3')).toString());
    }

    @Test
    public void testParseString() {
        assertEquals("a3-b2", parseMove("a3-b2").toString());
        assertEquals("a3-a5", parseMove("a3-a5").toString());
        assertEquals("a3-a5-c3", parseMove("a3-a5-c3").toString());
        assertEquals("a3-a5-c3-e1", parseMove("a3-a5-c3-e1").toString());
    }

    /** above is provided */
    @Test
    public void testJumpedCol() {
        Move mov = parseMove("a3-b2");
        assertEquals('b', mov.jumpedCol());
        assertEquals('2', mov.jumpedRow());
        Move mov1 = parseMove("a2-c2");
        System.out.println(mov1.jumpedCol());
        System.out.println(mov1.jumpedRow());
    }
//
//    @Test
//    public void testGetJumps() {
//
//    }
}

package qirkat;

import static qirkat.PieceColor.*;
import static qirkat.Command.Type.*;

/** A Player that receives its moves from its Game's getMoveCmnd method.
 *  @author Xiyuan Wang
 */
class Manual extends Player {

    /** A Player that will play MYCOLOR on GAME, taking its moves from
     *  GAME. */
    Manual(Game game, PieceColor myColor) {
        super(game, myColor);
        _prompt = myColor + ": ";
    }

    @Override
    Move myMove() {
        Command cmd = game().getMoveCmnd(_prompt);
        if (cmd != null) {
            String[] operands = cmd.operands();
            String ope = "";
            for (String opr : operands) {
                ope += opr;
            }
            Move result = Move.parseMove(ope);
            return result;
        } else {
            return null;
        }
        // FIXME
    }

    /** Identifies the player serving as a source of input commands. */
    private String _prompt;
}


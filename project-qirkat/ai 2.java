package qirkat;
import java.util.ArrayList;
import static qirkat.Move.*;
import static qirkat.PieceColor.*;

/** A Player that computes its own moves.
 *  @author CS61B*****.
 */
class AI extends Player {

    /** Maximum minimax search depth before going to static evaluation. */
    private static final int MAX_DEPTH = 8;
    /** A position magnitude indicating a win (for white if positive, black
     *  if negative). */
    private static final int WINNING_VALUE = Integer.MAX_VALUE - 1;
    /** A magnitude greater than a normal value. */
    private static final int INFTY = Integer.MAX_VALUE;

    /** A new AI for GAME that will play MYCOLOR. */
    AI(Game game, PieceColor myColor) {
        super(game, myColor);
    }

    @Override
    Move myMove() {
        Main.startTiming();
        Move move = findMove();
        Main.endTiming();
        if (move != null) {
            game().reportMove("%s moves %s.", myColor(), move.toString());
        }
        return move;
    }

    /** Return a move for me from the current position, assuming there
     *  is a move. */
    private Move findMove() {
        Board b = new Board(board());
        if (myColor() == WHITE) {
            findMove(b, MAX_DEPTH, true, 1, -INFTY, INFTY);
        } else {
            findMove(b, MAX_DEPTH, true, -1, -INFTY, INFTY);
        }
        return _lastFoundMove;
    }

    /** The move found by the last call to one of the ...FindMove methods
     *  below. */
    private Move _lastFoundMove;

    /** Find a move from position BOARD and return its value, recording
     *  the move found in _lastFoundMove iff SAVEMOVE. The move
     *  should have maximal value or have value > BETA if SENSE==1,
     *  and minimal value or value < ALPHA if SENSE==-1. Searches up to
     *  DEPTH levels.  Searching at level 0 simply returns a static estimate
     *  of the board value and does not set _lastMoveFound. */
    private int findMove(Board board, int depth, boolean saveMove, int sense,
                         int alpha, int beta) {
        Move best;
        best = null;
        if (depth == 0 || board.getMoves().isEmpty()) {
            return staticScore(board);
        }

        int score = sense == 1 ? -INFTY : INFTY;
        if (sense == 1) {
            int value;
            ArrayList<Move> moveList = board.getMoves();
            for (Move move : moveList) {
                board.tempMove(move);
                board.changePlayer();
                value = findMove(board, depth - 1, false,
                        -1, Math.max(score, alpha), beta);
                if (value > score) {
                    best = move;
                    score = value;
                }
                if (score <= beta) {
                    break;
                }
                board.tempUndo(move);
                board.changePlayer();
            }

        }
        if (sense == -1) {
            ArrayList<Move> moveList2 = board.getMoves();
            int value2;
            for (Move move : moveList2) {
                board.tempMove(move);
                board.changePlayer();
                value2 = findMove(board, depth - 1, false,
                        1, alpha, Math.min(score,beta));
                if (value2 < score) {
                    score = value2;
                    best = move;
                }
                if (score >= alpha) {
                    break;
                }
                board.tempUndo(move);
                board.changePlayer();
            }

        }

        if (saveMove) {
            _lastFoundMove = best;
        }

        return score;
    }


    /** Return a heuristic value for BOARD. */
    private int staticScore(Board board) {
        int value = 0;
        for (int i = 0; i < MAX_INDEX + 1; i++) {
            if (board.get(i) == WHITE) {
                value += 1;
            } else if (board.get(i) == BLACK) {
                value -= 1;
            }
        }
        return value;
    }

}

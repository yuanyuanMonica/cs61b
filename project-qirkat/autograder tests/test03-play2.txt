
===TEST 03-play2.inp===

Test script 03-play2.inp:
--------------------------------------------------
# Have the program play against itself as two programs.
java -ea qirkat.Main
auto White
manual Black
seed 1237
start
@send white...
---------------------
java -ea qirkat.Main
seed 3948
start
@recv white...
--------------------------------------------------

package qirkat;


import com.sun.tools.doclets.formats.html.SourceToHTMLConverter;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;

import static qirkat.PieceColor.*;
import static qirkat.Move.*;

/** A Qirkat board.   The squares are labeled by column (a char value between
 *  'a' and 'e') and row (a char value between '1' and '5'.
 *
 *  For some purposes, it is useful to refer to squares using a single
 *  integer, which we call its "linearized index".  This is simply the
 *  number of the square in row-major order (with row 0 being the bottom row)
 *  counting from 0).
 *
 *  Moves on this board are denoted by Moves.
 *  @author yuchen hua
 */
class Board extends Observable {
    /**Maximum number of pieces that can be on the board. */
    public static final int MAXPIECES = 25;

    /**
     * A new, cleared board at the start of the game.
     */
    Board() {
        clear();
    }

    /**
     * A copy of B.
     */
    Board(Board b) {
        internalCopy(b);
    }

    /**
     * Return a constant view of me (allows any access method, but no
     * method that modifies it).
     */
    Board constantView() {
        return this.new ConstantBoard();
    }

    /**
     * Clear me to my starting state, with pieces in their initial
     * positions.
     */
    void clear() {
        _whoseMove = WHITE;
        _gameOver = false;

        setPieces(initialBoard, whoseMove());

        setChanged();
        notifyObservers();
    }

    /**
     * Copy B into me.
     */
    void copy(Board b) {
        internalCopy(b);
    }

    /**
     * Copy B into me.
     */
    private void internalCopy(Board b) {
        // FIXME
        for (int i = 0; i < MAXPIECES; i++) {
            set(i, b.get(i));
        }

        previousMove = new Stack<Move>();
        previousMove.addAll(b.previousMove);
        _gameOver = b.gameOver();
        _whoseMove = b.whoseMove();

    }

    /**
     * Set my contents as defined by STR.  STR consists of 25 characters,
     * each of which is b, w, or -, optionally interspersed with whitespace.
     * These give the contents of the Board in row-major order, starting
     * with the bottom row (row 1) and left column (column a). All squares
     * are initialized to allow horizontal movement in either direction.
     * NEXTMOVE indicates whose move it is.
     */
    void setPieces(String str, PieceColor nextMove) {
        if (nextMove == EMPTY || nextMove == null) {
            throw new IllegalArgumentException("bad player color");
        }
        str = str.replaceAll("\\s", "");
        if (!str.matches("[bw-]{25}")) {
            throw new IllegalArgumentException("bad board description");
        }

        // FIXME
        _whoseMove = nextMove;

        //k iterates through a single string -- make it update the hashmap
        //actually update hte board too

        //everything below is given code

        for (int k = 0; k < str.length(); k += 1) {
            switch (str.charAt(k)) { case '-':
                set(k, EMPTY); //sets k to empty
                break;
            case 'b':
            case 'B':
                set(k, BLACK);
                break;
            case 'w':
            case 'W':
                set(k, WHITE);
                break;
            default:
                break;
            }
        }

        //copyPieces should actually be parsing the given string
        ArrayList<PieceColor> copyPieces = new ArrayList<PieceColor>();
        for (int i = 0; i < pieces.length; i++) {
            copyPieces.add(pieces[i]);
        }


        //_whoseMove = nextMove;

        // FIXME

        setChanged();
        notifyObservers();
    }

    /**
     * Return true iff the game is over: i.e., if the current player has
     * no moves.
     */
    boolean gameOver() {
        if (!isMove()) {
           // System.out.println("game is over");
            _gameOver =true;
        } else {
            _gameOver = false;
        }
        return _gameOver;
    }

    /**
     * Return the current contents of square C R, where 'a' <= C <= 'e',
     * and '1' <= R <= '5'.
     */
    PieceColor get(char c, char r) {
        assert validSquare(c, r);
        return get(index(c, r)); // FIXME?//fixed

        //is there an index function? unless it is inbuilt in hashmap
    }

    /**
     * Return the current contents of the square at linearized index K.
     */
    PieceColor get(int k) {
        assert validSquare(k);

        return pieces[k]; // FIXME //fixed
        //see get method from last project
    }

    /**
     * Set get(C, R) to V, where 'a' <= C <= 'e', and
     * '1' <= R <= '5'.
     */
    private void set(char c, char r, PieceColor v) {
        assert validSquare(c, r);
        set(index(c, r), v);  // FIXME? //fixed?
        //figure out what the index function does
    }

    /**
     * Set get(K) to V, where K is the linearized index of a square.
     */
    private void set(int k, PieceColor v) {
        assert validSquare(k);
        // FIXME//fixed
        //array of pieces -- check necessity of hashmap
        pieces[k] = v;
    }

    /**
     * Return true iff MOV is legal on the current board.
     */
    boolean legalMove(Move mov) {
        //only want to check current position's move
        int beginIndex = mov.fromIndex();
        int toIndex = mov.toIndex();
        if (pieces[beginIndex].equals(whoseMove())) {
       //     System.out.println("starting with " + whoseMove().toString());
            if (!mov.isJump()) { //if the move is not a jump
                for (int i = 0; i < pieces.length; i++) {
                    if (pieces[i].equals(whoseMove())) { //if the piece is your color
                        if (jumpPossible(i)) { //if there is a jump possbile for a piece of your color
                        //    System.out.println("Jumps are possible for" + whoseMove().toString());
                            return false;
                        }
                    }
                } //a piece may not move back to a previous position unless there has been a jump in between moves
                ArrayList<Integer> adj = adjacentSpaces(beginIndex);
                if (adj.contains(toIndex)) { //if the toindex is adjacent
                    if (pieces[toIndex].equals(EMPTY)) { //if the to space is empty
                    /*    if (!IndicesSinceJump.empty() && IndicesSinceJump.contains(toIndex)) {
                       //     System.out.println("cannot move to a previous spot");
                            return false;
                        }*/
                        //check for backwards moves
                        if (whoseMove().equals(WHITE)) {
                            //
                            if (beginIndex > 20) {
                                return false;
                            }
                            //
                       //     System.out.println("white turn");
                            if (toIndex > beginIndex) {
                                return true;
                            } else if (toIndex == (beginIndex - 1) && beginIndex % 5 != 0) {
                                System.out.println("print");
                                return true;
                            }

                        } else {
                            //
                            if (beginIndex < 5) {
                                return false;
                            }
                            //
                            if (toIndex < beginIndex ||
                                    (beginIndex % 5 != 4 && toIndex == beginIndex + 1)) {
                                return true;
                            }
                        }
                    }
                } else {
                    return false;
                }

            } else {
              //  System.out.println("is jump");
                if (checkJump(mov, false)) {
                    //System.out.println("checking jump");
                    return true;
                }
                //return false; // FIXME
            }
        }
            return false;

    }

    /**
     * Return a list of all legal moves from the current position.
     */
    ArrayList<Move> getMoves() {
        ArrayList<Move> result = new ArrayList<>();
        getMoves(result);
        return result;
    }

    /**
     * Add all legal moves from the current position to MOVES.
     */
    void getMoves(ArrayList<Move> moves) {
        if (gameOver()) {
            return;
        }

            if (jumpPossible()) {
                for (int k = 0; k <= MAX_INDEX; k += 1) {
                    getJumps(moves, k);
                    //System.out.println(moves.size());
                }
            } else {
                for (int k = 0; k <= MAX_INDEX; k += 1) {
                    getMoves(moves, k);
                }
            }


    }

    /**
     * Add all legal non-capturing moves from the position
     * with linearized index K to MOVES.
     */
    public void getMoves(ArrayList<Move> moves, int k) { //change to private
        // FIXME //fixed for now--edit later
       if (pieces[k] == whoseMove()) {
            if (pieces[k].equals(WHITE)) {
                ArrayList<Integer> adj = adjacentSpaces(k);
                for (int place : adj) {
                    if (k < 20) {
                        if (place >= k || (place == (k - 1))) { //if index is greater or to the left or k isnt on last row
                            if (pieces[place].equals(EMPTY)) {
                                Move m = move(col(k), row(k), col(place), row(place));
                                moves.add(m);
                            }
                        }
                    }
                }
            } else {
                ArrayList<Integer> adj = adjacentSpaces(k);
                for (int place : adj) {
                    if (k > 4) {
                        if (place <= k || (place == (k + 1))) {
                            if (pieces[place].equals(EMPTY)) {
                                Move m = move(col(k), row(k), col(place), row(place));
                                moves.add(m);
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * Add all legal captures from the position with linearized index K
     * to MOVES.
     */
    private void getJumps(ArrayList<Move> moves, int k) { //turn back to private
        // FIXME
        ArrayList<Integer> adjacentSpaces = adjacentSpaces(k);
        for (int space : adjacentSpaces) {

            if (!jumpPossible(k)) {

                break;
            } else {
                if (pieces[k].equals(whoseMove())) {
                    if (pieces[space].equals(whoseMove().opposite())) {
                        int difference = space - k;
                        int next = difference + space;
                        if (validSquare(next)) {
                            if (pieces[next].equals(EMPTY)) {

                                getJumpsHelper(moves, move(col(k), row(k), col(next), row(next)));

                            }
                        }
                    }
                }
            }
        }
        //System.out.println(moves.size());
        return;
    }

    //private //Tree
    private void getJumpsHelper(ArrayList<Move> moves, Move m) {
        set(m.fromIndex(), EMPTY);
        set(m.toIndex(), whoseMove());
        set(m.jumpedIndex(), EMPTY);
        ArrayList<Move> nextPossible = new ArrayList<Move>();
        getJumps(nextPossible, m.toIndex());
        if (nextPossible.isEmpty()) {
            nextPossible.add(m);
            moves.add(m);
        } else {
            for (Move move : nextPossible) {
                Move concat = move(m, move);
                moves.add(concat);
            }
        }
        set(m.fromIndex(), whoseMove());
        set(m.toIndex(), EMPTY);
        set(m.jumpedIndex(), whoseMove().opposite());
    }

 /*   private void getFirstJumps(ArrayList<Move> moves, int k) {
        if (!jumpPossible(k)) {
            return;
        }
        ArrayList<Integer> adjacentSpaces = adjacentSpaces(k);
        for (int space : adjacentSpaces) {
            if (pieces[space].equals(whoseMove().opposite())) {
                int difference = space - k;
                int next = difference + space;
                if (pieces[next].equals(EMPTY)) { //if this is a valid jump
                    //call a helper
                    Move validSingle = move(col(k), row(k), col(next), row(next));
                    moves.add(validSingle);
                    //System.out.println(validSingle.toString());
                }
            }
        }
    }*/

 /*   private Move getJumpsHelper(ArrayList<Move> moves, int k) {
        if (!jumpPossible(k)) {
            return null;
        }
        for (Move m : moves) {
            if (jumpPossible(m.toIndex())) {
                System.out.println(m.toString());
                moves.add(move(m, getJumpsHelper(moves, m.toIndex()))); //if a jump is possible, recurse
            } else {
                moves.add(m);
            }
        }
        return null;
    }*/

    /**
     * Return true iff MOV is a valid jump sequence on the current board.
     * MOV must be a jump or null.  If ALLOWPARTIAL, allow jumps that
     * could be continued and are valid as far as they go.
     */
    boolean checkJump(Move mov, boolean allowPartial) {
        boolean result;
        ArrayList<Integer> adj = adjacentSpaces(mov.fromIndex());
        if (mov == null) {
            return true;
        }
        if (!allowPartial && mov.jumpTail() == null && jumpPossible(mov.toIndex())) {
          //  System.out.println("there are still jumps");
          //  System.out.println(mov.toString());
            return false;
        }
        if (!adj.contains(mov.jumpedIndex())){ //if jumped index isnt adjacent
         //   System.out.println(mov.jumpedIndex());
          //  System.out.println(adj.get(2));
            return false;
        }

        if (!pieces[mov.fromIndex()].equals(whoseMove()) ||
                !pieces[mov.jumpedIndex()].equals(whoseMove().opposite()) ||
                !pieces[mov.toIndex()].equals(EMPTY)) {
            return false;
        }
        if (mov.jumpTail() != null) {
            set(mov.fromIndex(), EMPTY);
            set(mov.toIndex(), whoseMove());
            set(mov.jumpedIndex(), EMPTY);
            result = checkJump(mov.jumpTail(), allowPartial);
            set(mov.fromIndex(), whoseMove());
            set(mov.toIndex(), EMPTY);
            set(mov.jumpedIndex(), whoseMove().opposite());
            //return true;
        } else if (allowPartial) {
            result = true;
        }
        else { //if you dont have a tail and allow partial is false
            if (!jumpPossible(mov.toIndex())) {
            //    System.out.println("yay");
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }

    /**
     * Return true iff a jump is possible for a piece at position C R.
     */
    boolean jumpPossible(char c, char r) {
        return jumpPossible(index(c, r));
    }

    /**
     * Return true iff a jump is possible for a piece at position with
     * linearized index K.
     */
    boolean jumpPossible(int k) {
        //for an empty space
            ArrayList<Integer> surroundings = adjacentSpaces(k);
            for (int space : surroundings) {
                if (pieces[space].equals(whoseMove().opposite())) {
                    int difference = space - k;
                    try {
                        if (pieces[space + difference].equals(EMPTY)) {
                            return true;
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                    }
                }
            }
            return false;
        //return false; // FIXME //fixed
    }

    /**
     * Return true iff a jump is possible from the current board.
     */
    boolean jumpPossible() {
        for (int k = 0; k <= MAX_INDEX; k += 1) {
            if (jumpPossible(k)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return the color of the player who has the next move.  The
     * value is arbitrary if gameOver().
     */
    PieceColor whoseMove() {
        return _whoseMove;
    }

    /**
     * Perform the move C0R0-C1R1, or pass if C0 is '-'.  For moves
     * other than pass, assumes that legalMove(C0, R0, C1, R1).
     */
    void makeMove(char c0, char r0, char c1, char r1) {
        makeMove(Move.move(c0, r0, c1, r1, null));
    }

    /**
     * Make the multi-jump C0 R0-C1 R1..., where NEXT is C1R1....
     * Assumes the result is legal.
     */
    void makeMove(char c0, char r0, char c1, char r1, Move next) {
        makeMove(Move.move(c0, r0, c1, r1, next));
    }

    /**
     * Make the Move MOV on this Board, assuming it is legal.
     */
    void makeMove(Move mov) {
            // FIXME
            PieceColor piece = whoseMove();
            if (mov.isJump()) {
                IndicesSinceJump.clear();
                int jumpedIndex = mov.jumpedIndex();
                int fromInd = mov.fromIndex();
                int toInd = mov.toIndex();
                set(jumpedIndex, EMPTY);
                set(fromInd, EMPTY);
                set(toInd, piece);
                if (mov.jumpTail() != null) {
                    Move mov1 = mov.jumpTail();
                    makeMove(mov1);
                }
            } else {

                int fromIndex = mov.fromIndex();
                int toIndex = mov.toIndex();
                set(fromIndex, EMPTY);
                set(toIndex, piece);
                previousMove.push(mov);
                if (!IndicesSinceJump.contains(fromIndex)) {
                    IndicesSinceJump.push(fromIndex);
                } else if (!IndicesSinceJump.contains(toIndex)) {
                    IndicesSinceJump.push(toIndex);
                }
                    //_whoseMove = _whoseMove.opposite();
                }

            if (mov.jumpTail() == null) {
                _whoseMove = _whoseMove.opposite();
            }
           // previousMove.push(mov);
            setChanged();
            notifyObservers();
        }



    /** Undo the last move, if any. */

    void undo() {
        if (previousMove.size() != 0) {
            Move prev = previousMove.pop();
            undoJump(undoMove(prev), whoseMove());
            if (previousMove.size() != 0) {
                Move next = previousMove.peek();
                while (next != null
                        && prev.jumpTail() != null
                        && next.equals(prev.jumpTail())) {
                    previousMove.pop();
                    next = previousMove.peek();
                    prev = next;
                }
            }
            _whoseMove = _whoseMove.opposite();
        }
        //_whoseMove = _whoseMove.opposite();
        setChanged();
        notifyObservers();
    }
    //edit later
    /**Returns a move that is the reverse of the last move.
     * Takes in a Move MOVE to be reversed. */
    private Move undoMove(Move move) {
        Move next = move.jumpTail();
        if (next == null) {
            return move(move.col1(), move.row1(), move.col0(), move.row0());
        } else {
            Move move0 = undoMove(next);
            Move move1 = move(move.col0(), move.row0());
            return move(move0, move1);
        }
    }
    /**Actually performs the undo move. Takes in parameters MOVE,
     * a reversing move, and Piececolor PIECE to move. */
    private void undoJump(Move move, PieceColor piece) {
        if (move.isJump()) {
            set(move.jumpedIndex(), piece);
            set(move.toIndex(), piece.opposite());
            set(move.fromIndex(), EMPTY);
            if (move.jumpTail() != null) {
                undoJump(move.jumpTail(), piece);
            }
        } else {
            set(move.fromIndex(), EMPTY);
            set(move.toIndex(), piece.opposite());
        }
    }

    @Override
    public String toString() {
        return toString(false);
    }

    /** Return a text depiction of the board.  If LEGEND, supply row and
     *  column numbers around the edges. */
    String toString(boolean legend) {
        Formatter out = new Formatter();
        // FIXME
        //
        for (int i = MAXPIECES - 5; i >= 5; i -= 5) {
            out.format("  %s %s %s %s %s",
                    pieces[i + 0].shortName(), pieces[i + 1].shortName(),
                    pieces[i + 2].shortName(), pieces[i + 3].shortName(),
                    pieces[i + 4].shortName());
            out.format("\n");
        }
        out.format("  %s %s %s %s %s",
                pieces[0].shortName(), pieces[1].shortName(),
                pieces[2].shortName(),
                pieces[3].shortName(), pieces[4].shortName());

        return out.toString();
    }

    /** Return true iff there is a move for the current player. */
    public boolean isMove() { //change to private
        ArrayList<Move> possibleMoves = new ArrayList<Move>();
        for (int i = 0; i < pieces.length; i++ ) {
            if (pieces[i].equals(whoseMove())) {
                getJumps(possibleMoves, i);
                getMoves(possibleMoves, i);
            }
        }
        if (possibleMoves.size() == 0) {
        //    System.out.println("there are no moves");
            return false;
        } else {
       //     System.out.println(possibleMoves.get(0));
            return true;
        }
        //return true;// FIXME
    }


    /** Player that is on move. */
    private PieceColor _whoseMove;

    /** Set true when game ends. */
    private boolean _gameOver;

    /** Convenience value giving values of pieces at each ordinal position. */
    static final PieceColor[] PIECE_VALUES = PieceColor.values();

    /** One cannot create arrays of ArrayList<Move>, so we introduce
     *  a specialized private list type for this purpose. */
    private static class MoveList extends ArrayList<Move> {
    }

    /** A read-only view of a Board. */
    private class ConstantBoard extends Board implements Observer {
        /** A constant view of this Board. */
        ConstantBoard() {
            super(Board.this);
            Board.this.addObserver(this);
        }

        @Override
        void copy(Board b) {
            assert false;
        }

        @Override
        void clear() {
            assert false;
        }

        @Override
        void makeMove(Move move) {
            assert false;
        }

        /** Undo the last move. */
        @Override
        void undo() {
            assert false;
        }

        @Override
        public void update(Observable obs, Object arg) {
            super.copy((Board) obs);
            setChanged();
            notifyObservers(arg);
        }
    }

    /**String representing a cleared board. */
    private String initialBoard =
            "  w w w w w\n  w w w w w\n  b b - w w\n  b b b b b\n  b b b b b";

    /**Stack that holds all previous moves. */
    private Stack<Move> previousMove = new Stack<Move>();

    /**Array that holds the entire board. */
    public PieceColor[] pieces = new PieceColor[MAXPIECES]; //change back to private
    //edit later

    /**Number of pieces on the board. */
    //private int numPieces = pieces.length;

    /**Stack containing all the non-capturing moves since the last jump*/
    private Stack<Integer> IndicesSinceJump = new Stack<Integer>();


    /** Equals method to override old one. */
    @Override
    public boolean equals(Object obj) {
        Board b = (Board) obj;
        for (int i = 0; i < pieces.length; i++) {
            if (get(i).equals(b.get(i))) {
                return true;
            }
        }
        return false;

    }
    /**Return new HashCode to override old one.  */
    public int hashCode() { //edit later
        return  super.hashCode();
    }

    /**Returns an integer array of spaces adjacent to k */
    public ArrayList<Integer> adjacentSpaces(int k) { //change back to privatem
        ArrayList<Integer> possiblePlaces = new ArrayList<Integer>();
        if (k % 5 != 0) { //move left
            possiblePlaces.add(k - 1);
        }
        if (k % 5 != 4) { //move right
            possiblePlaces.add(k + 1);
        }
        if (k > 4) { //move down
            possiblePlaces.add(k - 5);
            if (k % 2 == 0) {//can move diagonally down to the left
                if (k % 5 != 0) {
                    possiblePlaces.add(k - 6);
                }
                if (k % 5 != 4) { //can move diagonally down to the right
                    possiblePlaces.add(k - 4);
                }
            }
        }
        if (k < 20) {//move up
            possiblePlaces.add(k + 5);
            if (k % 2 == 0) {
                //can move diagonally up to the right
                if (k % 5 != 4) {
                    possiblePlaces.add(k + 6);
                }
                if (k % 5 != 0) {
                    possiblePlaces.add(k + 4);
                }

            }
        }
       return possiblePlaces;
    }

    public int boardSize() {
        return MAXPIECES;
    }



  /*  public void switchPlayer() {
        System.out.println("switching players");
        _whoseMove = _whoseMove.opposite();
    } */




}

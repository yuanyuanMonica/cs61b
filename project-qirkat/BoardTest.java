package qirkat;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.ArrayList;
/** Tests of the Board class.
 *  @author JIN LIU
 */
public class BoardTest {

    private static final String INIT_BOARD =
        "  b b b b b\n  b b b b b\n  b b - w w\n  w w w w w\n  w w w w w";

    private static final String[] GAME1 =
    { "c2-c3", "c4-c2",
      "c1-c3", "a3-c1",
      "c3-a3", "c5-c4",
      "a3-c5-c3",
    };

    private static final String GAME1_BOARD =
        "  b b - b b\n  b - - b b\n  - - w w w\n  w - - w w\n  w w b w w";

    private static void makeMoves(Board b, String[] moves) {
        for (String s : moves) {
            b.makeMove(Move.parseMove(s));
        }
    }

    @Test
    public void testInit1() {
        Board b0 = new Board();
        assertEquals(INIT_BOARD, b0.toString());
    }

    @Test
    public void testLegalMove() {
        Board b = new Board();
        Board b0 = new Board();

    }


    @Test
    public void testMoves1() {
        Board b0 = new Board();
        makeMoves(b0, GAME1);
        assertEquals(GAME1_BOARD, b0.toString());
    }

    @Test
    public void testUndo() {
        Board b0 = new Board();
        Board b1 = new Board(b0);
        makeMoves(b0, GAME1);
        Board b2 = new Board(b0);
        for (int i = 0; i < GAME1.length; i += 1) {
            b0.undo();
        }
        assertEquals("failed to return to start", b1, b0);
        makeMoves(b0, GAME1);
        assertEquals("second pass failed to reach same position", b2, b0);
    }

    @Test
    public void testIndex() {
        Board bo = new Board();
        assertEquals("w", bo.get(0).shortName());
        assertEquals("b", bo.get('a', '5').shortName());
        assertEquals("b", bo.get('a', '3').shortName());
    }

    @Test
    public void testSetPieces() {
        Board bo = new Board();
        PieceColor v = PieceColor.WHITE;
        bo.setPieces("w---w ----- ----- ----- bb---", v);
    }

    @Test
    public void testGetMoves() {
        Board b0 = new Board();
        ArrayList<Move> expectMoves = new ArrayList<>();
        ArrayList<Move> move = new ArrayList<>();
        expectMoves.add(Move.move('b', '2', 'c', '3'));
        expectMoves.add(Move.move('c', '2', 'c', '3'));
        expectMoves.add(Move.move('d', '2', 'c', '3'));
        expectMoves.add(Move.move('d', '3', 'c', '3'));
        assertEquals(b0.getMoves(), expectMoves);
    }

    @Test
    public void testGetJumps() {
        Board b0 = new Board();
        PieceColor v = PieceColor.WHITE;
        b0.setPieces("bb-bb b--bb --www w--ww wwbww", v);
        ArrayList<Move> move = new ArrayList<>();
        move.add(Move.move('e', '3', 'c', '1'));
        assertEquals(move, b0.getMoves());
    }

    @Test
    public void testgetBalck() {
        Board b0 = new Board();
        PieceColor v = PieceColor.BLACK;
        b0.setPieces("wwwww ww-ww bbwww bbbbb bbbbb", v);
        Move mov = Move.parseMove("c4-c2");
        assertTrue(b0.legalMove(mov));

    }

    @Test
    public void testAIundo() {
        Board b0 = new Board();
        Move mov1 = Move.parseMove("c2-c3");
        Move mov2 = Move.parseMove("c4-c2");
        b0.makeMove(mov1);
        b0.makeMove(mov2);
        b0.undo();
    }

    @Test
    public void testgetMove() {
        Board b0 = new Board();
        PieceColor v = PieceColor.BLACK;
        b0.setPieces("w--w- wwb-w bw--w b---- bbbbb", v);
        assertEquals(2, b0.getMoves().size());
    }

    @Test
    public void testContin() {
        Board b0 = new Board();
        PieceColor v = PieceColor.WHITE;
        b0.setPieces("---w- ---b- ----- ---b- --b--", v);
        ArrayList<Move> movesl = b0.getMoves();
        System.out.println(movesl.size());
        ArrayList<Move> moves = new ArrayList<>();
        for(int i = 0; i < 25; i++){
            b0.getVestigal(moves, i);
        }
        System.out.println(moves.size());
        for(int i = 0; i < moves.size(); i++){
            System.out.println(moves.get(i).toString());
        }
    }

    @Test
    public void testcase() {
        Board b0 = new Board();
        PieceColor v = PieceColor.WHITE;
        b0.setPieces("----- ----- ---w- ---b- --b--", v);
        System.out.println(b0.getMoves().get(0));
    }


}

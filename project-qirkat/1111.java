package qirkat;

import static qirkat.PieceColor.*;

import java.util.ArrayList;
import static java.lang.Math.*;

/** A Player that computes its own moves.
 *  @author yuchen hua
 */
class AI extends Player {

    /** Maximum minimax search depth before going to static evaluation. */
    //private static final int MAX_DEPTH = 8;
    private static final int MAX_DEPTH = 5;
    /** A position magnitude indicating a win (for white if positive, black
     *  if negative). */
    private static final int WINNING_VALUE = Integer.MAX_VALUE - 1;
    /** A magnitude greater than a normal value. */
    private static final int INFTY = Integer.MAX_VALUE;

    /** A new AI for GAME that will play MYCOLOR. */
    AI(Game game, PieceColor myColor) {
        super(game, myColor);
    }

    @Override
    Move myMove() {
        Main.startTiming();
        Move move = findMove();
        Main.endTiming();

        // FIXME
        //return move;
        return findMove();
    }

    /** Return a move for me from the current position, assuming there
     *  is a move. */
    private Move findMove() {
        Board b = new Board(board());
        if (myColor() == WHITE) {
            findMove(b, MAX_DEPTH, true, 1, -INFTY, INFTY);
        } else {
            findMove(b, MAX_DEPTH, true, -1, -INFTY, INFTY);
        }
        return _lastFoundMove;
    }

    /** The move found by the last call to one of the ...FindMove methods
     *  below. */
    private Move _lastFoundMove;

    /** Find a move from position BOARD and return its value, recording
     *  the move found in _lastFoundMove iff SAVEMOVE. The move
     *  should have maximal value or have value > BETA if SENSE==1,
     *  and minimal value or value < ALPHA if SENSE==-1. Searches up to
     *  DEPTH levels.  Searching at level 0 simply returns a static estimate
     *  of the board value and does not set _lastMoveFound. */
    private int findMove(Board board, int depth, boolean saveMove, int sense, //the int is the best score so far
                         int alpha, int beta) {
        //everything below is given code
        Move best;
        best = null;

        //FIXME
        int scoreSofar;
        if (sense  > 0) {
            scoreSofar = FindMax(board, depth, alpha, beta);
        } else {
            scoreSofar = FindMin(board, depth, alpha, beta);
        }


    /*    if (saveMove) {
            _lastFoundMove = best;
        }*/
        //everything above is given code
        return scoreSofar;
        //return best; // FIXME
    }


    private int FindMax(Board board, int depth, int alpha, int beta) {
        if (depth == 0 || board.gameOver()) {
            return SimpleFindMax(board, alpha, beta);
        }
        Move best;
        best = null;
        int bestScoresoFar = 0 - INFTY;
        ArrayList<Move> possible = board.getMoves();
        for (Move m : possible) {
            Board b = new Board();
            b.copy(board);
            b.makeMove(m);
            //int score = staticScore(b);
            int response = FindMin(b, depth-1, alpha, beta);
            if (response >= bestScoresoFar) {
                best = m;
                _lastFoundMove = best;
                bestScoresoFar = response;
                alpha = max(alpha, response);
                if (beta <= alpha) {
                    break;
                }

            }
        }
        return bestScoresoFar;

    }

    private int FindMin(Board board, int depth, int alpha, int beta) {
        if (depth == 0 || board.gameOver()) {
            return SimpleFindMin(board, alpha, beta);
        }
        Move best;
        best = null;
        int bestScoresoFar = INFTY;
        ArrayList<Move> possible = board.getMoves();
        for (Move m : possible) {
            Board b = new Board();
            b.copy(board);
            b.makeMove(m);
            //int score = staticScore(b);
            int response = FindMin(b, depth-1, alpha, beta);
            if (response <= bestScoresoFar) {
                best = m;
                _lastFoundMove = best;
                bestScoresoFar = response;
                beta = min(beta, response);
                if (beta <= alpha) {
                    break;
                }

            }
        }
        return bestScoresoFar;

    }


    private int SimpleFindMax(Board board, int alpha, int beta) {
        if (board.gameOver() && WINNING_VALUE > 0) {
            return INFTY;
        } else if (board.gameOver() && WINNING_VALUE < 0) {
            return 0 - INFTY;
        }
        Move best;
        best = null;
        int bestScoresoFar = 0 - INFTY;
        ArrayList<Move> possible = board.getMoves();
        if (possible.size() == 0) {
            System.out.println("no moves");
        } else {
            for (Move m : possible) {
                Board b = new Board();
                b.copy(board);
                b.makeMove(m);
                int score = staticScore(b);
                if (score >= bestScoresoFar) {
                    bestScoresoFar = score;
                    best = m;
                    _lastFoundMove = best;
                    alpha = max(alpha, score);
                    if (beta >= alpha) {
                        break;
                    }
                }


            }
        }
        return bestScoresoFar;
    }

    private int SimpleFindMin(Board board, int alpha, int beta) {
        if (board.gameOver() && WINNING_VALUE > 0) {
            return INFTY;
        } else if (board.gameOver() && WINNING_VALUE < 0) {
            return 0 - INFTY;
        }
        Move best;
        best = null;
        int bestScoresoFar = INFTY;
        ArrayList<Move> possible = board.getMoves();
        if (possible.size() == 0) {
            System.out.println("no moves");
        } else {
            for (Move m : possible) {
                Board b = new Board();
                b.copy(board);
                b.makeMove(m);
                int score = staticScore(b);
                if (score >= bestScoresoFar) {
                    bestScoresoFar = score;
                    best = m;
                    _lastFoundMove = best;
                    beta = min(beta, score);
                    if (beta >= alpha) {
                        break;
                    }
                }

            }
        }
        return bestScoresoFar;
    }



    /** Return a heuristic value for BOARD. */
    private int staticScore(Board board) {
        //iterate through every square on the board, returning 1 for white and -1 for black
        int scoreWhite = 0;
        int scoreBlack = 0;
        for (int i = 0; i < board.boardSize(); i++ ) {
            if (board.get(i).equals(WHITE)) {
                scoreWhite += 1;
            } else if (board.get(i).equals(BLACK)) {
                scoreBlack += 1;
            }
        }
        return scoreWhite - scoreBlack; // FIXME
        //this makes black the minimizer and white the maximizer
    }
}

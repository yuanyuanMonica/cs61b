
make -f /home/ff/cs61b/testing-files/proj2/testing.mk --no-print-directory prepare
find . -name '*.class' -exec rm -f {} \;
find . -name '*~' -exec rm -f {} \;
find . -name sentinel -exec rm -f {} \;
rm -f -r style61b python3 python make load-files
rm -f load-files proj2/load-files
ln -s /home/ff/cs61b/testing-files/proj2/testing load-files; ln -s /home/ff/cs61b/testing-files/proj2/testing proj2/load-files

=============================================

I. Compiling your program with 'make'.


make -C qirkat default
javac -Xlint:unchecked -encoding utf-8 -cp "..:.:/home/ff/cs61b/lib/*:;..;.:/home/ff/cs61b/lib/*" AI.java Board.java BoardTest.java BoardWidget.java Command.java CommandSource.java CommandSources.java CommandTest.java GUI.java Game.java GameException.java Main.java Manual.java MoreBoardTests.java Move.java MoveTest.java PieceColor.java Player.java ReaderSource.java Reporter.java TextReporter.java UnitTest.java
Board.java:86: warning: [unchecked] unchecked cast
        _previousMoves = (Stack<Move>) b._previousMoves.clone();
                                                             ^
  required: Stack<Move>
  found:    Object
1 warning
touch sentinel

=============================================

II. Running style61b on ALL .java files found, whether or not used.

style61b ./qirkat/AI.java ./qirkat/PieceColor.java ./qirkat/Player.java ./qirkat/Manual.java ./qirkat/BoardWidget.java ./qirkat/GameException.java ./qirkat/ReaderSource.java ./qirkat/Command.java ./qirkat/Move.java ./qirkat/CommandSource.java ./qirkat/CommandSources.java ./qirkat/BoardTest.java ./qirkat/Game.java ./qirkat/CommandTest.java ./qirkat/TextReporter.java ./qirkat/Reporter.java ./qirkat/MoreBoardTests.java ./qirkat/MoveTest.java ./qirkat/UnitTest.java ./qirkat/GUI.java ./qirkat/Board.java ./qirkat/Main.java
Starting audit...
qirkat/AI.java:30:9: '//'-style comments are not allowed.
qirkat/AI.java:31:37: '+' is not preceded with whitespace.
qirkat/AI.java:31:38: '+' is not followed by whitespace.
qirkat/AI.java:31:42: '+' is not preceded with whitespace.
qirkat/AI.java:31:43: '+' is not followed by whitespace.
qirkat/AI.java:32:9: '//'-style comments are not allowed.
qirkat/AI.java:58:5: Method length is 61 lines (max allowed is 60).
qirkat/AI.java:63:9: '//'-style comments are not allowed.
qirkat/AI.java:67:9: '//'-style comments are not allowed.
qirkat/AI.java:68:13: Local variable 'BestScore' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/AI.java:70:13: '//'-style comments are not allowed.
qirkat/AI.java:72:29: Local variable 'moves_max' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/AI.java:73:1: '//'-style comments are not allowed.
qirkat/AI.java:74:1: '//'-style comments are not allowed.
qirkat/AI.java:77:1: '//'-style comments are not allowed.
qirkat/AI.java:78:1: '//'-style comments are not allowed.
qirkat/AI.java:79: Line is longer than 80 characters (found 82).
qirkat/AI.java:80: Line is longer than 80 characters (found 87).
qirkat/AI.java:80:1: '//'-style comments are not allowed.
qirkat/AI.java:91:13: '//'-style comments are not allowed.
qirkat/AI.java:93:13: '//'-style comments are not allowed.
qirkat/AI.java:95:29: Local variable 'moves_min' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/AI.java:98:1: '//'-style comments are not allowed.
qirkat/AI.java:99: Line is longer than 80 characters (found 81).
qirkat/AI.java:100: Line is longer than 80 characters (found 87).
qirkat/AI.java:100:1: '//'-style comments are not allowed.
qirkat/AI.java:111:13: '//'-style comments are not allowed.
qirkat/AI.java:118: Don't use trailing comments.
qirkat/AI.java:118:27: '//'-style comments are not allowed.
qirkat/AI.java:132:13: Local variable 'WhitePiece' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/AI.java:133:13: Local variable 'BlackPiece' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/AI.java:135:29: magic.number
qirkat/AI.java:142: Don't use trailing comments.
qirkat/AI.java:142:23: '//'-style comments are not allowed.
qirkat/Manual.java:32:9: '//'-style comments are not allowed.
qirkat/BoardWidget.java:66:9: '//'-style comments are not allowed.
qirkat/BoardWidget.java:89:5: '//'-style comments are not allowed.
qirkat/Move.java:113:13: '//'-style comments are not allowed.
qirkat/Move.java:122: Don't use trailing comments.
qirkat/Move.java:122:27: '//'-style comments are not allowed.
qirkat/Move.java:125: Don't use trailing comments.
qirkat/Move.java:125:27: '//'-style comments are not allowed.
qirkat/Move.java:171: Don't use trailing comments.
qirkat/Move.java:171:58: '//'-style comments are not allowed.
qirkat/Move.java:177: Don't use trailing comments.
qirkat/Move.java:177:58: '//'-style comments are not allowed.
qirkat/Move.java:204:1: '//'-style comments are not allowed.
qirkat/Move.java:209:9: '//'-style comments are not allowed.
qirkat/Move.java:210:9: '//'-style comments are not allowed.
qirkat/Move.java:211:9: '//'-style comments are not allowed.
qirkat/Move.java:217:9: '//'-style comments are not allowed.
qirkat/Move.java:223:8: '//'-style comments are not allowed.
qirkat/Move.java:224:9: '//'-style comments are not allowed.
qirkat/Move.java:304:10: '//'-style comments are not allowed.
qirkat/Move.java:305:9: '//'-style comments are not allowed.
qirkat/BoardTest.java:3:8: Unused import - graph.B.
qirkat/BoardTest.java:32:5: '//'-style comments are not allowed.
qirkat/BoardTest.java:76:5: '//'-style comments are not allowed.
qirkat/BoardTest.java:84:5: '//'-style comments are not allowed.
qirkat/Game.java:39:5: Method length is 72 lines (max allowed is 60).
qirkat/Game.java:46:13: '//'-style comments are not allowed.
qirkat/Game.java:51:13: '//'-style comments are not allowed.
qirkat/Game.java:52:13: '//'-style comments are not allowed.
qirkat/Game.java:63: Line is longer than 80 characters (found 107).
qirkat/Game.java:63:1: '//'-style comments are not allowed.
qirkat/Game.java:70:13: '//'-style comments are not allowed.
qirkat/Game.java:73:17: '//'-style comments are not allowed.
qirkat/Game.java:75:17: '//'-style comments are not allowed.
qirkat/Game.java:82:25: '//'-style comments are not allowed.
qirkat/Game.java:85:1: '//'-style comments are not allowed.
qirkat/Game.java:87:25: '//'-style comments are not allowed.
qirkat/Game.java:98:1: '//'-style comments are not allowed.
qirkat/Game.java:100:17: '//'-style comments are not allowed.
qirkat/Game.java:102:13: '//'-style comments are not allowed.
qirkat/Game.java:172:9: '//'-style comments are not allowed.
qirkat/Game.java:195:9: '//'-style comments are not allowed.
qirkat/Game.java:226:13: '//'-style comments are not allowed.
qirkat/Game.java:227:1: '//'-style comments are not allowed.
qirkat/Game.java:228:1: '//'-style comments are not allowed.
qirkat/Game.java:229:1: '//'-style comments are not allowed.
qirkat/Game.java:232:13: '//'-style comments are not allowed.
qirkat/Game.java:241:9: '//'-style comments are not allowed.
qirkat/Game.java:264:9: '//'-style comments are not allowed.
qirkat/Game.java:280:9: '//'-style comments are not allowed.
qirkat/Game.java:282:1: '//'-style comments are not allowed.
qirkat/Game.java:288:9: '//'-style comments are not allowed.
qirkat/Game.java:297:9: '//'-style comments are not allowed.
qirkat/Game.java:323:1: '//'-style comments are not allowed.
qirkat/Game.java:328:9: '//'-style comments are not allowed.
qirkat/Game.java:332:9: '//'-style comments are not allowed.
qirkat/Game.java:355:9: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:5:8: Unused import - java.lang.reflect.Array.
qirkat/MoreBoardTests.java:11:5: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:12:5: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:13:5: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:52:5: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:59:5: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:132:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:133:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:134:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:135:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:136:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:137:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:138:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:140:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:141:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:142:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:143:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:144:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:145:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:146:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:147:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:148:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:149:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:150:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:151:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:164:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:165:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:166:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:167:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:168:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:170:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:171:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:172:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:173:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:174:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:178:1: '//'-style comments are not allowed.
qirkat/MoreBoardTests.java:193:35: ',' is not followed by whitespace.
qirkat/MoreBoardTests.java:234:1: '//'-style comments are not allowed.
qirkat/MoveTest.java:7:8: Unused import - java.util.ArrayList.
qirkat/MoveTest.java:8:8: Unused import - java.util.List.
qirkat/GUI.java:108:9: '//'-style comments are not allowed.
qirkat/GUI.java:113:9: '//'-style comments are not allowed.
qirkat/GUI.java:122:28: Must have at least one statement.
qirkat/GUI.java:123:13: '//'-style comments are not allowed.
qirkat/GUI.java:124:36: Must have at least one statement.
qirkat/GUI.java:125:13: '//'-style comments are not allowed.
qirkat/GUI.java:131:9: '//'-style comments are not allowed.
qirkat/Board.java:28:9: '//'-style comments are not allowed.
qirkat/Board.java:30:9: '//'-style comments are not allowed.
qirkat/Board.java:50:9: '//'-style comments are not allowed.
qirkat/Board.java:51:9: '//'-style comments are not allowed.
qirkat/Board.java:63:9: '//'-style comments are not allowed.
qirkat/Board.java:77:9: '//'-style comments are not allowed.
qirkat/Board.java:106:9: '//'-style comments are not allowed.
qirkat/Board.java:125:9: '//'-style comments are not allowed.
qirkat/Board.java:147:9: '//'-style comments are not allowed.
qirkat/Board.java:148:9: '//'-style comments are not allowed.
qirkat/Board.java:155:9: '//'-style comments are not allowed.
qirkat/Board.java:156:9: '//'-style comments are not allowed.
qirkat/Board.java:164:9: '//'-style comments are not allowed.
qirkat/Board.java:165:9: '//'-style comments are not allowed.
qirkat/Board.java:171:9: '//'-style comments are not allowed.
qirkat/Board.java:173:9: '//'-style comments are not allowed.
qirkat/Board.java:178:5: Method length is 67 lines (max allowed is 60).
qirkat/Board.java:179:9: '//'-style comments are not allowed.
qirkat/Board.java:180:9: '//'-style comments are not allowed.
qirkat/Board.java:182:1: '//'-style comments are not allowed.
qirkat/Board.java:185:9: '//'-style comments are not allowed.
qirkat/Board.java:189:1: '//'-style comments are not allowed.
qirkat/Board.java:192:1: '//'-style comments are not allowed.
qirkat/Board.java:195:9: '//'-style comments are not allowed.
qirkat/Board.java:197:9: '//'-style comments are not allowed.
qirkat/Board.java:199:1: '//'-style comments are not allowed.
qirkat/Board.java:202:1: '//'-style comments are not allowed.
qirkat/Board.java:207: Line is longer than 80 characters (found 83).
qirkat/Board.java:207:1: '//'-style comments are not allowed.
qirkat/Board.java:214:43: '{' is not preceded with whitespace.
qirkat/Board.java:215:1: '//'-style comments are not allowed.
qirkat/Board.java:222:1: '//'-style comments are not allowed.
qirkat/Board.java:224:24: Conditional logic can be removed.
qirkat/Board.java:234:1: '//'-style comments are not allowed.
qirkat/Board.java:235:13: Conditional logic can be removed.
qirkat/Board.java:236:1: '//'-style comments are not allowed.
qirkat/Board.java:239:1: '//'-style comments are not allowed.
qirkat/Board.java:243:9: '//'-style comments are not allowed.
qirkat/Board.java:272:9: '//'-style comments are not allowed.
qirkat/Board.java:284: Line is longer than 80 characters (found 116).
qirkat/Board.java:284:1: '//'-style comments are not allowed.
qirkat/Board.java:295:9: '//'-style comments are not allowed.
qirkat/Board.java:302:1: '//'-style comments are not allowed.
qirkat/Board.java:305: Line is longer than 80 characters (found 115).
qirkat/Board.java:305:1: '//'-style comments are not allowed.
qirkat/Board.java:306:25: Local variable 'diff_col' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:307:25: Local variable 'diff_row' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:308:26: Local variable 'to_col' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:309:26: Local variable 'to_row' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:310: Line is longer than 80 characters (found 109).
qirkat/Board.java:312:1: '//'-style comments are not allowed.
qirkat/Board.java:317:1: '//'-style comments are not allowed.
qirkat/Board.java:323: Line is longer than 80 characters (found 81).
qirkat/Board.java:323:1: '//'-style comments are not allowed.
qirkat/Board.java:325:38: Local variable 'tail_m' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:328: Line is longer than 80 characters (found 97).
qirkat/Board.java:328:1: '//'-style comments are not allowed.
qirkat/Board.java:329:1: '//'-style comments are not allowed.
qirkat/Board.java:330:1: '//'-style comments are not allowed.
qirkat/Board.java:337:1: '//'-style comments are not allowed.
qirkat/Board.java:342: Line is longer than 80 characters (found 81).
qirkat/Board.java:342:1: '//'-style comments are not allowed.
qirkat/Board.java:343:1: '//'-style comments are not allowed.
qirkat/Board.java:425:1: '//'-style comments are not allowed.
qirkat/Board.java:432:1: '//'-style comments are not allowed.
qirkat/Board.java:433:1: '//'-style comments are not allowed.
qirkat/Board.java:437:1: '//'-style comments are not allowed.
qirkat/Board.java:443:1: '//'-style comments are not allowed.
qirkat/Board.java:446:1: '//'-style comments are not allowed.
qirkat/Board.java:449:1: '//'-style comments are not allowed.
qirkat/Board.java:476:9: '//'-style comments are not allowed.
qirkat/Board.java:477:9: '//'-style comments are not allowed.
qirkat/Board.java:497:25: Local variable 'diff_col' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:498:25: Local variable 'diff_row' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:499:26: Local variable 'toIndex_col' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:500:26: Local variable 'toIndex_row' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:503:39: Local variable '_pos' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:505: Line is longer than 80 characters (found 83).
qirkat/Board.java:506: Line is longer than 80 characters (found 87).
qirkat/Board.java:516:9: '//'-style comments are not allowed.
qirkat/Board.java:518:9: '//'-style comments are not allowed.
qirkat/Board.java:552:9: '//'-style comments are not allowed.
qirkat/Board.java:567:21: Local variable 'prev_size' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:592:9: '//'-style comments are not allowed.
qirkat/Board.java:599:9: '//'-style comments are not allowed.
qirkat/Board.java:621:9: '//'-style comments are not allowed.
qirkat/Board.java:663:9: '//'-style comments are not allowed.
qirkat/Board.java:664:21: Must have at least one statement.
qirkat/Board.java:666:9: '//'-style comments are not allowed.
qirkat/Board.java:704:33: Local variable 'non_cap' must start with a lower-case letter, or consist of a single, upper-case letter.
qirkat/Board.java:705:1: '//'-style comments are not allowed.
qirkat/Board.java:734: First sentence should end with a period.
qirkat/Main.java:39:17: '//'-style comments are not allowed.
qirkat/Main.java:40:17: '//'-style comments are not allowed.
Audit done.
Checkstyle ends with 232 errors.

=============================================

III. Running our tests.


Running testing scripts
01-commands1.inp: OK
01-moves1.inp: OK
01-moves2.inp: ERROR (uncaught exception occurwhite: Exception in thread "main" java.lang.AssertionError near line 296)
01-moves3.inp: OK
01-moves4.inp: OK
01-win1.inp: OK
02-moves1.inp: ERROR (output mismatch ( - w - - - /  - - w - -) near line 27)
02-moves2.inp: ERROR (uncaught exception occurwhite: Exception in thread "main" java.lang.AssertionError near line 15)
02-moves3.inp: OK
03-play1.inp: ERROR (invalid move for white (Exception in thread "main" java.lang.AssertionError
) near line 6)
03-play2.inp: ERROR (other program fails to finish)
03-play3.inp: ERROR (other program fails to finish)
03-play4.inp: ERROR (other program fails to finish)
04-commands2.inp: OK
04-win1.inp: ERROR (invalid move for black (Exception in thread "main" java.lang.AssertionError
) near line 41)
04-win2.inp: OK
test01.inp: OK
test02.inp: OK
test03.inp: OK
test04.inp: OK

Summary:
    20 tests
    12 passed
     8 errors
     0 problematic tests


===TEST 01-moves2.inp===

Test script 01-moves2.inp:
--------------------------------------------------
# Test all legal diagonal moves.
java -ea qirkat.Main
set white w---- ----- ----- ----- b----
a1-b2
dump
@<===
@< b - - - -
@< - - - - -
@< - - - - -
@< - w - - -
@< - - - - -
@<===
set white --w-- ----- ----- ----- --b--
c1-d2
dump
@<===
@< - - b - -
@< - - - - -
@< - - - - -
@< - - - w -
@< - - - - -
@<===
set white --w-- ----- ----- ----- --b--
c1-b2
dump
@<===
@< - - b - -
@< - - - - -
@< - - - - -
@< - w - - -
@< - - - - -
@<===
set white ----w ----- ----- ----- ----b
e1-d2
dump
@<===
@< - - - - b
@< - - - - -
@< - - - - -
@< - - - w -
@< - - - - -
@<===
set white ----- -w--- ----- ----- -b---
b2-c3
dump
@<===
@< - b - - -
@< - - - - -
@< - - w - -
@< - - - - -
@< - - - - -
@<===
set white ----- -w--- ----- ----- -b---
b2-a3
dump
@<===
@< - b - - -
@< - - - - -
@< w - - - -
@< - - - - -
@< - - - - -
@<===
set white ----- ---w- ----- ----- ---b-
d2-e3
dump
@<===
@< - - - b -
@< - - - - -
@< - - - - w
@< - - - - -
@< - - - - -
@<===
set white ----- ---w- ----- ----- ---b-
d2-c3
dump
@<===
@< - - - b -
@< - - - - -
@< - - w - -
@< - - - - -
@< - - - - -
@<===
set white ----- ----- w---- ----- b----
a3-b4
dump
@<===
@< b - - - -
@< - w - - -
@< - - - - -
@< - - - - -
@< - - - - -
@<===
set white ----- ----- --w-- ----- --b--
c3-d4
dump
@<===
@< - - b - -
@< - - - w -
@< - - - - -
@< - - - - -
@< - - - - -
@<===
set white ----- ----- --w-- ----- --b--
c3-b4
dump
@<===
@< - - b - -
@< - w - - -
@< - - - - -
@< - - - - -
@< - - - - -
@<===
set white ----- ----- ----w ----- ----b
e3-d4
dump
@<===
@< - - - - b
@< - - - w -
@< - - - - -
@< - - - - -
@< - - - - -
@<===
set white ----- ----- ----- -w--- -b---
b4-c5
dump
@<===
@< - b w - -
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - - -
@<===
set white ----- ----- ----- -w--- -b---
b4-a5
dump
@<===
@< w b - - -
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - - -
@<===
set white ----- ----- ----- ---w- ---b-
d4-e5
dump
@<===
@< - - - b w
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - - -
@<===
set white ----- ----- ----- ---w- ---b-
d4-c5
dump
@<===
@< - - w b -
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - - -
@<===
set black -w--- -b--- ----- ----- -----
b2-c1
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - - -
@< - w b - -
@<===
set black -w--- -b--- ----- ----- -----
b2-a1
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - - -
@< b w - - -
@<===
set black ---w- ---b- ----- ----- -----
d2-e1
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - w b
@<===
set black ---w- ---b- ----- ----- -----
d2-c1
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - - -
@< - - b w -
@<===
set black w---- ----- b---- ----- -----
a3-b2
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - -
@< - b - - -
@< w - - - -
@<===
set black --w-- ----- --b-- ----- -----
c3-d2
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - b -
@< - - w - -
@<===
set black --w-- ----- --b-- ----- -----
c3-b2
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - -
@< - b - - -
@< - - w - -
@<===
set black ----w ----- ----b ----- -----
e3-d2
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - -
@< - - - b -
@< - - - - w
@<===
set black -w--- ----- ----- -b--- -----
b4-c3
dump
@<===
@< - - - - -
@< - - - - -
@< - - b - -
@< - - - - -
@< - w - - -
@<===
set black -w--- ----- ----- -b--- -----
b4-a3
dump
@<===
@< - - - - -
@< - - - - -
@< b - - - -
@< - - - - -
@< - w - - -
@<===
set black ---w- ----- ----- ---b- -----
d4-e3
dump
@<===
@< - - - - -
@< - - - - -
@< - - - - b
@< - - - - -
@< - - - w -
@<===
set black ---w- ----- ----- ---b- -----
d4-c3
dump
@<===
@< - - - - -
@< - - - - -
@< - - b - -
@< - - - - -
@< - - - w -
@<===
set black w---- ----- ----- ----- b----
a5-b4
dump
@<===
@< - - - - -
@< - b - - -
@< - - - - -
@< - - - - -
@< w - - - -
@<===
set black --w-- ----- ----- ----- --b--
c5-d4
dump
@<===
@< - - - - -
@< - - - b -
@< - - - - -
@< - - - - -
@< - - w - -
@<===
set black --w-- ----- ----- ----- --b--
c5-b4
dump
@<===
@< - - - - -
@< - b - - -
@< - - - - -
@< - - - - -
@< - - w - -
@<===
set black ----w ----- ----- ----- ----b
e5-d4
dump
@<===
@< - - - - -
@< - - - b -
@< - - - - -
@< - - - - -
@< - - - - w
@<===
--------------------------------------------------

===TEST 02-moves1.inp===

Test script 02-moves1.inp:
--------------------------------------------------
# Check that horizonal moves are not reversible.
java -ea qirkat.Main
set white ----- --b-- ----- -w--- -----
dump
@<===
@< - - - - -
@< - w - - -
@< - - - - -
@< - - b - -
@< - - - - -
@<===
b4-c4
c2-b2
dump
@<===
@< - - - - -
@< - - w - -
@< - - - - -
@< - b - - -
@< - - - - -
@<===
c4-b4
b2-c2
dump
@<===
@< - - - - -
@< - - w - -
@< - - - - -
@< - b - - -
@< - - - - -
@<===
--------------------------------------------------

Reached limit on number of error inputs/outputs shown (2)

Summary: 8 errors detected

=============================================

IV. Running make check.


make -C qirkat check
java -ea -cp "..:.:/home/ff/cs61b/lib/*:;..;.:/home/ff/cs61b/lib/*" qirkat.UnitTest
b
2
tail of the move
tail of the move
tail of the move
  b b b b b
  b - - - -
  b w - - w
  w w b - w
  w - - w -
a3-c1-e1
a3-c3
  b b b b b
  - b b - b
  - - - w w
  b w w w w
  b w - w w
a1-c3-c1-a1
a1-c1-a3
a1-c1-c3-a1
tail of the move
  - - b - -
  - - - b -
  - - - w -
  - - - - -
  - - - - -
d3-d5-b5
  - b - b -
  - - w - -
  - - - w -
  b - - - -
  b - b - -
  - - w b -
  - b - - -
  - - - - w
  b - - - -
  b b - - -
  - - - - -
  - b b - -
  - w - - -
  - - b b -
  - - - - w
  b b b b b
  b b - b b
  - b w w w
  w - - w w
  w w b w w
c3-a3
Time: 0.088
Ran 33 tests. All passed.
make -C ../testing check
CLASSPATH="..:.:/home/ff/cs61b/lib/*:;..;.:/home/ff/cs61b/lib/*" python3 test-qirkat.py *.inp
test01.inp: OK
test02.inp: OK
test03.inp: OK
test04.inp: OK

Summary:
     4 tests
     4 passed
     0 errors
     0 problematic tests

make -f /home/ff/cs61b/testing-files/proj2/testing.mk --no-print-directory -C proj2 extra-credit;

V. Check for extra credit.

Extra credit not attempted.

*************************

SUMMARY

 PRE. all required files present.
   I. Compilation: successful
  II. Style check: style errors found.
 III. Our tests: passed 12 of 20 tests.
  IV. Make check: all tests passed.
   V. Did not attempt extra credit.

make: *** [standard] Error 1
make: Target `default' not remade because of errors.

<<PROBLEM: Test(s) terminated with non-zero exit code 512

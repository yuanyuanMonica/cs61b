package qirkat;

        import javafx.geometry.Side;

        import static qirkat.PieceColor.*;
        import java.util.ArrayList;
        import static java.lang.Math.max;

/** A Player that computes its own moves.
 *  @author Jin Liu
 */
class AI extends Player {

    /** Maximum minimax search depth before going to static evaluation. */
    private static final int MAX_DEPTH = 8;
    /** A position magnitude indicating a win (for white if positive, black
     *  if negative). */
    private static final int WINNING_VALUE = Integer.MAX_VALUE - 1;
    /** A magnitude greater than a normal value. */
    private static final int INFTY = Integer.MAX_VALUE;

    /** A new AI for GAME that will play MYCOLOR. */
    AI(Game game, PieceColor myColor) {
        super(game, myColor);
    }

    @Override
    Move myMove() {
        Main.startTiming();
        Move move = findMove();
        Main.endTiming();
        System.out.println(myColor() + ": " + move.toString());
        return move;
    }

    /** Return a move for me from the current position, assuming there
     *  is a move. */
    private Move findMove() {
        Board b = new Board(board());
        if (myColor() == WHITE) {
            findMove(b, MAX_DEPTH, true, 1, -INFTY, INFTY);
        } else {
            findMove(b, MAX_DEPTH, true, -1, -INFTY, INFTY);
        }
        return _lastFoundMove;
    }

    /** The move found by the last call to one of the ...FindMove methods
     *  below. */
    private Move _lastFoundMove;

    /** Find a move from position BOARD and return its value, recording
     *  the move found in _lastFoundMove iff SAVEMOVE. The move
     *  should have maximal value or have value > BETA if SENSE==1,
     *  and minimal value or value < ALPHA if SENSE==-1. Searches up to
     *  DEPTH levels.  Searching at level 0 simply returns a static estimate
     *  of the board value and does not set _lastMoveFound. */
    private int findMove(Board board, int depth, boolean saveMove, int sense,
                         int alpha, int beta) {
        Move best;
        best = null;
        int bestValue;
        if (depth == 0 || board.gameOver()) {
            return staticScore(board);
        }
        ArrayList<Move> moves = board.getMoves();
        if (sense == 1) {
            bestValue = -INFTY;
            for (Move mov: moves) {
                board.makeMove(mov);
                int response = findMove(board, depth - 1, false, -1, alpha, beta);
                board.undo();
                if (response >= bestValue) {
                    best = mov;
                    bestValue = response;
                    alpha = Math.max(response, alpha);
                }
                if (beta <= alpha) {
                    break;
                }
            }
        } else {
            bestValue = INFTY;
            for (Move mov: moves) {
                board.makeMove(mov);
                int response = findMove(board, depth - 1, false, 1, alpha, beta);
                board.undo();
                if (response <= bestValue) {
                    best = mov;
                    bestValue = response;
                    beta = Math.min(response, beta);
                }
                if (beta <= alpha) {
                    break;
                }
            }
        }

        if (saveMove) {
            _lastFoundMove = best;
        }

        return bestValue;
    }

    /** Return a heuristic value for BOARD. */
    private int staticScore(Board board) {
        int white, black;
        white = black = 0;
        if(board().gameOver()) {
            if (board.whoseMove().equals (WHITE)) {
                return -WINNING_VALUE;
            } else {
                return WINNING_VALUE;
            }
        }
        for (int i = 0; i < 25; i++ ) {
            if(board.get(i).equals(WHITE)) {
                white += 1;
            } else if (board.get(i).equals(BLACK)) {
                black += 1;
            }
        }
        int score = ((white - black) * 10000) / (white + black);
        return score;
    }
}

/**
 * qirkat: set white ----- ----- ---w- ---b- --b--
 qirkat: start
 White is Manua
 */